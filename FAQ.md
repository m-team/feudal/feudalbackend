
How do I register a FEUDAL client?
---

- Go into the admin at `/backend/admin` and login
- Navigate to `Home > Backend > Users`
    - Click "ADD USER" in top righthand corner
        - Specify a username and a password
        - The username and password need to be specified in the clients own config, see [here](https://codebase.helmholtz.cloud/m-team/feudal/feudalClient)
        - Click save
- Navigate to `Home > Backend > Sites`
    - Click "ADD SITE" in top righthand corner
        - For the client field select the client user you just created
        - Specify a name, and optionally a description for the site
        - Click save
        
        
Where are the logs?
---

- The logging depends on your configuration. Here are the default log locations:
    - nginx logs to /var/log/nginx
    - rabbitmq logs to /var/log/rabbitmq
    - django logs to /home/feudal/logs (it creates multiple log files for different log levels)
    - uwsgi logs to /home/feudal/logs/uwsgi.log
