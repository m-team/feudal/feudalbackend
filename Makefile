
PWD := $(shell pwd)

KEY := .secret.key
VENV := env
ADMIN := static
WEBPAGE := webpage
DIST := $(WEBPAGE)/dist
RPMBUILDROOT := $(PWD)/.rpmbuild

.PHONY: all $(WEBPAGE) rpm clean

all: $(ADMIN) $(DIST)

$(KEY):
	dd if=/dev/urandom bs=64 count=1 | base64 > $@

$(VENV):
	@python3.6 -m venv $@; \
		source $@/bin/activate; \
		pip -q install -U pip; \
		pip -q install .


$(ADMIN): $(KEY) $(VENV)
	@export DJANGO_SECRET=$< ; \
		./manage.py collectstatic --no-input

$(WEBPAGE):
	@git submodule update --init --force

$(DIST): $(WEBPAGE)
	@$(MAKE) -C $^ prod

$(RPMBUILDROOT):
	mkdir -p $@

rpm: $(DIST) $(ADMIN) $(RPMBUILDROOT)
	@docker run \
		--name feudal-rpmbuild \
		--rm=true \
		-v $(RPMBUILDROOT):/home/rpmbuilder/rpmbuild \
		-v $(PWD)/feudal.spec:/home/rpmbuilder/rpmbuild/SPECS/feudal.spec \
		-v $(PWD):/home/rpmbuilder/rpmbuild/SOURCES \
		jc21/rpmbuild-centos7 \
		/bin/build-spec /home/rpmbuilder/rpmbuild/SPECS/feudal.spec
	@cp $(RPMBUILDROOT)/RPMS/noarch/*.rpm $(PWD)

clean:
	@$(MAKE) -C webpage cleaner
	@rm -rf $(ADMIN) $(VENV) $(RPMBUILDROOT)
