
[![pipeline status](https://codebase.helmholtz.cloud/m-team/feudal/feudalBackend/badges/master/pipeline.svg)](https://codebase.helmholtz.cloud/m-team/feudal/feudalBackend/-/commits/master)
[![coverage report](https://codebase.helmholtz.cloud/m-team/feudal/feudalBackend/badges/master/coverage.svg)](https://codebase.helmholtz.cloud/m-team/feudal/feudalBackend/-/commits/master)

# FEderated User Credential Deployment PortAL (FEUDAL)
- User interface: [Webpage](https://codebase.helmholtz.cloud/m-team/feudal/feudalWebpage)
- Component at the sites: [Client](https://codebase.helmholtz.cloud/m-team/feudal/feudalClient)

## Installation
- Needs Python 3.6
- For debian 9: see debian/README.md
- A puppet setup is could be made available.


## Starting the backend
  - systemctl start nginx rabbitmq-server (as root)
  - systemctl --user start uwsgi

## Runtime Configuration
- For runtime configuration we use the django inbuilt admin interface.
	- Default path of the django admin: `/backend/admin`
	- The credentials for the admin were entered by you during the run of the `install` script
- Your OpenId Connect clients are configured in `Home > Backend > Oidc configs`
	- The default redirect URI is: `/auth/callback`
	- `scopes` is a list of strings (JSON)
- Users *and* FEUDAL Clients are managed in `Home > Backend > Users`
	- You can manually add FEUDAL Clients
	- You can specify admin users
- You need to configure the `sites`, which provide services to your users in `Home > Backend > Sites`
- Configure your RabbitMQ instance in `Home > Backend > Rabbit mq instances`
