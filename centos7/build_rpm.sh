#!/bin/sh

base=$(dirname "$0")
cd $base/..

RPMBUILDROOT=${RPMBUILDROOT-~/.rpmbuild}
mkdir -p $RPMBUILDROOT
SPEC=feudal.spec

docker run \
	--name rpmbuild-centos7-feudal \
	-v $RPMBUILDROOT:/home/rpmbuilder/rpmbuild \
	-v $(pwd)/$SPEC:/home/rpmbuilder/rpmbuild/SPECS/$SPEC \
	-v $(pwd):/home/rpmbuilder/rpmbuild/SOURCES \
	--rm=true \
	jc21/rpmbuild-centos7 \
	/bin/build-spec /home/rpmbuilder/rpmbuild/SPECS/$SPEC
