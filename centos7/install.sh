#!/bin/bash

# exit whole script
trap "exit" INT

# {{{ CONFIG FILES
config_file_default=./config.env.default
config_file=${1-./config.env}

if [[ ! -f $config_file ]]
then
	echo "FEUDAL> Need the file with config variables!"
	echo "FEUDAL> Adjust the file $config_file_default to your needs"
	echo "FEUDAL> Copy the result to $config_file or provide the file name as parameter to this script"
	exit 1
fi

# load config variables
source $config_file

if [[ $user != $(whoami) ]]
then
	echo "FEUDAL> You need to run this script as user $user"
	echo "FEUDAL> Exiting"
	exit 1
fi

templates=$backend/config-templates

# the webpage is now a submodule
export webpage=$backend/webpage
export dist=$webpage/dist

mkdir -p $logs
echo "FEUDAL> Logs will be located at: $logs"

mkdir -p $config
echo "FEUDAL> Config files will be located at: $config"

# django settings

[[ $DEV ]] && export django_debug=True || export django_debug=False
echo "FEUDAL> Configuring $config/django_settings.py with DEBUG=$django_debug"
cat $templates/django_settings.py | envsubst > $config/django_settings.py
echo "FEUDAL> Configuring $backend/feudal/settings.py"
ln -sf $config/django_settings.py $backend/feudal/settings.py

# django secret key
if [[ ! -f $secret ]]
then
	echo "FEUDAL> Generating secret key: $secret"
	dd if=/dev/urandom bs=64 count=1 | base64 > $secret
fi

# uwsgi files
echo "FEUDAL> Configuring $uwsgi_ini"
cat $templates/uwsgi.ini | envsubst > $uwsgi_ini

echo "FEUDAL> Configuring $HOME/.local/share/systemd/user/uwsgi.service"
mkdir -p $HOME/.local/share/systemd/user
cat $templates/uwsgi.service | envsubst > $HOME/.local/share/systemd/user/uwsgi.service
echo "FEUDAL> Configuring $config/uwsgi_params"
cp $templates/uwsgi_params $config/uwsgi_params

echo "FEUDAL> Configuring $mysql"
cat $templates/mysql.cnf | envsubst > $mysql

echo "FEUDAL> Enabling uWSGI service"
mkdir -p $HOME/.config/systemd/user/default.target.wants
ln -s $systemd/uwsgi.service $HOME/.config/systemd/user/default.target.wants
# }}}

# {{{ PYTHON
echo "FEUDAL> Creating new python virtual environment: $venv"
python3.6 -m venv $venv

echo "FEUDAL> Activating virtual environment"
source $venv/bin/activate

pip -q install -U pip
pip install .
# }}}

# {{{ DJANGO
echo "FEUDAL> Starting database migration"
# makemigrations seems to be a bad idea here
# $backend/manage.py makemigrations backend
$backend/manage.py migrate


if [[ -d $static ]]
then
	rm -rf $static
	mkdir -p $static
fi
echo "FEUDAL> Preparing static files in $static"
$backend/manage.py collectstatic

# }}}

# {{{ WEBPAGE
cd $webpage
# this file get changed during building
git checkout package-lock.json

cd $backend
git submodule update --init

echo "FEUDAL> Changing directory to $webpage"
cd $webpage

echo "FEUDAL> Installing dependencies (using npm install)"
export npm_config_loglevel=error
npm install

ng=$webpage/node_modules/.bin/ng
ng_flags="--prod"
[[ -n $DEV ]] && ng_flags="--aot"
echo "FEUDAL> Building webpage (using $ng build $ng_flags)"
$ng build $ng_flags
# }}}

echo "FEUDAL>"
echo "FEUDAL> Done. Please check the output above for errors"
echo "FEUDAL>"
echo "FEUDAL> Here is how you can start the backend:"
echo "FEUDAL> Run as root: systemctl start nginx rabbitmq-server"
echo "FEUDAL> Run as $user: systemctl --user start uwsgi"
echo "FEUDAL>"
echo "FEUDAL> Here is how you can check the status of the backend:"
echo "FEUDAL> Run as $user: systemctl status nginx rabbitmq-server"
echo "FEUDAL> Run as $user: systemctl --user status uwsgi"
echo "FEUDAL>"
echo "FEUDAL> Have a nice day"

# vim: set foldmethod=marker :
