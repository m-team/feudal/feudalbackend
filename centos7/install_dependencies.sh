#!/bin/bash
# vim: foldmethod=marker

#{{{ python 3.6
# enable additional repos
# subscription-manager repos --enable rhel-7-server-optional-rpms --enable rhel-server-rhscl-7-rpms

# install gcc and the like
# yum -y install 	@development

yum -y install @development python36 python36-devel python36-pip uwsgi-plugin-python36 epel-release
yum -y install mariadb-server mariadb-devel
#}}}

#{{{ rabbitmq 3.7

# erlang 21
curl -s https://packagecloud.io/install/repositories/rabbitmq/erlang/script.rpm.sh | bash

# add rabbitmq packagecloud repo
curl -s https://packagecloud.io/install/repositories/rabbitmq/rabbitmq-server/script.rpm.sh | bash

#}}}

yum -y install rabbitmq-server nginx

#{{{ mysql 5.7
yum install mysql-community-devel
#}}}
