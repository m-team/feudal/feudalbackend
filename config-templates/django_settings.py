
import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = $django_debug

if DEBUG:
    DEBUG_AUTH = False
    DEBUG_PUBLISHING = False
    DEBUG_CREDENTIALS = False

ALLOWED_HOSTS = [
    '$domain',
]

AUTH_USER_MODEL = 'backend.User'

# cookie settings
SESSION_COOKIE_AGE = 3600
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = False

WSGI_APPLICATION = 'feudal.wsgi.application'

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

CSRF_HEADER_NAME = 'HTTP_X_CSRFTOKEN'

CORS_ORIGIN_ALLOW_ALL = True

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
with open('$secret') as f:
    SECRET_KEY = f.read().strip()

# Application definition
ROOT_URLCONF = 'feudal.urls'

STATIC_URL = '/backend/static/'
STATIC_ROOT = '$static'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'feudal.backend',
    'corsheaders',
    'django_mysql',
    'django_nose',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'OPTIONS': {
            'read_default_file': os.path.expanduser('$mysql'),
            'init_command': "SET innodb_strict_mode=1; SET sql_mode='STRICT_TRANS_TABLES';",
            'charset': 'utf8mb4'
        },
        'TEST': {
            'NAME': 'scc-hdfmysql0001_portal_dev_test',
        }
    }
}

# AUTHENTICATION AND AUTHORIZATION
AUTHENTICATION_BACKENDS = [
    'feudal.backend.auth.v1.OIDCTokenAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'feudal.backend.auth.v1.OIDCTokenAuthHTTPBackend',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Berlin'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# LOGGING
AUDIT_LOG_LEVEL = 25
LOGGING_ROOT = os.path.expanduser('$logs')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        },
        'compact': {
            'format': '%(levelname)s - %(message)s',
        },
        'audit': {
            'format': '%(asctime)s - %(message)s',
        },
    },
    'handlers': {
        'django': {
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/django.log',
            'formatter': 'standard',
        },
        'debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/debug.log',
            'formatter': 'standard',
        },
        'compact-debug': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/compact-debug.log',
            'formatter': 'compact',
        },
        'info': {
            'level': 'INFO',
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/info.log',
            'formatter': 'standard',
        },
        'error': {
            'level': 'ERROR',
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/error.log',
            'formatter': 'standard',
        },
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'standard',
        },
        'audit': {
            'level': AUDIT_LOG_LEVEL,
            'class': 'logging.FileHandler',
            'filename': LOGGING_ROOT + '/audit.log',
            'formatter': 'audit',
        },
    },
    'loggers': {
        'feudal': {
            'handlers': ['compact-debug', 'debug', 'info', 'error', 'audit'],
            'level': 'DEBUG',
        },
        'django': {
            'handlers': ['django'],
            'level': 'INFO',
        },
    },
}
