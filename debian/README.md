FEderated User Credential Deployment PortAL (FEUDAL)
====
- User interface: [Webpage](https://codebase.helmholtz.cloud/m-team/feudal/feudalWebpage)
- Component at the sites: [Client](https://codebase.helmholtz.cloud/m-team/feudal/feudalClient)

Installation (Debian 9.6)
----

- Add some dependency repositories
  - Add repository for Erlang 21 (e.g. from [here](https://www.rabbitmq.com/install-debian.html#bintray-apt-repo-erlang))
  - Add repository for node.js v10.x (e.g. from [nodesource](https://github.com/nodesource/distributions#debinstall))
- Add a user for the backend
	- `useradd -m $user`
  - login as $user
	- `git clone https://codebase.helmholtz.cloud/m-team/feudal/feudalBackend $backend`
	- `cd $backend`
    - `cp config.env.default config.env`
      - Adjust the default values where needed
        - $backend needs to the directory this repo is cloned into
        - $domain is the domain of your host machine
    - Run `./install_privileged` as root
		- Run `./install` as $user

Starting the backend
----
  - systemctl start nginx rabbitmq-server (as root)
  - systemctl --user start uwsgi

Checking the status of the backend
----
 - `./status`

		
Runtime Configuration
----

- For runtime configuration we use the django inbuilt admin interface.
 	- Default path of the django admin: `/backend/admin`	
	- The credentials for the admin were entered by you during the run of the `install` script
- Your OpenId Connect clients are configured in `Home > Backend > Oidc configs`
	- The default redirect URI is: `/backend/auth/v1/callback`
	- `scopes` is a list of strings (JSON)
- Users *and* FEUDAL Clients are managed in `Home > Backend > Users`
	- You can manually add FEUDAL Clients
	- You can specify admin users
- You need to configure the `sites`, which provide services to your users in `Home > Backend > Sites`
- Configure your RabbitMQ instance in `Home > Backend > Rabbit mq instances`

