#!/bin/bash

if [[ $EUID -ne 0 ]]
then
	echo "FEUDAL> Need root privileges"
	exit 1
fi


# {{{ CONFIG FILES
config_file_default=./config.env.default
config_file=${1-./config.env}

if [[ ! -f $config_file ]]
then
	echo "FEUDAL> Need the file with config variables!"
	echo "FEUDAL> Adjust the file $config_file_default to your needs"
	echo "FEUDAL> Copy the result to $config_file or provide the file name as parameter to this script"
	exit 1
fi

# load config variables
source $config_file

templates=$backend/config-templates

# the webpage is now a submodule
export webpage=$backend/webpage
export dist=$webpage/dist

# write config files
if [[ -z $nginx_dir ]]; then echo "set the nginx_dir"; exit 1; fi
mkdir -p $nginx_dir
echo "FEUDAL> Configuring $nginx_dir/feudal.conf"
# ATTENTION! nginx.conf contains variables we dont want to substitute!
# We can tell envsubst which variables to substitute
cat $templates/nginx.conf | envsubst '\$dist \$static \$uwsgi_socket \$port_websocket \$ssl_chain \$ssl_fullchain \$ssl_key \$dhparam \$domain \$config' > $nginx_dir/feudal.conf

rabbitmq_dir=/etc/rabbitmq
mkdir -p $rabbitmq_dir
echo "FEUDAL> Configuring $rabbitmq_dir/rabbitmq.conf"
cat $templates/rabbitmq.conf | envsubst > $rabbitmq_dir/rabbitmq.conf
# }}}

# {{{ APT
echo "FEUDAL> Installing dependencies"
apt install nginx rabbitmq-server uwsgi-plugin-python3 python3 python3-venv default-libmysqlclient-dev gcc nodejs
# }}}

# {{{ NGINX
echo "FEUDAL> Generating diffie hellman parameters"
openssl dhparam -out $dhparam 2048
# }}}

# {{{ RABBITMQ
echo "FEUDAL> Activating RabbitMQ plugins"
rabbitmq-plugins enable rabbitmq_web_stomp rabbitmq_stomp rabbitmq_auth_backend_http
# }}}

# {{{ SYSTEMD
echo "FEUDAL> Enabling nginx service"
echo "FEUDAL> Enabling rabbitmq-server service"
systemctl enable nginx rabbitmq-server
# }}}


echo "FEUDAL> Done."
echo "FEUDAL>"
echo "FEUDAL> Proceed by running as $user: ./install"

# vim: set foldmethod=marker :
