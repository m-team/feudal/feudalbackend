
Name: feudal
Version: 0.1.0
Release: 1
Summary: FEUDAL
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-buildroot
Prefix: %{_prefix}
BuildArch: noarch
Vendor: Lukas Burgey
Url: https://codebase.helmholtz.cloud/m-team/feudal
License: MIT

Requires: gcc
Requires: python36
Requires: python36-devel
Requires: python36-pip
Requires: uwsgi >= 2.0.0
Requires: uwsgi-plugin-python36
Requires: mysql-community-devel >= 5.7.0
Requires: nginx
Requires: rabbitmq-server >= 3.7.0

# ignore byte compilation errors
%global _python_bytecompile_errors_terminate_build 0

%description
FEUDAL backend and webpage


%install
rm -rf $RPM_BUILD_ROOT

%define feudal $RPM_BUILD_ROOT%{_datadir}/feudal
mkdir -p %{feudal}
install -d %{_sourcedir}/feudal %{feudal}
install %{_sourcedir}/manage.py \
			%{_sourcedir}/setup.py \
			%{_sourcedir}/LICENSE \
			%{_sourcedir}/README.md \
			%{feudal}

%define var $RPM_BUILD_ROOT%{_localstatedir}/feudal
mkdir -p %{var}/webpage
cp -r %{_sourcedir}/webpage/dist %{var}/webpage
mkdir -p %{var}/webpage
cp -r %{_sourcedir}/static %{var}/django-static


%post
pip3.6 install %{feudal}


%postun
pip3.6 uninstall --yes feudal


%files
%doc %{feudal}/README.md

%{_datadir}/feudal
%{_localstatedir}/feudal


%changelog
