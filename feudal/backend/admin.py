# vim: set foldmethod=marker :
# pylint: disable=unused-argument

from django.contrib.admin import site, ModelAdmin
from django.contrib.auth.models import Group as AuthGroup
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm

from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin

from feudal.backend.models import Site, Service
from feudal.backend.models.users import User, SSHPublicKey
from feudal.backend.models.deployments import VODeployment, ServiceDeployment, Deployment, DeploymentState, CredentialState, TARGET_CHOICES, STATE_CHOICES
from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.auth.vos import VO, Group, Entitlement, EntitlementNameSpace


class ReadonlyAdmin(ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        return list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))

# filters {{{
class ListFilter(SimpleListFilter):
    def lookups(self, request, model_admin):
        raise NotImplementedError('abstract ListFilter used')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(**{self.parameter_name: self.value()})
        return queryset

class UserFilter(ListFilter):
    title = 'User'
    parameter_name = 'user__id'

    def lookups(self, request, model_admin):
        return [(u.id, u.email) for u in User.objects.filter(user_type=User.TYPE_CHOICE_USER)]

class UserTypeFilter(ListFilter):
    title = 'Type'
    parameter_name = 'user_type'

    def lookups(self, request, model_admin):
        return User.TYPE_CHOICES

class EntitlementNameSpaceFilter(ListFilter):
    title = 'Namespace'
    parameter_name = 'name_space'

    def lookups(self, request, model_admin):
        return [(ns.id, str(ns.name)) for ns in EntitlementNameSpace.objects.order_by('name')]

class IdPFilter(ListFilter):
    title = 'IdP'
    parameter_name = 'idp__id'

    # returns sorted list of pairs
    def lookups(self, request, model_admin):
        return [(idp.id, str(idp)) for idp in OIDCConfig.objects.order_by('name')]

class StateTargetFilter(ListFilter):
    title = 'State Target'
    parameter_name = 'state_target'

    # returns sorted list of pairs
    def lookups(self, request, model_admin):
        return TARGET_CHOICES

class StateFilter(ListFilter):
    title = 'State'
    parameter_name = 'state'

    # returns sorted list of pairs
    def lookups(self, request, model_admin):
        return STATE_CHOICES

class ServiceFilter(ListFilter):
    title = 'Service'
    parameter_name = 'service__id'

    # returns sorted list of pairs
    def lookups(self, request, model_admin):
        return [(s.id, s.name) for s in Service.objects.all()]
# }}}

# users {{{
# from https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#custom-users-and-the-built-in-auth-forms
class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = UserCreationForm.Meta.fields + (
            'user_type', 'idp',
        )

class CustomUserAdmin(UserAdmin):
    list_display = ('__str__', 'idp', 'is_staff', 'is_superuser', 'date_joined', 'last_login',)
    list_filter = (UserTypeFilter, IdPFilter,)
    add_form = CustomUserCreationForm

    fieldsets = (
        (None, {'fields': ('username', 'password', 'user_type',)}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser',)}),
        ('Dates', {'fields': ('last_login', 'date_joined')}),
        ('Upstream Client fields', {'fields': ('idp',)}),
    )
    add_fieldsets = (
        (None, {'fields': ('user_type',)}),
    ) + UserAdmin.add_fieldsets + (
        ('Upstream Client fields', {'fields': ('idp',)}),
    )

site.register(User, CustomUserAdmin)
site.unregister(AuthGroup)
# }}}

# vos {{{
class GroupAdmin(PolymorphicChildModelAdmin):
    base_model = Group  # Explicitly set here!
    show_in_index = True  # makes child model admin visible in main admin site
    list_display = ('name', 'idp', 'description')
    list_filter = (IdPFilter,)

class EntitlementAdmin(PolymorphicChildModelAdmin):
    base_model = Entitlement  # Explicitly set here!
    show_in_index = True  # makes child model admin visible in main admin site
    list_display = ('name', 'idp', 'description')
    list_filter = (IdPFilter, EntitlementNameSpaceFilter)

class VOAdmin(PolymorphicParentModelAdmin):
    """ The parent model admin """
    base_model = VO  # Optional, explicitly set here.
    child_models = (Group, Entitlement,)
    list_display = ('name', 'idp', 'description')
    list_filter = (IdPFilter,)

def flush_idp(modeladmin, request, queryset):
    for idp in queryset:
        idp.flush()

flush_idp.short_description = 'Delete users and vos related to the selected oidc configs'

class OIDCConfigAdmin(ModelAdmin):
    list_display = ('name', 'client_id', 'issuer_uri', 'enabled')
    fields = ('name', 'client_id', 'client_secret', 'issuer_uri', 'enabled', 'scopes', 'userinfo_field_groups', 'userinfo_field_entitlements')
    actions = (flush_idp,)

    def get_readonly_fields(self, request, obj=None):
        # the issuer may never be changed after creation
        if obj:
            return ['issuer_uri']

        return []

site.register(Group, GroupAdmin)
site.register(Entitlement, EntitlementAdmin)
site.register(VO, VOAdmin)
site.register(EntitlementNameSpace)
site.register(OIDCConfig, OIDCConfigAdmin)
# }}}

# deployments {{{
class VODeploymentAdmin(PolymorphicChildModelAdmin):
    show_in_index = True
    list_display = ('id', 'user', 'vo', 'state_target', 'target_reached',)
    list_filter = (StateTargetFilter, UserFilter,)

    def get_readonly_fields(self, request, obj=None):
        return list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))

class ServiceDeploymentAdmin(PolymorphicChildModelAdmin):
    show_in_index = True
    list_display = ('id', 'user', 'service', 'state_target', 'target_reached',)
    list_filter = (StateTargetFilter, ServiceFilter, UserFilter,)

    def get_readonly_fields(self, request, obj=None):
        return list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))

class DeploymentAdmin(PolymorphicParentModelAdmin):
    base_model = Deployment  # Explicitly set here!
    show_in_index = True  # makes child model admin visible in main admin site
    child_models = (VODeployment, ServiceDeployment)
    list_display = ('id', 'user', 'state_target', 'target_reached',)
    list_filter = (StateTargetFilter, UserFilter,)

    def get_readonly_fields(self, request, obj=None):
        return list(set(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        ))

class DeploymentStateAdmin(ReadonlyAdmin):
    list_display = ('id', 'user', 'site', 'service', 'state')
    list_filter = (StateFilter, ServiceFilter, UserFilter,)

class CredentialStateAdmin(ReadonlyAdmin):
    list_display = ('id', 'state_target', 'state', 'credential',)

site.register(VODeployment, VODeploymentAdmin)
site.register(ServiceDeployment, ServiceDeploymentAdmin)
site.register(Deployment, DeploymentAdmin)
site.register(DeploymentState, DeploymentStateAdmin)
site.register(CredentialState, CredentialStateAdmin)
# }}}

# other models {{{
class SiteAdmin(ModelAdmin):
    list_display = ('name', 'client', 'description')

class ServiceAdmin(ModelAdmin):
    list_display = ('name', 'site', 'contact_email', 'description')
    readonly_fields = ('name', 'site', 'vos',)

class SSHPublicKeyAdmin(ReadonlyAdmin):
    list_display = ('name', 'user',)
    list_filter = (UserFilter,)

site.register(Site, SiteAdmin)
site.register(Service, ServiceAdmin)
site.register(SSHPublicKey, SSHPublicKeyAdmin)
# }}}
