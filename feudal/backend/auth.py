
import logging
from urllib.error import HTTPError
import jwt

from rest_framework.authentication import BaseAuthentication

from feudal.backend.sessions import get_session, set_session, del_session, SessionError
from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.users import User


LOGGER = logging.getLogger(__name__)


class OIDCTokenAuthBackend:

    # get_user is part of the authentication backend API
    def get_user(self, user_id):
        try:
            return User.objects.get(
                user_type=User.TYPE_CHOICE_USER,
                pk=user_id,
            )

        except User.DoesNotExist:
            LOGGER.error('OIDCTokenAuthBackend: query for user id %s: no results', user_id)
            return None

    # raises OIDCConfig.DoesNotExist if no idp can be getd
    def _get_idp(self, request):
        # OPTION 1: issuer set in the 'X-Issuer' header
        if 'HTTP_X_ISSUER' in request.META:
            LOGGER.debug('X-Issuer header found')
            return OIDCConfig.objects.get(
                issuer_uri=request.META['HTTP_X_ISSUER'],
                enabled=True,
            )

        # OPTION 2: issuer set in users session (before redirecting to IdP)
        idp_id = get_session(request, 'idp_id', None)
        if idp_id is not None:
            LOGGER.debug('User session contains idp_id')
            return OIDCConfig.objects.get(
                id=idp_id,
                enabled=True,
            )

        # OPTION 3: read 'iss' JWT
        potential_jwt = request.META.get('HTTP_AUTHORIZATION', '').replace('Bearer ', '')
        try:
            data = jwt.decode(potential_jwt, verify=False)
            if 'iss' in data:
                LOGGER.debug('Access token is a jwt')
                return OIDCConfig.objects.get(
                    issuer_uri=data['iss'],
                    enabled=True,
                )
            LOGGER.debug("JWT access token does not contain iss field")

        except jwt.exceptions.InvalidTokenError as e:  # base exception for jwt.decode
            LOGGER.debug('Access token is probably not a JWT: %s', e)

        LOGGER.debug('_get_idp is unable to determine an IdP')
        raise OIDCConfig.DoesNotExist('Unable to get IdP')

    # _get_idp_userinfo_bruteforce tries to get an userinfo from _any_ registered idp
    # returns idp, userinfo
    def _get_idp_userinfo_bruteforce(self, access_token):
        for idp in OIDCConfig.objects.filter(enabled=True):
            try:
                return idp, idp.get_userinfo(access_token)
            except HTTPError:
                pass

        raise OIDCConfig.DoesNotExist('Unable to get IdP')

    # _get_idp_userinfo returns idp, userinfo
    def _get_idp_userinfo(self, request, access_token):
        # get idp AND userinfo
        try:
            idp = self._get_idp(request)
            userinfo = idp.get_userinfo(access_token)
            return idp, userinfo

        except OIDCConfig.DoesNotExist:  # raised in self._get_idp

            # Idp was not provided in param / session / JWT -> just try all of them
            try:
                return self._get_idp_userinfo_bruteforce(access_token)
            except OIDCConfig.DoesNotExist:
                set_session(request, 'auth_error', 'Unable determine IdP of access token. Please set the X-Issuer Header to the IdPs issuer url.')
                raise ValueError('Unable determine IdP of access token. Please set the X-Issuer Header to the IdPs issuer url.')

        except HTTPError as e:  # raised in idp.get_userinfo
            raise ValueError('Error retrieving userinfo from the IdP') from e

    #
    # authenticate is part of the authentication backend API
    # must report errors via the as 'auth_error' in the session
    def authenticate(self, request):
        if 'HTTP_AUTHORIZATION' not in request.META:
            set_session(request, 'msg', 'Not authenticated')
            set_session(request, 'auth_error', 'Authorization header is not set')
            return None

        access_token = request.META['HTTP_AUTHORIZATION']

        try:
            idp, userinfo = self._get_idp_userinfo(request, access_token)

            # gets or creates the user
            user = User.construct_from_userinfo(userinfo, idp)

            if not user.is_active:
                LOGGER.info('Login by deactivated user: %s', user)
                set_session(request, 'deactivated', True)
                set_session(request, 'msg', 'Account deactivated')
                set_session(request, 'auth_error', 'Account deactivated')
                return None

            LOGGER.info('Authenticated: %s', user)
            # reset all error values we possibly set in the session
            del_session(request, ['deactivated', 'auth_error'])
            return user

        except (ValueError, SessionError) as e:
            LOGGER.error('authenticate: %s', e)
            set_session(request, 'msg', 'Unable to authenticate')
            set_session(request, 'auth_error', str(e))
            return None


class OIDCTokenAuthHTTPBackend(BaseAuthentication):

    def authenticate_header(self, request):
        return 'Bearer realm="openidconnect"'

    def authenticate(self, request):
        return (OIDCTokenAuthBackend().authenticate(request), None)
