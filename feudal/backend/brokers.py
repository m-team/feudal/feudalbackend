
from logging import getLogger

from pika import BlockingConnection, ConnectionParameters, BasicProperties
from pika.credentials import PlainCredentials
from django.conf import settings

LOGGER = getLogger(__name__)


class RabbitMQInstance:

    @property
    def host(self):
        return 'localhost'

    @property
    def vhost(self):
        return '/'

    @property
    def exchanges(self):
        return [
            'entitlements',
            'groups',
            'users',
            'services',
        ]

    @property
    def _no_broker(self):
        """ indicates if this module should do anything.
        This is useful during testing, when there may not be a running RabbitMQ server
        """
        return getattr(settings, 'NO_BROKER', False)

    @property
    def _params(self):
        # we set NO port here, we use the default (probably 5672)
        # this requires the
        return ConnectionParameters(
            host=self.host,
            virtual_host=self.vhost,
            credentials=PlainCredentials(
                'guest',
                'guest',
            ),
        )

    def __init__(self):
        # always initialize
        self.initialize()

    def _init_exchanges(self, channel):
        # this is no error
        for exchange in self.exchanges:
            channel.exchange_declare(
                exchange=exchange,
                durable=True,  # so exchange survive a broker restart
                auto_delete=False,
                exchange_type='topic',
            )

    def __str__(self):  # pragma: no cover
        return self.host

    def msg(self, msg):  # pragma: no cover
        return '[RabbitMQ:{}] {}'.format(self.host, msg)

    def _open_connection(self):
        try:
            # start a new connection
            return BlockingConnection(self._params)
        except Exception as e:
            LOGGER.exception('RabbitMQ connection error: %s', e)
            raise e

    def _publish(self, exchange, routing_key, body):
        # when running CI tests we have no broker, so just do nothing
        if self._no_broker:
            return

        connection = self._open_connection()

        channel = connection.channel()
        # put in confirm mode
        channel.confirm_delivery()

        channel.basic_publish(
            exchange=exchange,
            routing_key=routing_key,
            body=body,
            properties=BasicProperties(
                delivery_mode=1,
            ),
        )

        # close channel and connection
        connection.close()


    # PUBLIC API

    # called on client registration to make sure the  exchanges exists
    def initialize(self):
        # when running CI tests we have no broker, so just do nothing
        if self._no_broker:
            return

        connection = self._open_connection()
        channel = connection.channel()
        self._init_exchanges(channel)
        connection.close()

    def publish_deployment_state(self, deployment_state, msg):
        self._publish(
            'services',
            deployment_state.service.name,
            msg,
        )

    def publish_to_user(self, user, msg):
        self._publish(
            'users',
            str(user.id),
            msg,
        )
