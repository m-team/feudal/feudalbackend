# pylint: disable=line-too-long,invalid-name,unused-argument,redefined-outer-name,wildcard-import,unused-wildcard-import

import logging
import pytest

from django.test import Client, RequestFactory
from django.urls import reverse
from django.contrib.sessions.middleware import SessionMiddleware
from rest_framework.test import APIClient

from feudal.backend.brokers import RabbitMQInstance
from feudal.backend.models import Site, Service
from feudal.backend.models.deployments import DEPLOYED, VODeployment, ServiceDeployment
from feudal.backend.models.users import User
from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.auth.vos import VO, Group, Entitlement

LOGGER = logging.getLogger(__name__)

# https://docs.pytest.org/en/stable/example/parametrize.html#deferring-the-setup-of-parametrized-resources
DEPLOYMENT_TYPES = ['vo', 'service']
def pytest_generate_tests(metafunc):
    if 'deployment_type' in metafunc.fixturenames:
        metafunc.parametrize('deployment_type', DEPLOYMENT_TYPES, indirect=True)

# parametrized in pytest_generate_tests
# returns the value its parametrized with
@pytest.fixture
def deployment_type(request):
    return request.param


# AUTOUSE FIXTURE
#
@pytest.fixture(autouse=True)
def enable_db_access_for_all_tests(db):
    pass


# MODEL FIXTURES
#
@pytest.fixture
def idp(db):
    return OIDCConfig.objects.create(
        name='OIDCConfig-test-fixture',
        client_id='test_idp_client_id',
        client_secret='test_idp_secret',
        issuer_uri='https://test-idp.foo/bar/oauth2',
        scopes=['test_scope'],
        userinfo_field_entitlements='eduperson_entitlements',
        userinfo_field_groups='groups',
        enabled=True,
    )

@pytest.fixture
def idp_well_known_config(idp):
    # example json response string of an idps well known openid config
    # this example is from unity
    # format using % { 'iss': <issuer uri> }
    EXAMLPE_WELL_KNOWN_OPENID_CONFIGURATION = '{ "authorization_endpoint": "%(iss)s-as/oauth2-authz", "token_endpoint": "%(iss)s/token", "introspection_endpoint": "%(iss)s/introspect", "revocation_endpoint": "%(iss)s/revoke", "issuer": "%(iss)s", "jwks_uri": "%(iss)s/jwk", "scopes_supported": [ "credentials", "openid", "profile", "eduperson_scoped_affiliation", "eduperson_unique_id", "sn", "eduperson_assurance", "display_name", "email", "eduperson_entitlement", "eduperson_principal_name", "single-logout" ], "response_types_supported": [ "code", "token", "id_token", "code id_token", "id_token token", "code token", "code id_token token" ], "response_modes_supported": [ "query", "fragment" ], "grant_types_supported": [ "authorization_code", "implicit" ], "code_challenge_methods_supported": [ "plain", "S256" ], "request_parameter_supported": true, "request_uri_parameter_supported": false, "require_request_uri_registration": false, "tls_client_certificate_bound_access_tokens": false, "subject_types_supported": [ "public" ], "userinfo_endpoint": "%(iss)s/userinfo", "id_token_signing_alg_values_supported": [ "RS256", "ES256" ], "claims_parameter_supported": false, "frontchannel_logout_supported": false, "backchannel_logout_supported": false }'
    return EXAMLPE_WELL_KNOWN_OPENID_CONFIGURATION % {'iss': idp.issuer_uri}


@pytest.fixture
def broker():
    return RabbitMQInstance()


@pytest.fixture
def group(idp):
    return Group.get_group(
        idp=idp,
        name='Group-test-fixture',
    )

@pytest.fixture
def entitlement(idp):
    return Entitlement.get_entitlement(
        idp=idp,
        name='Entitlement-test-fixture',
    )

# if we don't care about the group/entitlement differencs we just use vo
@pytest.fixture
def vo(idp):
    return VO.objects.create(
        name='VO-test-fixture',
        idp=idp,
    )

@pytest.fixture
def userinfo(idp, entitlement, group):
    return {
        'iss': idp.issuer_uri,
        'sub':'fb0fa558-cfa2-49f9-b847-5c651d1f6135',
        'ssh_key': 'ssh-rsa AAAAB3NzaC1yhApzBpUulukg9Q== TEST_KEY',
        'name': 'Gustav Holst',
        'given_name': 'Gustav',
        'family_name': 'Holst',
        'preferred_username': 'gustav.holst',
        'email': 'gustav@holst-feudal-test.co.uk',
        idp.userinfo_field_groups: [group.name],
        idp.userinfo_field_entitlements: [entitlement.name],
    }

@pytest.fixture
def userinfo_without_lists(idp, entitlement, group):
    return {
        'iss': idp.issuer_uri,
        'sub':'fb0fa558-cfa2-49f9-b847-5c651d1f6135',
        'ssh_key': 'ssh-rsa AAAAB3NzaC1yhApzBpUulukg9Q== TEST_KEY',
        'name': 'Gustav Holst',
        'given_name': 'Gustav',
        'family_name': 'Holst',
        'preferred_username': 'gustav.holst',
        'email': 'gustav@holst-feudal-test.co.uk',
        idp.userinfo_field_groups: group.name,
        idp.userinfo_field_entitlements: entitlement.name,
    }

@pytest.fixture
def userinfo_invalid_vos(idp):
    return {
        'iss': idp.issuer_uri,
        'sub':'fb0fa558-cfa2-49f9-b847-5c651d1f6135',
        'ssh_key': 'ssh-rsa AAAAB3NzaC1yhApzBpUulukg9Q== TEST_KEY',
        'name': 'Gustav Holst',
        'given_name': 'Gustav',
        'family_name': 'Holst',
        'preferred_username': 'gustav.holst',
        'email': 'gustav@holst-feudal-test.co.uk',
        idp.userinfo_field_groups: 0,  # invalid value
        idp.userinfo_field_entitlements: 0,  # invalid value
    }

@pytest.fixture
def password():
    return 'foobarbaz'

@pytest.fixture
def downstream_client(password):
    c = User.construct_client(
        username='downstream-User-test-fixture',
        password=password,
    )
    c.save()
    return c

@pytest.fixture
def upstream_client(password, idp):
    c = User.construct_client(
        username='upstream-User-test-fixture',
        password=password,
        user_type=User.TYPE_CHOICE_UPSTREAM,
        idp=idp,
    )
    c.save()
    return c

@pytest.fixture
def user(userinfo, idp):
    u = User.construct_from_userinfo(userinfo, idp)
    assert u is not None
    u.save()
    return u

@pytest.fixture
def site(downstream_client):
    s = Site.objects.create(
        client=downstream_client,
        name='Site-test-fixture',
    )
    LOGGER.debug('Site created: %s', s)
    return s

@pytest.fixture
def service(site, group, entitlement, vo):
    s = Service.get_service(
        name='Service-test-fixture',
        site=site,
        vos=[group, entitlement, vo],
    )
    assert s is not None
    s.save()
    return s

# a deployment with a state
@pytest.fixture
def dep(user, service):
    return ServiceDeployment.get_or_create(user, service)

@pytest.fixture
def dep_state(dep):
    assert dep.states.count() == 1

    return dep.states.first()


# TEST INFRASTRUCTURE
#
@pytest.fixture
def testClient():
    return Client()

@pytest.fixture
def test_client():
    return Client()

@pytest.fixture
def user_test_client(user):
    client = APIClient()
    client.force_authenticate(user=user)
    return client

@pytest.fixture
def rf():
    return RequestFactory()

@pytest.fixture
def downstream_test_client(downstream_client):
    client = APIClient()
    client.force_authenticate(user=downstream_client)
    return client

@pytest.fixture
def upstream_test_client(upstream_client):
    client = APIClient()
    client.force_authenticate(user=upstream_client)
    return client

# use session_middleware.process_request(request) to add a session to the request
@pytest.fixture
def session_middleware():
    return SessionMiddleware()



# {{{
def _patch_state(state, data, downstream_test_client):
    data['id'] = state.id

    # if the adapter does not set individual credential states the client sets the states to the deployemnts' state
    # we do the same here to replicate the clients behaviour
    if 'credential_states' not in data and 'state' in data:
        # set all the credential states to 'state'
        ssh_key_states = {}
        data['credential_states'] = {'ssh_key': ssh_key_states}

        for credential_state in state.credential_states.all():
            ssh_key_states[credential_state.credential.name] = data['state']

    downstream_test_client.patch(reverse('client-dep-state'), data, format='json')

def _typed_deployed_deployment(deployment_type, user, service, vo, downstream_test_client):
    def _deployed_deployment(user=user, service=service, vo=vo):
        if deployment_type == 'vo':
            LOGGER.debug('SETUP: Deployed %s deployment for %s', deployment_type, vo)
        else:
            LOGGER.debug('SETUP: Deployed %s deployment for %s via %s', deployment_type, service, vo)

        # get a deployment for the user
        dep = None
        if deployment_type == 'vo':
            dep = VODeployment.get_or_create(user, vo)
        else:
            dep = ServiceDeployment.get_or_create(user, service)

        assert dep is not None

        # deploy the deployment
        dep.state_target = DEPLOYED
        dep.save()
        dep.target_changed()

        assert dep.states.count() == 1  # after target_changed the DeploymentState should be created

        state = dep.states.first()

        assert not dep.target_reached  # the deployment in not yet deployed

        # deployment by the client
        data = {'state': DEPLOYED, 'message': 'Done'}
        _patch_state(state, data, downstream_test_client)

        assert dep.target_reached  # the deployment is now deployed

        user.refresh_from_db()
        service.refresh_from_db()
        dep.refresh_from_db()
        state.refresh_from_db()

        assert state.state == DEPLOYED
        assert state.state_target == DEPLOYED
        assert not state.is_orphaned
        assert not state.is_credential_pending
        assert not state.is_pending

        LOGGER.debug('END SETUP')
        return dep, state
    return _deployed_deployment

# auto parametrized by deployment_type
@pytest.fixture
def deployed_deployment(deployment_type, user, service, vo, downstream_test_client):
    return _typed_deployed_deployment(deployment_type, user, service, vo, downstream_test_client)

# unparametrized versions of deployed_deployment
@pytest.fixture
def deployed_vo_deployment(user, service, vo, downstream_test_client):
    return _typed_deployed_deployment('vo', user, service, vo, downstream_test_client)

# unparametrized versions of deployed_deployment
@pytest.fixture
def deployed_service_deployment(user, service, vo, downstream_test_client):
    return _typed_deployed_deployment('service', user, service, vo, downstream_test_client)


def _typed_pending_deployment(deployment_type, user, service, vo):
    def _pending_deployment(user=user, service=service, vo=vo):
        # get a deployment for the user
        dep = None
        if deployment_type == 'vo':
            dep = VODeployment.get_or_create(user, vo)
            assert dep.vo == vo
        else:
            dep = ServiceDeployment.get_or_create(user, service)

        assert dep is not None

        # deploy the deployment
        dep.state_target = DEPLOYED
        dep.save()
        dep.target_changed()

        assert dep.states.count() == 1  # after target_changed the DeploymentState should be created

        state = dep.states.first()

        dep.refresh_from_db()
        state.refresh_from_db()

        assert not dep.target_reached  # the deployment in not yet deployed

        return dep, state
    return _pending_deployment

# auto parametrized by deployment_type
@pytest.fixture
def pending_deployment(deployment_type, user, service, vo):
    return _typed_pending_deployment(deployment_type, user, service, vo)

# unparametrized versions of deployed_deployment
@pytest.fixture
def pending_vo_deployment(user, service, vo, downstream_test_client):
    return _typed_pending_deployment('vo', user, service, vo)

@pytest.fixture
def pending_service_deployment(user, service, vo, downstream_test_client):
    return _typed_pending_deployment('service', user, service, vo)

# }}}
