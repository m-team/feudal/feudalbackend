# Generated by Django 2.1.5 on 2019-02-08 13:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0006_remove_entitlement_full_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='credentialstate',
            name='state',
            field=models.CharField(choices=[('deployment_pending', 'Deployment Pending'), ('removal_pending', 'Removal Pending'), ('deployed', 'Deployed'), ('not_deployed', 'Not Deployed'), ('questionnaire', 'Questionnaire'), ('failed', 'Failed'), ('rejected', 'Rejected')], default='not_deployed', max_length=50),
        ),
        migrations.AlterField(
            model_name='deploymentstate',
            name='state',
            field=models.CharField(choices=[('deployment_pending', 'Deployment Pending'), ('removal_pending', 'Removal Pending'), ('deployed', 'Deployed'), ('not_deployed', 'Not Deployed'), ('questionnaire', 'Questionnaire'), ('failed', 'Failed'), ('rejected', 'Rejected')], default='not_deployed', max_length=50),
        ),
    ]
