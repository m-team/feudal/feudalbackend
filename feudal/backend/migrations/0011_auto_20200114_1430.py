# Generated by Django 2.2.7 on 2020-01-14 13:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('backend', '0010_remove_oidcconfig_redirect_uri'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='contact_description',
            field=models.CharField(blank=True, max_length=150),
        ),
        migrations.AddField(
            model_name='service',
            name='contact_email',
            field=models.CharField(blank=True, max_length=150),
        ),
    ]
