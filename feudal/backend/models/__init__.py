
from logging import getLogger

from django.db import models

from feudal.backend.models.users import User
from feudal.backend.models.auth.vos import VO

LOGGER = getLogger(__name__)


class Site(models.Model):
    client = models.OneToOneField(
        User,
        related_name='site',
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
    )
    name = models.CharField(
        max_length=150,
        unique=True,
    )
    description = models.TextField(
        max_length=300,
        blank=True,
    )

    def __str__(self):
        return self.name


class Service(models.Model):

    name = models.CharField(
        max_length=150,
        unique=True,
    )

    site = models.ForeignKey(
        Site,
        related_name='services',
        on_delete=models.CASCADE,
    )

    description = models.TextField(
        max_length=300,
        blank=True,
    )

    contact_email = models.CharField(
        max_length=150,
        blank=True,
    )

    contact_description = models.CharField(
        max_length=150,
        blank=True,
    )

    vos = models.ManyToManyField(
        VO,
        related_name='services',
        blank=True,
    )

    # the vos kwarg is solely used for the testing setup
    # default values or values to update are expected as kwargs
    @classmethod
    def get_service(cls, name, site, **kwargs):
        # non essential args
        defaults = {}
        for key in ['description', 'contact_email', 'contact_description']:
            if key in kwargs:
                defaults[key] = kwargs[key]

        service, created = cls.objects.update_or_create(
            name=name,
            site=site,
            defaults=defaults,
        )
        if created:
            LOGGER.info('Service created: %s', service)

        if 'vos' in kwargs:
            for vo in kwargs['vos']:
                if not vo in service.vos.all():
                    LOGGER.info('VO %s now permits use of service %s', vo, service)
                    service.vos.add(vo)

        return service

    def __str__(self):
        return self.name

    def remove_service(self):
        LOGGER.info('Site %s stopped providing %s. Deleting', self.site, self)
        self.delete()
