
import logging
import json
import re
import requests

from django.core.exceptions import ImproperlyConfigured
from django.core.validators import RegexValidator, URLValidator
from django.utils.deconstruct import deconstructible
from django.utils.translation import gettext_lazy as _
from django.db import models as db_models
from django.conf import settings
from django_mysql.models import JSONField

from oic.oic import Client
from oic.oic.message import RegistrationResponse
from oic.utils.authn.client import CLIENT_AUTHN_METHOD


LOGGER = logging.getLogger(__name__)

BEARER_TOKEN_EXTRACTOR = re.compile('^Bearer (.+)$')

OIDC_CLIENT = {}


@deconstructible
class IDPNameValidator(RegexValidator):
    regex = r'^[\w\-_ ]+$'
    message = (
        'Enter a valid IdP name. This value may contain only letters, '
        'numbers, and -/_/<space> characters'
    )
    flags = 0


def scopes_default():  # pragma: no cover
    return [
        'openid',
        'profile',
        'email',
        'credentials',
        'eduperson_entitlement',
    ]


class OIDCConfig(db_models.Model):
    name = db_models.CharField(
        max_length=200,
        validators=[
            IDPNameValidator(),
        ],
    )

    client_id = db_models.CharField(max_length=200)
    client_secret = db_models.CharField(max_length=200)

    issuer_uri = db_models.CharField(
        max_length=200,
        validators=[
            URLValidator(schemes=['https']),
        ],
    )
    enabled = db_models.BooleanField(default=False)

    @property
    def redirect_uri(self):
        return settings.OIDC_REDIRECT_URI

    # scopes as a list of strings
    scopes = JSONField(
        default=scopes_default,
        editable=True,
        help_text='The scopes we use when requesting user infos',
    )

    # The field in the userinfo (served by this IdP) that describes groups of the user
    userinfo_field_groups = db_models.CharField(
        max_length=200,
        help_text="""The field in the userinfo (served by this IdP) that contains groups of the user.
        Leave blank if you don't want to use groups of this IdP""",
        default='',
        blank=True,
    )

    # The field in the userinfo (served by this IdP) that describes entitlements of the user
    userinfo_field_entitlements = db_models.CharField(
        max_length=200,
        help_text="""The field in the userinfo (served by this IdP) that contains entitlements of the user.
        Leave blank if you don't want to use entitlements of this IdP""",
        default='',
        blank=True,
    )

    @property
    def oidc_client(self):
        # create the client object if does no yet exist
        if self.id not in OIDC_CLIENT:
            LOGGER.info('Loading new oidc client for idp %s', self)
            new_oidc_client = Client(client_authn_method=CLIENT_AUTHN_METHOD)
            new_oidc_client.provider_config(self.issuer_uri)
            info = {
                'client_id': self.client_id,
                'client_secret': self.client_secret,
            }
            new_oidc_client.store_registration_info(RegistrationResponse(**info))

            pi = new_oidc_client.provider_info

            # sometimes the configured self.issuer_uri and the actual issuer uri (as discovered by oic) differ
            # e.g. by a trailing slash. We simply correct the configured value
            if 'issuer' in pi and self.issuer_uri != pi['issuer']:
                LOGGER.info('AUTOMATICALLY FIXING ISSUER: %s -> %s', self.issuer_uri, pi['issuer'])
                self.issuer_uri = pi['issuer']
                self.save()

            OIDC_CLIENT[self.id] = new_oidc_client

        return OIDC_CLIENT[self.id]

    @property
    def provider_info(self):
        return self.oidc_client.provider_info

    def __str__(self):  # pragma: no cover
        if not self.enabled:
            return '{} (disabled)'.format(self.name)

        return self.name

    def get_auth_request(self, state, nonce):
        args = {
            'client_id': self.client_id,
            'response_type': 'code',
            'scope': self.scopes,
            'redirect_uri': self.redirect_uri,
            'state': state,
            'nonce': nonce,
        }

        auth_req = self.oidc_client.construct_AuthorizationRequest(
            request_args=args,
        )
        return auth_req.request(self.oidc_client.authorization_endpoint)

    # raises HttpError on error
    def get_userinfo(self, access_token):
        # as the at is put into the Authorization header we always need the bearer prefix
        if not access_token.startswith('Bearer '):
            access_token = 'Bearer ' + access_token

        headers = {'Authorization': access_token}
        resp = requests.get(self.provider_info['userinfo_endpoint'], headers=headers)

        userinfo = resp.json()

        # only log when in debug mode, as prod should never log userinfos
        if settings.DEBUG:
            formatted_userinfo = json.dumps(userinfo, sort_keys=True, indent=4)
            LOGGER.debug('Userinfo from idp %s:\n%s', self, formatted_userinfo)

        userinfo['iss'] = self.provider_info['issuer']

        return userinfo

    def delete(self, using=None, keep_parents=False):
        # prevent django polymorphic cascade deletions issues that occur for model VO
        for vo in self.vos.all():
            LOGGER.debug('Deleting %s causes the deleting of %s', self, vo)
            vo.delete()

        super().delete(using=using, keep_parents=keep_parents)

    # called from an admin action
    def flush(self):
        LOGGER.info('ADMIN ACTION: flushing %s', self)
        for vo in self.vos.all():
            LOGGER.info('Deleting %s', vo)
            vo.delete()

        for user in self.users.filter(user_type='oidcuser'):
            LOGGER.info('Deleting %s', user)
            user.delete()

        LOGGER.info('END ADMIN ACTION')


def default_idp():
    available_idps = OIDCConfig.objects.filter(enabled=True)
    if not available_idps.exists():
        raise ImproperlyConfigured('No OIDCConfig available')
    return available_idps.first()
