# we don't need to deserialize, so we do not implement the abstract methods
# pylint: disable=abstract-method

from rest_framework import serializers
from rest_polymorphic.serializers import PolymorphicSerializer

from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.auth.vos import VO, Group, Entitlement, EntitlementNameSpace


class OIDCConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = OIDCConfig
        fields = (
            'name',
            'id',
            'issuer_uri',
        )


class AuthInfoSerializer(serializers.Serializer):
    idps = OIDCConfigSerializer(many=True)
    default = serializers.IntegerField()


class EntitlementNameSpaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = EntitlementNameSpace
        fields = (
            'name',
        )


# polymorphic serializer
VO_FIELDS = (
    'id',
    'name',
    'pretty_name',
    'description',
)


class AbstractVOSerializer(serializers.ModelSerializer):
    class Meta:
        model = VO
        fields = VO_FIELDS


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = VO_FIELDS


class EntitlementSerializer(serializers.ModelSerializer):
    name_space = EntitlementNameSpaceSerializer()

    class Meta:
        model = Entitlement
        fields = VO_FIELDS + (
            'name_space',
            'group_authority',
            'full_name',
        )


class VOSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        VO: AbstractVOSerializer,
        Group: GroupSerializer,
        Entitlement: EntitlementSerializer,
    }
