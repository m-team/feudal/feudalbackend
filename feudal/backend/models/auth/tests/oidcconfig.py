# pylint: disable=global-statement,protected-access
import logging
import json

import requests
from requests import Response

import pytest

from django.core.exceptions import ImproperlyConfigured

from oic.oauth2 import Client as oauth_client

from feudal.backend.models.auth import default_idp, OIDC_CLIENT
from feudal.backend.models.auth.vos import Group, Entitlement
from feudal.backend.models.users import User

LOGGER = logging.getLogger(__name__)



def test_missing_default_idp():
    ''' without setup there is default idp '''

    with pytest.raises(ImproperlyConfigured):
        default_idp()

def test_existing_default_idp(idp):
    ''' default idp is the first exsting idp '''

    di = default_idp()
    assert di.id == idp.id


def test_existing_oidc_client(idp):
    global OIDC_CLIENT
    # load the 'client' into the global cache
    OIDC_CLIENT[idp.id] = 'foo'

    oidc_client = idp.oidc_client
    assert oidc_client == 'foo'


def test_oidc_client(monkeypatch, idp, idp_well_known_config, userinfo):

    def mock_well_known_config_request(*args, **kwargs):
        LOGGER.debug('mock_well_known_config_request: args = %s,  kwargs = %s', args, kwargs)
        r = Response()
        r._content = bytes(idp_well_known_config, encoding='utf-8')
        r.status_code = 200
        return r

    def mock_userinfo_request(*args, **kwargs):
        LOGGER.debug('mock_userinfo_request: args = %s,  kwargs = %s', args, kwargs)
        r = Response()
        r._content = bytes(json.dumps(userinfo), encoding='utf-8')
        r.status_code = 200
        return r

    # mock the actual request
    monkeypatch.setattr(oauth_client, 'http_request', mock_well_known_config_request)

    oidc_client = idp.oidc_client
    assert oidc_client is not None
    LOGGER.debug('Got oidc_client: %s', oidc_client)

    auth_req = idp.get_auth_request('state', 'nonce')
    assert auth_req is not None
    LOGGER.debug('Auth request: %s', auth_req)

    # mock the actual request
    monkeypatch.setattr(requests, 'get', mock_userinfo_request)
    at = 'test-access-token'
    ui = idp.get_userinfo(at)
    assert ui.get('sub') == userinfo.get('sub')


def test_issuer_fix(monkeypatch, idp, idp_well_known_config):
    correct_issuer = idp.issuer_uri
    # intentionally 'break' the issuer
    idp.issuer_uri = idp.issuer_uri + '/'
    idp.save()

    def mock_well_known_config_request(*args, **kwargs):
        LOGGER.debug('mock_well_known_config_request: args = %s,  kwargs = %s', args, kwargs)
        r = Response()
        r._content = bytes(idp_well_known_config, encoding='utf-8')
        r.status_code = 200
        return r

    # mock the actual request
    monkeypatch.setattr(oauth_client, 'http_request', mock_well_known_config_request)

    # upon loading the oidc client the issuer should get fixed
    oidc_client = idp.oidc_client
    assert oidc_client is not None
    LOGGER.debug('Got oidc_client: %s', oidc_client)

    idp.refresh_from_db()
    assert idp.issuer_uri == correct_issuer


def test_idp_flush(idp, user, group, entitlement):
    idp.flush()

    assert not User.objects.filter(pk=user.pk).exists()
    assert not Group.objects.filter(pk=group.pk).exists()
    assert not Entitlement.objects.filter(pk=entitlement.pk).exists()
