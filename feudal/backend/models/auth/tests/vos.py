# pylint: disable=unused-argument,too-many-arguments

import logging
import pytest

from feudal.backend.models.auth.vos import Entitlement, Group
from feudal.backend.models.auth import serializers as webpage_serializers
from feudal.backend.models.auth.serializers import clients as client_serializers

LOGGER = logging.getLogger(__name__)


@pytest.mark.parametrize(
    'raw_entitlement,group_name,role,group_authority',
    [
        ('urn:mace:egi.eu:group:vo.indigo-datacloud.eu:role=member#aai.egi.eu', 'vo.indigo-datacloud.eu:role=member', 'member', 'aai.egi.eu'),
        ('urn:mace:egi.eu:aai.egi.eu:member@goc.egi.eu', '', '', ''),
    ],
)
def test_get_entitlement(raw_entitlement, group_name, role, group_authority, idp):
    assert not Entitlement.exists(raw_entitlement)
    entitlement = Entitlement.get_entitlement(raw_entitlement)
    LOGGER.debug('Created: %s', str(entitlement))

    pk = entitlement.id

    assert entitlement.role == role
    assert entitlement.group_authority == group_authority
    assert Entitlement.exists(raw_entitlement)
    assert repr(entitlement) == raw_entitlement

    # now the entilement exists
    entilement = Entitlement.get_entitlement(raw_entitlement)
    assert entilement.id == pk

    # now the entilement exists and we add a idp
    entilement = Entitlement.get_entitlement(raw_entitlement, idp=idp)
    assert entilement.id == pk

    if group_authority == '':
        # add  a group_authority
        ga = 'aai.egi.eu'
        entilement = Entitlement.get_entitlement(raw_entitlement+'#'+ga, idp=idp)
        assert entilement.id == pk
        assert entilement.group_authority == ga

    # check pretty name
    assert entitlement.pretty_name == entitlement.name.replace(':', ' : ')

    entitlement.alias = 'foo'
    entitlement.save()
    assert entitlement.pretty_name == 'foo'


@pytest.mark.parametrize(
    'serializers, fields',
    [
        (webpage_serializers, ['id', 'name', 'pretty_name', 'description', 'name_space', 'group_authority', 'full_name']),
        (client_serializers, ['name', 'name_space', 'group_authority']),
    ],
)
@pytest.mark.parametrize(
    'raw_entitlement, group_name, role, group_authority',
    [
        ('urn:mace:egi.eu:group:vo.indigo-datacloud.eu:role=member#aai.egi.eu', 'vo.indigo-datacloud.eu:role=member', 'member', 'aai.egi.eu'),
        ('urn:mace:egi.eu:aai.egi.eu:member@goc.egi.eu', '', '', ''),
    ],
)
def test_entitlement_serializer(serializers, fields, raw_entitlement, group_name, role, group_authority):
    entitlement = Entitlement.get_entitlement(raw_entitlement)
    serializer = serializers.EntitlementSerializer(entitlement)
    data = serializer.data
    assert data is not None
    for key in fields:
        assert key in data

    serializer = serializers.VOSerializer(entitlement)
    data = serializer.data
    assert data is not None
    for key in fields:
        assert key in data


@pytest.mark.parametrize(
    'raw_group',
    [
        'myExampleColab',
    ],
)
def test_get_group(raw_group, idp):
    group = Group.get_group(raw_group)
    pk = group.id
    assert repr(group) == raw_group

    # now apply an idp
    group = Group.get_group(raw_group, idp)
    assert group.id == pk
    assert group.idp == idp


@pytest.mark.parametrize(
    'serializers, fields',
    [
        (webpage_serializers, ['id', 'name', 'pretty_name', 'description']),
        (client_serializers, ['name']),
    ],
)
@pytest.mark.parametrize(
    'raw_group',
    [
        'myExampleColab',
    ],
)
def test_group_serializer(serializers, fields, raw_group):
    group = Group.get_group(raw_group)
    LOGGER.debug('Created: %s', str(group))

    serializer = serializers.GroupSerializer(group)
    data = serializer.data
    assert data is not None
    for key in fields:
        assert key in data

    serializer = serializers.VOSerializer(group)
    assert data is not None
    for key in fields:
        assert key in data
