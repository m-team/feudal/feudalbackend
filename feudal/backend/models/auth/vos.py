
import logging
import re

from polymorphic.models import PolymorphicModel
from django.db import models

from feudal.backend.models.auth import OIDCConfig

LOGGER = logging.getLogger(__name__)


class EntitlementNameSpace(models.Model):

    name = models.CharField(
        max_length=200,
        unique=True,
    )

    @classmethod
    def get_name_space(cls, name):
        try:
            return cls.objects.get(
                name=name,
            )
        except cls.DoesNotExist:
            name_space = cls(
                name=name,
            )

            name_space.save()
            return name_space

    def __str__(self):  # pragma: no cover
        return self.name


class VO(PolymorphicModel):

    name = models.CharField(
        max_length=200,
        unique=True,
    )

    idp = models.ForeignKey(
        OIDCConfig,
        related_name='vos',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
    )

    description = models.TextField(
        max_length=1000,
        blank=True,
        null=True,
    )

    @property
    def pretty_name(self):
        return self.name

    def __str__(self):
        return self.name


class Group(VO):

    @classmethod
    def get_group(cls, name, idp=None):
        try:
            # searching only by name!
            group = cls.objects.get(
                name=name,
            )

            # use case: a client caused the group to be created
            # we apply the idp here
            if group.idp is None and idp is not None:
                group.idp = idp
                group.save()

            return group

        except cls.DoesNotExist:
            group = cls(
                name=name,
                idp=idp,
            )

            group.save()
            return group

    @property
    def pretty_name(self):
        return self.name

    def __str__(self):
        return 'VO-GROUP-'+self.name

    def __repr__(self):
        return self.name


class Entitlement(VO):

    name_space = models.ForeignKey(
        EntitlementNameSpace,
        related_name='entitlements',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    group_authority = models.CharField(
        max_length=200,
        blank=True,
        null=True,
    )

    role = models.CharField(
        max_length=200,
        blank=True,
        null=True,
    )

    # optional
    alias = models.CharField(
        max_length=200,
        blank=True,
        null=True,
    )

    @property
    def pretty_name(self):
        if self.alias is not None:
            return self.alias

        # the removal of the group component made the entitlements inconsistent
        # so sadly we can't keep doing it
        # name_search = re.search('group:(.*)$', self.name)
        # if name_search and name_search.group(1) != '':
        #     return name_search.group(1)

        # pad the separators to increase readability
        return self.name.replace(':', ' : ')

    @property
    def full_name(self):
        return self.__repr__()

    @staticmethod
    def extract_name(raw_name):
        name_search = re.search('^(.*)#', raw_name)
        if name_search:
            return name_search.group(1)
        return raw_name

    @staticmethod
    def extract_group_authority(raw_name):
        group_authority_search = re.search('#(.*)$', raw_name)
        if group_authority_search:
            return group_authority_search.group(1)
        return ''

    @staticmethod
    def extract_role(raw_name):
        role_search = re.search(':role=(.*)#', raw_name)
        if role_search:
            return role_search.group(1)
        return ''

    @classmethod
    def get_entitlement(cls, name, idp=None):
        try:
            # searching only by name! not idp
            entitlement = cls.objects.get(
                name=cls.extract_name(name),
            )

            # group authority changed / added
            group_authority = cls.extract_group_authority(name)
            if group_authority not in ('', entitlement.group_authority):
                entitlement.group_authority = group_authority
                entitlement.save()

            # use case: a client caused the group to be created
            # we apply the idp here
            if entitlement.idp is None and idp is not None:
                entitlement.idp = idp
                entitlement.save()

            return entitlement

        except cls.DoesNotExist:
            entitlement = cls(
                name=cls.extract_name(name),
                group_authority=cls.extract_group_authority(name),
                role=cls.extract_role(name),
                idp=idp,
            )

            name_space_search = re.search('^(.*):group', name)
            if name_space_search:
                entitlement.name_space = EntitlementNameSpace.get_name_space(name_space_search.group(1))

            entitlement.save()
            return entitlement

    @classmethod
    def exists(cls, raw_name):
        try:
            cls.objects.get(
                name=cls.extract_name(raw_name),
            )
            return True
        except cls.DoesNotExist:
            return False

    def __str__(self):
        return 'VO-ENTITLEMENT-'+self.pretty_name

    def __repr__(self):
        if self.group_authority:
            return '{}#{}'.format(self.name, self.group_authority)
        return self.name
