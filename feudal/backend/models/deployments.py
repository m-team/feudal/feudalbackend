# pylint: disable=import-outside-toplevel,bare-except

import types

from json import dumps
from logging import getLogger

from django.conf import settings
from django.db import models
from django.db.models.signals import pre_delete
from django.dispatch import receiver

from django_mysql.models import JSONField
from polymorphic.models import PolymorphicModel

from feudal.backend.models import Site, Service
from feudal.backend.models.users import User, SSHPublicKey
from feudal.backend.models.auth.vos import VO
from feudal.backend.brokers import RabbitMQInstance


LOGGER = getLogger(__name__)

MAX_FAILED_ATTEMPTS = 10

DEPLOYMENT_PENDING = 'deployment_pending'
REMOVAL_PENDING = 'removal_pending'
NOT_DEPLOYED = 'not_deployed'
DEPLOYED = 'deployed'
QUESTIONNAIRE = 'questionnaire'
FAILED = 'failed'
FAILED_PERMANENTLY = 'failed_permanently'
REJECTED = 'rejected'

TARGET_CHOICES = (
    (DEPLOYED, 'Deployed'),
    (NOT_DEPLOYED, 'Not Deployed'),
)
STATE_CHOICES = (
    (DEPLOYMENT_PENDING, 'Deployment Pending'),
    (REMOVAL_PENDING, 'Removal Pending'),
    (DEPLOYED, 'Deployed'),
    (NOT_DEPLOYED, 'Not Deployed'),
    (QUESTIONNAIRE, 'Questionnaire'),
    (FAILED, 'Failed'),
    (FAILED_PERMANENTLY, 'Failed Permanently'),
    (REJECTED, 'Rejected'),
)


def questionnaire_default():  # pragma: no cover
    return {}

def answers_default():  # pragma: no cover
    return {}

def credential_default():  # pragma: no cover
    return {}

def user_info_default():  # pragma: no cover
    return {}


class Deployment(PolymorphicModel):
    user = models.ForeignKey(
        User,
        related_name='deployments',
        # deleting all deployments causes the DeploymentState to be orphaned -> removes deployments from the sites
        on_delete=models.CASCADE,
    )

    # which state do we currently want to reach?
    state_target = models.CharField(
        max_length=50,
        choices=TARGET_CHOICES,
        default=NOT_DEPLOYED,
    )

    is_active = models.BooleanField(
        default=True,
    )

    @property
    def state(self):
        if self.states.exists():
            _state = ''
            for state in self.states.all():
                if _state == '':
                    _state = state.state
                elif _state != state.state:
                    return 'mixed'

            return _state

        # if we have no states we have nothing to do
        return self.state_target

    @property
    def target_reached(self):
        return self.state_target == self.state

    # call when you changed Deployment.state_target
    def target_changed(self):
        LOGGER.debug(self.msg('Target changed to {}'.format(self.state_target)))

        for item in self.states.all():
            item.dep_target_changed()

        self.publish_to_user()

    def user_credential_added(self, key):
        for item in self.states.all():
            item.user_credential_added(key)

        if self.state_target == DEPLOYED:
            # TODO
            # self.publish()
            pass

    def user_credential_removed(self, key):
        for item in self.states.all():
            item.user_credential_removed(key)

        if self.state_target == DEPLOYED:
            # TODO
            # self.publish()
            pass

    # sends a state update via RabbitMQ / STOMP to the users webpage instance
    def publish_to_user(self):
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('publish_to_user: {}'.format(self.state_target)))

        from feudal.backend.models.serializers import UpdateSerializer
        RabbitMQInstance().publish_to_user(
            self.user,
            dumps(UpdateSerializer({'deployment': self}).data),
        )

    def msg(self, msg):  # pragma: no cover
        try:
            return '{} - {}'.format(self, msg)
        except:  # this call breaks sometimes when deleting from the django admin
            return 'broken-deployment'

    def __str__(self):  # pragma: no cover
        return 'Abstr. Deployment:({})#{}'.format(
            self.user,
            self.id,
        )


class VODeployment(Deployment):
    vo = models.ForeignKey(
        VO,
        related_name='vo_deployments',
        on_delete=models.CASCADE,
    )

    @property
    def services(self):
        return self.vo.services.all()

    @classmethod
    def get_or_create(cls, user, vo):
        deployment, created = cls.objects.get_or_create(
            user=user,
            vo=vo,
        )

        if created:
            LOGGER.debug(deployment.msg('Created'))
        else:
            LOGGER.debug(deployment.msg('Accessed'))

        LOGGER.debug('VO has services: %s', vo.services.all())
        for service in vo.services.all():
            DeploymentState.get_or_create(user, service, deployment)

        return deployment

    def service_added(self, service):
        LOGGER.debug(self.msg('Adding service {}'.format(service)))
        state, _ = DeploymentState.get_or_create(self.user, service, self)

        if state is not None and self.state_target == DEPLOYED:
            state.publish_to_client()
            state.publish_to_user()

    def __str__(self):
        return 'VO-Dep: ({}:{})#{}'.format(
            self.vo,
            self.user,
            self.id,
        )


class ServiceDeployment(Deployment):
    service = models.ForeignKey(
        Service,
        related_name='service_deployments',
        on_delete=models.CASCADE,
    )

    @classmethod
    def get_or_create(cls, user, service):
        deployment, created = cls.objects.get_or_create(user=user, service=service)

        if created:
            LOGGER.debug(deployment.msg('Created'))
        else:
            LOGGER.debug(deployment.msg('Accessed'))

        DeploymentState.get_or_create(user, service, deployment)

        return deployment

    def __str__(self):
        return 'Service-Dep: ({}:{})#{}'.format(
            self.service,
            self.user,
            self.id,
        )

@receiver(pre_delete, sender=VODeployment)
@receiver(pre_delete, sender=ServiceDeployment)
def inform_states_about_deletion(sender, instance=None, **kwargs):
    _ = sender
    _ = kwargs

    states = list(instance.states.all())
    LOGGER.debug(instance.msg('Informing states about my deletion: {}'.format(states)))

    instance.state_target = NOT_DEPLOYED
    instance.save()
    instance.target_changed()


class DeploymentState(models.Model):
    deployments = models.ManyToManyField(
        Deployment,
        related_name='states',
    )

    user = models.ForeignKey(
        User,
        related_name='states',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    userinfo = JSONField(
        default=user_info_default,
        null=True,
        blank=True,
        editable=False,
    )

    site = models.ForeignKey(
        Site,
        related_name='states',
        on_delete=models.CASCADE,
    )

    service = models.ForeignKey(
        Service,
        related_name='states',
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )

    service_name = models.CharField(
        max_length=200,
    )

    state = models.CharField(
        max_length=50,
        choices=STATE_CHOICES,
        default=NOT_DEPLOYED,
    )

    # message for the user
    message = models.TextField(
        default='',
    )

    # questions for the user (needed for deployment
    questionnaire = JSONField(
        null=True,
        blank=True,
    )

    # questionnaire_answers allows control over the users answers
    questionnaire_answers = JSONField(
        null=True,
        blank=True,
    )

    # the users answers for the questionnaire
    answers = JSONField(
        null=True,
        blank=True,
    )

    # credentials for the service
    # only valid when state == deployed
    credentials = JSONField(
        default=credential_default,
        null=True,
        blank=True,
    )

    # counts the number of failed deployments
    failed_attempts = models.SmallIntegerField(
        default=0,
    )

    @property
    def state_target(self):
        if not self.is_orphaned:
            for deployment in self.deployments.all():
                if deployment.state_target == DEPLOYED:
                    return DEPLOYED

        return NOT_DEPLOYED

    @property
    def is_orphaned(self):
        return not self.deployments.exists() or self.user is None or self.service is None

    @property
    def is_pending(self):
        if self.is_orphaned:
            return True  # pending until deleted

        # When in these states we are not pending
        if self.state in [QUESTIONNAIRE, REJECTED, FAILED_PERMANENTLY]:
            return False

        return self.state_target != self.state or self.is_credential_pending

    @property
    def is_pending_for_restarting_client(self):
        return self.is_pending or self.state in [FAILED_PERMANENTLY]

    @property
    def is_credential_pending(self):
        # the credentials are not pending when in these states
        if self.state in [QUESTIONNAIRE, REJECTED, FAILED_PERMANENTLY]:
            return False

        for credential_state in self.credential_states.all():
            if credential_state.is_pending:
                return True

        return False

    @classmethod
    def get_or_create(cls, user, service, deployment):
        state, created = cls.objects.get_or_create(
            user=user,
            service=service,
            defaults={
                'userinfo': user.userinfo,
                'service_name': service.name,
                'site': service.site,
            }
        )
        if created:
            LOGGER.debug(state.msg('Created'))

        # always bind this state to the deployment
        state.bind_to_deployment(deployment)

        return state, created

    # adds the deployment to our related deployments
    def bind_to_deployment(self, deployment):
        if not self.deployments.filter(id=deployment.id).exists():
            LOGGER.debug(self.msg('Binding to deployment {}'.format(deployment)))
            self.deployments.add(deployment)

            # we call this so the state_target of the new deployment gets propagated
            self.dep_target_changed()

    # starts tracking this the credential for this item
    def user_credential_added(self, credential):
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Adding credential {}'.format(credential.name)))

        CredentialState.get_credential_state(credential, self)

    def user_credential_removed(self, credential):
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Removing credential {}'.format(credential.name)))

        try:
            credential_state = self.credential_states.get(credential=credential)
            credential_state.credential_deleted()

        except CredentialState.DoesNotExist:
            LOGGER.error(self.msg('Credential {} has no CredentialState'.format(credential)))

    # STATE TRANSITIONS
    def _apply_transition(self, state_transitions):
        # state transitions are of type:  state_target -> state -> transition
        # transition can be ( None | ( callable | state )+ )

        transition = state_transitions.get(self.state_target, {}).get(self.state, None)

        # execute the transition
        if transition is not None:
            tparts = [transition]
            if isinstance(transition, list):
                tparts = transition

            for tpart in tparts:
                if isinstance(tpart, types.MethodType):
                    tpart()
                else:
                    self._set_state(tpart)
        else:
            LOGGER.debug(self.msg('No transition to apply for: Target = {} - State = {}'.format(self.state_target, self.state)))

    # called when a client patches parts of this state
    def state_changed(self):
        self.audit_log_response()

        LOGGER.debug(self.msg('Patched by client: {} - {}'.format(self.state, self.message)))
        self.publish_to_user()

        if self.state == NOT_DEPLOYED and self.is_orphaned:
            LOGGER.debug(self.msg('Deleting'))

            # don't delete 'self' but the DepState via its id
            DeploymentState.objects.get(id=self.id).delete()
            return

        # count the number of failed attempts
        if self.state == FAILED:
            self.failed_attempts += 1
            self.save()
            LOGGER.debug(self.msg('Failed attempts: {}'.format(self.failed_attempts)))

            if self.failed_attempts > MAX_FAILED_ATTEMPTS:
                self._set_state(FAILED_PERMANENTLY)
        else:
            # reset the failed attempts if not failure
            self.failed_attempts = 0
            self.save()

    # called when one of our deployments changes its state_target or when we are first created
    def dep_target_changed(self):
        LOGGER.debug(self.msg('Deployment changed'))

        self._assure_credential_states_exist()
        for cred_state in self.credential_states.all():
            cred_state.set_target(self.state_target)

        # target -> state -> transition
        state_transitions = {
            DEPLOYED: {
                NOT_DEPLOYED:       [DEPLOYMENT_PENDING, self.publish_to_client],     # needs to get deployed
                REMOVAL_PENDING:    DEPLOYED,               # aborting a removal
                DEPLOYED:           None,                   # already reached correct state
                DEPLOYMENT_PENDING: None,                   # already on the way to the correct state
                REJECTED:           None,                   # client will not execute the deployment
                FAILED:             None,                   # the client will retry this deployment via polling
                FAILED_PERMANENTLY: None,                   # the client will retry this deployment when it restarts
                QUESTIONNAIRE:      None,                   # needs answers
            },
            NOT_DEPLOYED: {
                DEPLOYED:           [REMOVAL_PENDING, self.publish_to_client],    # needs to get removed
                DEPLOYMENT_PENDING: NOT_DEPLOYED,           # aborting a deployment
                NOT_DEPLOYED:       None,                   # already reached correct state
                REMOVAL_PENDING:    None,                   # already on the way to the correct state

                # these four states indicate that the last deployment run was not successful
                # we therefore can turn around an say that the we reached the NOT_DEPLOYED state
                FAILED:             NOT_DEPLOYED,
                FAILED_PERMANENTLY: NOT_DEPLOYED,
                REJECTED:           NOT_DEPLOYED,
                QUESTIONNAIRE:      NOT_DEPLOYED,
            },
        }
        self._apply_transition(state_transitions)

    # called by the user views when the answers where patched
    def answers_changed(self):
        # state_target -> state -> transition
        state_transitions = {
            DEPLOYED: {
                DEPLOYED:       [DEPLOYMENT_PENDING, self.publish_to_client],   # the user updated the answers
                FAILED:         [DEPLOYMENT_PENDING, self.publish_to_client],   # maybe changed answers solve the problem
                QUESTIONNAIRE:  [DEPLOYMENT_PENDING, self.publish_to_client],   # answers were needed for deploying
            },
            NOT_DEPLOYED: {
                QUESTIONNAIRE:  [REMOVAL_PENDING, self.publish_to_client],      # answers were needed for removing
            },
        }
        self._apply_transition(state_transitions)

    # called by the client views when credential_states were part of a patch
    def client_credential_states(self, credential_states):
        # maps ssh key names to their state
        ssh_key_states = credential_states.get('ssh_key', {})

        for credential_state in self.credential_states.all():
            if credential_state.credential.name in ssh_key_states:
                credential_state.set(ssh_key_states[credential_state.credential.name])
            else:
                # if a key is not in the credential states we assume it was removed
                credential_state.set(NOT_DEPLOYED)

    # called by the user when the userinfo was changed (and should be updated at the services)
    def userinfo_changed(self, new_userinfo):
        # update our local copy of the userinfo
        self.userinfo = new_userinfo
        self.save()

        LOGGER.debug(self.msg('Userinfo changed'))
        state_transitions = {
            DEPLOYED: {
                DEPLOYED:       [DEPLOYMENT_PENDING, self.publish_to_client], # the deployment needs to be updated
                REJECTED:       [DEPLOYMENT_PENDING, self.publish_to_client], # the changed userinfo may have resolved the rejection
                FAILED:         [DEPLOYMENT_PENDING, self.publish_to_client], # the changed userinfo may have resolved the error
            },
        }
        self._apply_transition(state_transitions)


    ## other helpers
    #
    def _assure_credential_states_exist(self):
        # assure all user credentials have a state
        if self.user is None:
            LOGGER.debug(self.msg('_assure_credential_states_exist: User is None'))
            return

        for key in self.user.ssh_keys.all():
            CredentialState.get_credential_state(key, self)

    def _set_state(self, state):
        self._assure_credential_states_exist()

        # same state
        if self.state != state:
            LOGGER.debug(self.msg('Target: {} | State: {} -> {}'.format(self.state_target, self.state, state)))
            self.state = state

            # reset the failed attempts if not failure
            if self.state not in [FAILED, FAILED_PERMANENTLY]:
                self.failed_attempts = 0

            self.save()

            self.publish_to_user() # always publish to the use when the state changes
        else:
            LOGGER.debug(self.msg('State unchanged: {}'.format(self.state)))

    def publish_to_user(self):
        if self.user is None:
            LOGGER.debug(self.msg('publish_to_user: User is None'))
            return

        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('publish_to_user'))

        from feudal.backend.models.serializers import UpdateSerializer
        RabbitMQInstance().publish_to_user(
            self.user,
            dumps(UpdateSerializer({'deployment_state': self}).data),
        )

    def publish_to_client(self):
        self.audit_log_request()

        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('publish_to_client'))

        from feudal.backend.models.serializers.clients import DeploymentStateSerializer
        RabbitMQInstance().publish_deployment_state(
            self,
            dumps(DeploymentStateSerializer(self).data),
        )

    def msg(self, msg):
        return ' {} - {}'.format(self, msg)

    def _audit_log(self, message):
        identifier = ''
        if self.user is None:
            identifier = '{} - {} @ {}'.format(
                'DELETED_USER',
                self.service_name,
                self.site.name,
            )
        else:
            identifier = '{}{}@{} - {} @ {}'.format(
                self.user.email + ' aka ' if self.user.email else '',  # if the email is set use it as also know as
                self.user.sub,
                self.user.idp.issuer_uri,
                self.service_name,
                self.site.name,
            )

        LOGGER.log(settings.AUDIT_LOG_LEVEL, '%s : %s', identifier, message)


    def audit_log_request(self):
        self._audit_log('request - {}'.format(
            self.state_target,
        ))


    def audit_log_response(self):
        self._audit_log('response - {} - {}'.format(
            self.state,
            self.message,
        ))


    def __str__(self):
        return 'Dep-St: ({}:{})#{}'.format(
            self.service_name,
            self.site,
            self.id,
        )


class CredentialState(models.Model):
    #TODO
    # a separate state_target for CredentialState should not be needed. It can just inherit from its DeploymentState
    state_target = models.CharField(
        max_length=50,
        choices=TARGET_CHOICES,
        default=NOT_DEPLOYED
    )

    state = models.CharField(
        max_length=50,
        choices=STATE_CHOICES,
        default=NOT_DEPLOYED
    )

    credential = models.ForeignKey(
        SSHPublicKey,
        related_name='credential_states',
        on_delete=models.CASCADE,
    )

    deployment_state = models.ForeignKey(
        DeploymentState,
        related_name='credential_states',
        on_delete=models.CASCADE,
    )

    @property
    def is_pending(self):
        return self.state != self.state_target or (self.state == DEPLOYED and self.credential.deleted)

    @classmethod
    def get_credential_state(cls, credential, deployment_state):
        credential_state, created = cls.objects.get_or_create(
            credential=credential,
            deployment_state=deployment_state,
            defaults={
                'state': NOT_DEPLOYED,
                'state_target': deployment_state.state_target,
            },
        )

        if created:
            LOGGER.log(settings.DEBUGX_LOG_LEVEL, credential_state.msg('Created'))

        return credential_state

    def set_target(self, target):
        if self.state_target == target:
            LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Target already set'))
            return

        # state_target is locked, since we are marked for deletion
        if self.credential.deleted:
            LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Target is locked as my credential is deleted'))
            return

        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Target: {} -> {}'.format(self.state_target, target)))

        self.state_target = target
        self.save()

    def set(self, state):
        if state == NOT_DEPLOYED and self.credential.deleted:
            self._delete_state()
            return

        if str(self.state) == str(state) or state == self.state:
            return

        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('State: {} -> {}'.format(self.state, state)))

        self.state = state
        self.save()

    def credential_deleted(self):
        if self.state == NOT_DEPLOYED:
            self._delete_state()

        self.set_target(NOT_DEPLOYED)
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, self.msg('Marked as deleted'))

    def _delete_state(self):
        LOGGER.debug(self.msg('Deleting credential state'))
        credential = self.credential
        self.delete()

        credential.try_delete_key()

    def msg(self, message):
        return '  {} - {}'.format(self, message)

    def __str__(self):
        return 'Cred-St: ({}:{})#{}'.format(
            self.deployment_state.id,
            self.credential,
            self.id,
        )
