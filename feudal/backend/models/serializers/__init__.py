
from rest_framework.serializers import ModelSerializer, Serializer, CharField, JSONField
from rest_polymorphic.serializers import PolymorphicSerializer

from feudal.backend.models import Site, Service
from feudal.backend.models.users import User, SSHPublicKey, UserPreferences
from feudal.backend.models.deployments import CredentialState, DeploymentState, Deployment, VODeployment, ServiceDeployment

from feudal.backend.models.auth.serializers import VOSerializer


class SSHPublicKeySerializer(ModelSerializer):
    class Meta:
        model = SSHPublicKey
        fields = [
            'id',
            'name',
            'key',
        ]


class SSHPublicKeyRefSerializer(ModelSerializer):
    class Meta:
        model = SSHPublicKey
        fields = [
            'id',
            'name',
        ]


class CredentialStateSerializer(ModelSerializer):
    credential = SSHPublicKeyRefSerializer()

    class Meta:
        model = CredentialState
        fields = [
            'state',
            'state_target',
            'credential',
            'is_pending',
        ]


class SiteSerializer(ModelSerializer):
    class Meta:
        model = Site
        fields = [
            'id',
            'name',
            'description',
        ]


class ServiceSerializer(ModelSerializer):
    site = SiteSerializer()
    vos = VOSerializer(many=True)

    class Meta:
        model = Service
        fields = [
            'id',
            'name',
            'site',
            'vos',
            'description',
            'contact_email',
            'contact_description',
        ]


class CompactServiceSerializer(ModelSerializer):
    class Meta:
        model = Service
        fields = [
            'id',
            'name',
            'description',
        ]


class DeploymentStateSerializer(ModelSerializer):
    # why all the read_onlys: the rest interface exposes these fields, but they must not be changed
    answers = JSONField() # not read_only: patched by the user
    credential_states = CredentialStateSerializer(many=True, read_only=True)
    credentials = JSONField(read_only=True)
    questionnaire = JSONField(read_only=True)
    questionnaire_answers = JSONField(read_only=True)
    service = CompactServiceSerializer(read_only=True)
    site = SiteSerializer(read_only=True)

    class Meta:
        model = DeploymentState
        fields = (
            'answers',
            'credential_states',
            'credentials',
            'id',
            'is_credential_pending',
            'is_pending',
            'message',
            'questionnaire',
            'questionnaire_answers',
            'service',
            'site',
            'state',
            'state_target',
        )
        read_only_fields = (
            'state',
            'message',
        )


DEPLOYMENT_FIELDS = (
    'state_target',
    'id',
    'states',
)


class AbstractDeploymentSerializer(ModelSerializer):
    states = DeploymentStateSerializer(many=True)

    class Meta:
        model = Deployment
        fields = DEPLOYMENT_FIELDS


class VODeploymentSerializer(ModelSerializer):
    states = DeploymentStateSerializer(many=True)
    vo = VOSerializer()
    services = ServiceSerializer(many=True)

    class Meta:
        model = VODeployment
        fields = DEPLOYMENT_FIELDS + (
            'vo',
            'services',
        )


class ServiceDeploymentSerializer(ModelSerializer):
    states = DeploymentStateSerializer(many=True, read_only=True)
    service = ServiceSerializer()

    class Meta:
        model = ServiceDeployment
        fields = DEPLOYMENT_FIELDS + (
            'service',
        )


class DeploymentSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Deployment: AbstractDeploymentSerializer,
        VODeployment: VODeploymentSerializer,
        ServiceDeployment: ServiceDeploymentSerializer,
    }


class UserPreferencesSerializer(ModelSerializer):

    class Meta:
        model = UserPreferences
        fields = (
            'deployment_mode',
        )


class UserStateSerializer(ModelSerializer):
    deployments = DeploymentSerializer(many=True)
    services = ServiceSerializer(many=True)
    ssh_keys = SSHPublicKeySerializer(many=True)
    states = DeploymentStateSerializer(many=True)
    vos = VOSerializer(many=True)
    preferences = UserPreferencesSerializer()

    class Meta:
        model = User
        fields = (
            'deployments',
            'id',
            'profile_name',
            'services',
            'ssh_keys',
            'states',
            'userinfo',
            'vos',
            'preferences',
        )


# pylint: disable=abstract-method
class StateSerializer(Serializer):
    msg = CharField(
        allow_blank=True,
        required=False,
    )
    session = JSONField(
        required=False,
    )
    user = UserStateSerializer()


class UpdateSerializer(Serializer):
    error = CharField(allow_blank=True, required=False)
    deployment = DeploymentSerializer(required=False)
    deployment_state = DeploymentStateSerializer(required=False)
