# we don't need to deserialize, so we do not implement the abstract methods
# pylint: disable=abstract-method

from django_mysql.models import JSONField
from rest_framework.serializers import Serializer, ModelSerializer, DictField, ListField, CharField
from rest_polymorphic.serializers import PolymorphicSerializer

from feudal.backend.models import Service
from feudal.backend.models.users import User, SSHPublicKey
from feudal.backend.models.deployments import Deployment, VODeployment, ServiceDeployment, DeploymentState, CredentialState
from feudal.backend.models.auth.serializers.clients import VOSerializer


class ServiceSerializer(ModelSerializer):
    class Meta:
        model = Service
        fields = [
            'name',
        ]


class CredentialSerializer(ModelSerializer):
    class Meta:
        model = SSHPublicKey
        fields = [
            'id',
            'name',
            'value',
        ]


class UserSerializer(ModelSerializer):
    userinfo = JSONField()
    credentials = DictField(
        child=ListField(
            child=CredentialSerializer()
        )
    )

    class Meta:
        model = User
        fields = [
            'credentials',
            'userinfo',
        ]


class RabbitMQInstanceSerializer(Serializer):
    vhost = CharField()


DEPLOYMENT_FIELDS = (
    'id',
    'state_target',
    'user',
)


class AbstractDeploymentSerializer(ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Deployment
        fields = DEPLOYMENT_FIELDS


class VODeploymentSerializer(ModelSerializer):
    user = UserSerializer()
    vo = VOSerializer()
    services = ServiceSerializer(many=True)

    class Meta:
        model = VODeployment
        fields = DEPLOYMENT_FIELDS + (
            'vo',
            'services',
        )


class ServiceDeploymentSerializer(ModelSerializer):
    user = UserSerializer()
    service = ServiceSerializer()

    class Meta:
        model = ServiceDeployment
        fields = DEPLOYMENT_FIELDS + (
            'service',
        )


class DeploymentSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Deployment: AbstractDeploymentSerializer,
        VODeployment: VODeploymentSerializer,
        ServiceDeployment: ServiceDeploymentSerializer,
    }


class CredentialStateSerializer(ModelSerializer):

    class Meta:
        model = CredentialState
        fields = (
            'state',
            'state_target',
        )


class DeploymentStateSerializer(ModelSerializer):
    user = UserSerializer()
    userinfo = JSONField()
    service = ServiceSerializer()
    answers = JSONField()
    questionnaire = JSONField()
    questionnaire_answers = JSONField()

    class Meta:
        model = DeploymentState
        fields = (
            'id',
            'user',
            'userinfo',
            'state',
            'state_target',
            'credentials',
            'questionnaire',
            'questionnaire_answers',
            'service',
            'answers',
            'message',
        )
        read_only_fields = (
            'answers',
        )
