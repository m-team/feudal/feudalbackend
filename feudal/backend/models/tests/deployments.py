# vim: set foldmethod=marker :
# pylint: disable=redefined-outer-name

import logging
import copy
import pytest

# for the running the test client
from django.urls import reverse

from feudal.backend.models.deployments import (
    Deployment, DeploymentState, ServiceDeployment,
    REMOVAL_PENDING, DEPLOYMENT_PENDING, DEPLOYED, NOT_DEPLOYED, FAILED, FAILED_PERMANENTLY, MAX_FAILED_ATTEMPTS, QUESTIONNAIRE
)
from feudal.backend.models import serializers as webpage_serializers
# from feudal.backend.conftest import _patch_state


LOGGER = logging.getLogger(__name__)


# RETRIES {{{
# a deployment fails permanently
def test_permanent_failure(downstream_test_client, pending_vo_deployment):
    # setup a deployed deployment for the service
    _, state = pending_vo_deployment()

    path = reverse('client-dep-state')
    data = {'state': FAILED, 'id': state.id, 'message': 'error'}

    # make the deployment fail too many times
    for _ in range(0, MAX_FAILED_ATTEMPTS + 1):
        downstream_test_client.patch(path, data)


    state.refresh_from_db()
    assert state.state == FAILED_PERMANENTLY


def test_no_permanent_failure(pending_vo_deployment, downstream_test_client):
    # setup a deployed deployment for the service
    _, state = pending_vo_deployment()

    path = reverse('client-dep-state')
    data = {'state': FAILED, 'id': state.id, 'message': 'error'}

    # make the deployment fail a lot but then succeed
    for _ in range(0, MAX_FAILED_ATTEMPTS):
        downstream_test_client.patch(path, data)

    downstream_test_client.patch(path, {'state': DEPLOYED, 'id': state.id, 'message': 'success'})

    state.refresh_from_db()
    assert state.state == DEPLOYED
    assert state.failed_attempts == 0


def test_target_change_and_permanent_failure(pending_vo_deployment, downstream_test_client):
    # setup a deployed deployment for the service
    dep, state = pending_vo_deployment()

    path = reverse('client-dep-state')
    data = {'state': FAILED, 'id': state.id, 'message': 'error'}

    # make the deployment fail a lot
    for _ in range(0, MAX_FAILED_ATTEMPTS):
        downstream_test_client.patch(path, data)

    # the user doesnt want this failing service anyway
    dep.state_target = NOT_DEPLOYED
    dep.save()
    dep.target_changed()

    state.refresh_from_db()
    assert state.state == NOT_DEPLOYED
    assert state.failed_attempts == 0


def test_pending_and_permanently_failed(pending_vo_deployment, downstream_test_client):
    # setup a deployed deployment for the service
    _, state = pending_vo_deployment()

    state.state = FAILED_PERMANENTLY
    state.save()

    assert not state.is_pending
    assert not state.is_credential_pending
    assert state.is_pending_for_restarting_client

    path = reverse('client-dep-states')
    response = downstream_test_client.get(path)

    LOGGER.debug('response: %s', response)
#}}}


# REMOVAL HANDLING {{{
# we have a deployed deployment and delete the deployment manually from the db
def test_deployment_deletion_handling(deployed_vo_deployment):
    # setup a deployed deployment for the service
    dep, state = deployed_vo_deployment()

    # now we delete the deployment
    dep.delete()

    # we expect the state to still
    assert state.state_target == NOT_DEPLOYED
    assert state.is_pending


# we have a service with a deployed deployment and delete the service manually from the db
def test_service_deletion_handling(deployed_deployment, service, downstream_test_client):
    dep, state = deployed_deployment()

    # now we delete the service
    service.remove_service()

    # we _want_ that the Deployment is deleted
    if isinstance(dep, ServiceDeployment):
        assert not Deployment.objects.filter(id=dep.id).exists()
    else:
        # as vo deployments can have multiple services a deletion is not triggered here
        assert Deployment.objects.filter(id=dep.id).exists()

    # the DeploymentState should still exist so the client can remove the deployment
    assert DeploymentState.objects.filter(id=state.id).exists()
    state.refresh_from_db()

    assert state.state_target == NOT_DEPLOYED
    assert state.is_orphaned
    assert state.is_pending
    state_id = state.id

    # removal by the client
    data = {'state': NOT_DEPLOYED, 'id': state.id}
    downstream_test_client.patch(reverse('client-dep-state'), data)

    assert not DeploymentState.objects.filter(id=state_id).exists()  # the client update should've caused the deletion of the state


# we have a userinfo, idp with a deployed deployment and delete the userinfo, idp manually from the db
def test_user_deletion_handling(deployed_vo_deployment, user, downstream_test_client):
    # setup a deployed deployment for the service
    dep, state = deployed_vo_deployment()

    # now we delete the user
    user.delete()

    assert not Deployment.objects.filter(id=dep.id).exists()  # the user deletion should also delete the Deployment

    state.refresh_from_db()  # we would get errors because of the related and deleted user in the DeploymentState
    assert state.state_target == NOT_DEPLOYED
    assert state.is_pending
    state_id = state.id

    # removal by the client
    data = {'state': NOT_DEPLOYED, 'id': state.id}
    downstream_test_client.patch(reverse('client-dep-state'), data)

    assert not DeploymentState.objects.filter(id=state_id).exists()  # the client update should've caused the deletion of the state
# }}}


# TIMING ISSUES {{{
def test_remove_before_deploy(pending_vo_deployment):
    """ the user clicks remove before the deployment finishes """

    # setup a deployed deployment for the service
    dep, state = pending_vo_deployment()

    assert state.state == DEPLOYMENT_PENDING

    dep.state_target = NOT_DEPLOYED
    dep.save()
    dep.target_changed()

    # as the deployment did not finish (or even begin) we expect the state to go directly back to not deployed
    state.refresh_from_db()
    assert state.state == NOT_DEPLOYED


def test_deploy_before_remove(deployed_vo_deployment):
    """ the user clicks deploy before the removal finishes """
    # setup a deployed deployment for the service
    dep, state = deployed_vo_deployment()

    assert state.state == DEPLOYED

    dep.state_target = NOT_DEPLOYED
    dep.save()
    dep.target_changed()

    state.refresh_from_db()
    assert state.state == REMOVAL_PENDING

    dep.state_target = DEPLOYED
    dep.save()
    dep.target_changed()

    # as the removal did not finish (or even begin) we expect the state to go directly back to deployed
    state.refresh_from_db()
    assert state.state == DEPLOYED


def test_failure_on_remove(deployed_vo_deployment, downstream_test_client):
    """ the user clicks deploy before the removal finishes """
    # setup a deployed deployment for the service
    dep, state = deployed_vo_deployment()

    assert state.state == DEPLOYED

    # start removal
    dep.state_target = NOT_DEPLOYED
    dep.save()
    dep.target_changed()

    state.refresh_from_db()
    assert state.state == REMOVAL_PENDING

    data = {'state': FAILED, 'id': state.id}
    downstream_test_client.patch(reverse('client-dep-state'), data)

    # failed to remove so still pending
    state.refresh_from_db()
    assert state.is_pending


def test_questionnaire_on_remove(deployed_vo_deployment, downstream_test_client, user_test_client):
    """ the user clicks deploy before the removal finishes """
    # setup a deployed deployment for the service
    dep, state = deployed_vo_deployment()

    assert state.state == DEPLOYED

    # start removal
    dep.state_target = NOT_DEPLOYED
    dep.save()
    dep.target_changed()

    state.refresh_from_db()
    assert state.state == REMOVAL_PENDING

    data = {'state': QUESTIONNAIRE, 'questionnaire': {'foo': 'bar'}, 'id': state.id}
    downstream_test_client.patch(reverse('client-dep-state'), data, format='json')

    state.refresh_from_db()
    assert not state.is_pending
    assert state.state == QUESTIONNAIRE

    data = {'answers': {'foo': 'baz'}}
    user_test_client.patch(reverse('user-dep-state', kwargs={'id': state.id}), data, format='json')

    # now that the client has his answers he should get back to removing
    state.refresh_from_db()
    assert state.is_pending
    assert state.state == REMOVAL_PENDING


def test_login_changed_userinfo_redeploy(user, deployed_vo_deployment):
    """ the user gets a new userinfo, which should be push to the clients """
    # setup a deployed deployment for the service
    _, state = deployed_vo_deployment()

    assert state.state == DEPLOYED

    # maybe mock the login of a user changing the userinfo


    # change the userinfo
    ui = copy.deepcopy(user.userinfo)  # need to copy the dict, otherwise ui and user.userinfo refer to the same underlying dict
    ui['name'] = 'new name'

    user.update_userinfo(ui)
    assert user.userinfo['name'] == 'new name'

    # this should cause the republishing of the deployment
    state.refresh_from_db()
    assert state.is_pending
    assert state.userinfo['name'] == 'new name'


def test_upstream_changed_userinfo_redeploy(user, deployed_vo_deployment, upstream_test_client):
    """ the user gets a new userinfo, which should be push to the clients """
    # setup a deployed deployment for the service
    _, state = deployed_vo_deployment()

    assert state.state == DEPLOYED

    # The upstream client changes the userinfo
    new_userinfo = copy.deepcopy(user.userinfo)
    new_userinfo['name'] = 'new name'
    data = {'userinfo': new_userinfo}
    upstream_test_client.put(reverse('upstream-userinfo'), data, format='json')

    # this should cause the republishing of the deployment
    state.refresh_from_db()

    assert state.is_pending
    assert state.userinfo['name'] == 'new name'
# }}}


# SERIALIZERS {{{
def test_deployment_state_serializer(pending_vo_deployment):
    # setup a deployed deployment for the service
    _, state = pending_vo_deployment()

    ser = webpage_serializers.DeploymentStateSerializer(state)
    assert ser.data is not None
# }}}


# VO-Deployment and Service-Deployment interaction {{{
def test_both_service_and_vo_deployments_remove(pending_vo_deployment, pending_service_deployment, service):
    """
    The user first requests a VO-Dep which deploys a Service.
    Then an additional Service-Deployment for the same Service (which actually should change nothing)
    Then the VO-Deployment is removed.
    The Service must still be deployed because of the Service-Deployment
    """

    vo_dep, state_from_vo = pending_vo_deployment(service=service)
    assert state_from_vo.is_pending

    service_dep, state_from_service = pending_service_deployment(service=service)
    assert state_from_service.is_pending

    # these two deployments should've mapped to the same state
    assert state_from_vo.pk == state_from_service.pk
    state = state_from_vo # might as well just call it state from here on

    # remove first dep
    vo_dep.state_target = NOT_DEPLOYED
    vo_dep.save()
    vo_dep.target_changed()

    # still pending as there is still the service deployment
    assert state.is_pending

    # remove second dep
    service_dep.state_target = NOT_DEPLOYED
    service_dep.save()
    service_dep.target_changed()


    state.refresh_from_db()

    # both deployments are set NOT_DEPLOYED so the state is now pending
    assert not state.is_orphaned
    assert state.state == NOT_DEPLOYED
    assert not state.is_pending

def test_both_service_and_vo_deployments_delete(pending_vo_deployment, pending_service_deployment, service):
    """
    The same as test_both_service_and_vo_deployments_remove but we delete the deployments instead of removing them
    """

    vo_dep, state_from_vo = pending_vo_deployment(service=service)
    assert state_from_vo.is_pending

    service_dep, state_from_service = pending_service_deployment(service=service)
    assert state_from_service.is_pending

    # these two deployments should've mapped to the same state
    assert state_from_vo.pk == state_from_service.pk
    state = state_from_vo # might as well just call it state from here on

    # delete first dep
    vo_dep.delete()

    # still pending as there is still the service deployment
    assert state.is_pending

    # delete second dep
    service_dep.delete()

    state.refresh_from_db()

    # both deployments are deleted so the state is now pending
    assert state.is_orphaned
    assert state.state == NOT_DEPLOYED
    assert state.is_pending # because the state is orphaned
# }}}
