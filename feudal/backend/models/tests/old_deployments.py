# pylint: disable=line-too-long,invalid-name

import logging

from feudal.backend.tests import BaseTestCase
from feudal.backend.models import Service
from feudal.backend.models.deployments import (
    Deployment, VODeployment,
    DEPLOYED, NOT_DEPLOYED, DEPLOYMENT_PENDING, REMOVAL_PENDING,
)

LOGGER = logging.getLogger(__name__)


class DeploymentTest(BaseTestCase):

    def tearDown(self):
        Deployment.objects.all().delete()

    def check_new_deployment(self, deployment, service_count):
        self.assertIsNotNone(deployment)
        self.assertEqual(deployment.state, NOT_DEPLOYED)
        self.assertEqual(deployment.state_target, NOT_DEPLOYED)
        self.assertEqual(len(deployment.services), service_count)
        self.assertEqual(deployment.states.count(), service_count)

    def deployment_run(self, deployment, service_count):
        self.check_new_deployment(deployment, service_count)

        deployment.state_target = DEPLOYED
        deployment.save()
        deployment.target_changed()

        if service_count > 0:
            self.assertEqual(deployment.state, DEPLOYMENT_PENDING)
            self.assertFalse(deployment.target_reached)
        else:
            self.assertEqual(deployment.state, DEPLOYED)
            self.assertTrue(deployment.target_reached)

        # execute the deployment
        for state in deployment.states.all():
            self.assertEqual(state.state, DEPLOYMENT_PENDING)
            state.state = DEPLOYED
            state.save()

        self.assertEqual(deployment.state, DEPLOYED)
        self.assertTrue(deployment.target_reached)

        # start removal
        deployment.state_target = NOT_DEPLOYED
        deployment.save()
        deployment.target_changed()

        if service_count > 0:
            self.assertEqual(deployment.state, REMOVAL_PENDING)
            self.assertFalse(deployment.target_reached)
        else:
            self.assertEqual(deployment.state, NOT_DEPLOYED)
            self.assertTrue(deployment.target_reached)

        # execute removals
        for state in deployment.states.all():
            self.assertEqual(state.state, REMOVAL_PENDING)
            state.state = NOT_DEPLOYED
            state.save()
            state.state_changed()

        self.assertEqual(deployment.state, NOT_DEPLOYED)
        self.assertTrue(deployment.target_reached)

    # service count: prior to delayed service
    def deployment_run_delayed_service(self, deployment, vo, service_count):
        self.check_new_deployment(deployment, service_count)

        # start deployment
        deployment.state_target = DEPLOYED
        deployment.save()
        deployment.target_changed()


        if service_count == 0:
            self.assertFalse(deployment.states.exists())
            self.assertTrue(deployment.target_reached)
        else:
            # check constraints
            self.assertEqual(deployment.states.count(), service_count)
            self.assertFalse(deployment.target_reached)

            for state in deployment.states.all():
                self.assertEqual(state.state_target, DEPLOYED)
                self.assertEqual(state.state, DEPLOYMENT_PENDING)

        # add service
        delayed_service = Service.get_service(
            'DELAYED_SERVICE',
            self.site,
            vos=[vo],
        )
        deployment.service_added(delayed_service)

        for state in deployment.states.all():
            self.assertEqual(state.state, DEPLOYMENT_PENDING)
            self.assertEqual(state.state_target, DEPLOYED)

        self.assertEqual(deployment.states.count(), service_count + 1)
        self.assertEqual(deployment.state, DEPLOYMENT_PENDING)

        # emulate the deployment execution
        for state in deployment.states.all():
            state.state = DEPLOYED
            state.save()

        # the deployment was not done for new_service
        self.assertEqual(deployment.state, DEPLOYED)

        # start removal
        deployment.state_target = NOT_DEPLOYED
        deployment.save()
        deployment.target_changed()

        self.assertEqual(deployment.state, REMOVAL_PENDING)

        # emulate the removal execution
        for state in deployment.states.all():
            self.assertEqual(state.state, REMOVAL_PENDING)
            state.state = NOT_DEPLOYED
            state.save()

        self.assertEqual(deployment.state, NOT_DEPLOYED)

    def test_group_with_no_service(self):
        deployment = VODeployment.get_or_create(self.user, self.group_none)
        self.deployment_run(deployment, 0)

    def test_group_with_service(self):
        deployment = VODeployment.get_or_create(self.user, self.group_one)
        self.deployment_run(deployment, 1)

    def test_group_with_two_services(self):
        deployment = VODeployment.get_or_create(self.user, self.group_two)
        self.deployment_run(deployment, 2)

    # a vo with one service gets another service *after* the user requested the deployment
    def test_group_with_delayed_service(self):
        deployment = VODeployment.get_or_create(self.user, self.group_one)
        self.deployment_run_delayed_service(deployment, self.group_one, 1)

    # a vo with two services loses one service *after* the user requested the deployment
    def test_group_with_vanishing_service(self):
        VODeployment.get_or_create(self.user, self.group_two)
