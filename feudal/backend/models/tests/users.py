# pylint: disable=redefined-outer-name,too-many-arguments

import logging
import copy

from django.urls import reverse

from feudal.backend.models import Service
from feudal.backend.models.users import User, SSHPublicKey, UserPreferences
from feudal.backend.models.deployments import DEPLOYED, NOT_DEPLOYED, CredentialState, Deployment, DeploymentState
from feudal.backend.models.auth.vos import Group, Entitlement

LOGGER = logging.getLogger(__name__)


def test_user_deactivation(deployed_deployment, user):
    _, _ = deployed_deployment()

    assert user.is_active

    user.deactivate()
    assert not user.is_active

    user.deactivate()  # redo the deactivation
    assert not user.is_active

    user.activate()
    assert user.is_active

    user.activate()  # redo the activation
    assert user.is_active

    # TODO add assertions for the user and dep and state


def test_user_userinfo_without_lists(idp, userinfo_without_lists):
    user = User.construct_from_userinfo(userinfo_without_lists, idp)
    assert user.vos.count() == 2 # non list vos should still be applied


def test_user_userinfo_invalid_vos(idp, userinfo_invalid_vos):
    user = User.construct_from_userinfo(userinfo_invalid_vos, idp)
    assert user.vos.count() == 0 # no vos applied


def test_user_key_after_deployment(deployed_deployment, idp, userinfo, downstream_test_client):
    user = User.construct_from_userinfo(userinfo, idp)
    assert user is not None

    # the user has a deployed deployment
    _, state = deployed_deployment()

    assert user.ssh_keys.count() == 1
    assert state.credential_states.count() == 1


    ssh_key = SSHPublicKey.objects.create(
        user=user,
        name='temp_test_key',
        key='ssh-rsa ..',
    )

    # add a key after the deployment is already deployed
    user.add_key(ssh_key)

    assert user.ssh_keys.count() == 2
    assert state.credential_states.count() == 2
    assert state.is_credential_pending
    assert ssh_key.credential_states.count() == 1

    credential_state = ssh_key.credential_states.first()

    assert credential_state.state_target == DEPLOYED
    assert credential_state.state == NOT_DEPLOYED
    assert credential_state.is_pending

    # deployment of the added key by the client
    downstream_test_client.patch(
        reverse('client-dep-state'),
        {'state': DEPLOYED, 'id': state.id, 'message': 'Done', 'credential_states': {'ssh_key': {ssh_key.name: DEPLOYED}}},
        format='json',
    )

    credential_state.refresh_from_db()
    assert not credential_state.is_pending
    assert credential_state.state == DEPLOYED

    # remove the key while the depl
    user.remove_key(ssh_key)

    state.refresh_from_db()
    credential_state.refresh_from_db()

    # a fetching client should not be presented with the key again
    assert ssh_key not in user.ssh_keys.all()
    assert credential_state.state == DEPLOYED
    assert credential_state.is_pending
    assert state.credential_states.count() == 2


    # removal of the key by the client
    downstream_test_client.patch(
        reverse('client-dep-state'),
        {'state': DEPLOYED, 'id': state.id, 'message': 'Done', 'credential_states': {'ssh_key': {}}},  # empty credential state -> removed credentials
        format='json',
    )

    state.refresh_from_db()

    # the patch should have caused the deletion of sshkey and its credential state
    assert state.credential_states.count() == 1
    assert not CredentialState.objects.filter(id=credential_state.id).exists()
    assert not SSHPublicKey.objects.filter(id=ssh_key.id).exists()

def test_user_group_remove(deployed_deployment, idp, userinfo, site):
    """ the user loses a vo which should cause the removal of the deployment """

    group = Group.get_group('Temp-Test-Group', idp=idp)
    service = Service.get_service(
        name='Service-temp',
        site=site,
        vos=[group],
    )

    # add group to userinfo
    test_userinfo = copy.deepcopy(userinfo)
    test_userinfo['sub'] = 'temp-sub'
    test_userinfo[idp.userinfo_field_groups] = [repr(group)]

    # userinfo contains entitlement
    user = User.construct_from_userinfo(test_userinfo, idp)
    assert user is not None

    # the user has a deployed deployment
    _, state = deployed_deployment(user=user, service=service, vo=group)

    assert state.state == DEPLOYED

    # remove the group from the userinfo and update the user
    test_userinfo[idp.userinfo_field_groups] = []
    user.update_userinfo(test_userinfo)

    state.refresh_from_db()
    assert state.is_orphaned

def test_entitlement_remove(deployed_deployment, idp, userinfo, site):
    entitlement = Entitlement.get_entitlement('Temp-Test-Entitlement', idp=idp)
    service = Service.get_service(
        name='Service-temp',
        site=site,
        vos=[entitlement],
    )

    # add group to userinfo
    test_userinfo = copy.deepcopy(userinfo)
    test_userinfo['sub'] = 'temp-sub'
    test_userinfo[idp.userinfo_field_entitlements] = [repr(entitlement)]

    # userinfo contains entitlement
    user = User.construct_from_userinfo(test_userinfo, idp)
    assert user is not None

    # the user has a deployed deployment
    _, state = deployed_deployment(user=user, service=service, vo=entitlement)

    assert state.state == DEPLOYED

    # remove the group from the userinfo and update the user
    test_userinfo[idp.userinfo_field_entitlements] = []
    user.update_userinfo(test_userinfo)

    state.refresh_from_db()
    assert state.is_orphaned

def test_user_ssh_key_change(deployed_deployment, idp, userinfo):

    # replace the group in the userinfo with a test case specific one
    test_userinfo = copy.deepcopy(userinfo)

    # userinfo contains entitlement
    user = User.construct_from_userinfo(test_userinfo, idp)
    assert user is not None

    # the user has a deployed deployment
    _, state = deployed_deployment()

    assert state.state == DEPLOYED

    # remove the group from the userinfo and update the user
    test_userinfo['ssh_key'] = 'changed-key'
    user.update_userinfo(test_userinfo)

    state.refresh_from_db()
    # TODO add assertions

def test_user_ssh_key_remove(deployed_deployment, idp, userinfo):

    # replace the group in the userinfo with a test case specific one
    test_userinfo = copy.deepcopy(userinfo)

    # userinfo contains entitlement
    user = User.construct_from_userinfo(test_userinfo, idp)
    assert user is not None

    # the user has a deployed deployment
    _, state = deployed_deployment()

    assert state.state == DEPLOYED

    # remove the group from the userinfo and update the user
    del test_userinfo['ssh_key']
    user.update_userinfo(test_userinfo)

    state.refresh_from_db()
    # TODO add assertions

def test_idp_deletion(deployed_deployment, idp, user, service, vo):
    """ we delete the idp while the user has a deployment
        the deployment must be removed
    """

    # the user has a deployed deployment
    dep, state = deployed_deployment(service=service, vo=vo)

    assert not state.is_pending
    assert state.state == DEPLOYED

    state.refresh_from_db()

    # delete the users idp. The deletion cascade should delet the user, its Deployments but not its states
    idp.delete()

    assert not User.objects.filter(pk=user.pk).exists()
    assert not Deployment.objects.filter(pk=dep.pk).exists()
    assert DeploymentState.objects.filter(pk=state.pk).exists()
    assert Service.objects.filter(pk=service.pk).exists()

    state.refresh_from_db()
    assert state.is_pending
    assert state.is_orphaned

def test_construct_from_userinfo(idp, userinfo):
    test_value = 0
    userinfo = {
        'sub': 'foo',
        'iss': idp.issuer_uri,
        'test_value': test_value,
    }
    user = User.construct_from_userinfo(userinfo, idp)

    assert user.userinfo['test_value'] == test_value

    # change the test value and see if the userinfo actually gets changed
    test_value = 1
    userinfo = {
        'sub': 'foo',
        'iss': idp.issuer_uri,
        'test_value': test_value,
    }
    user = User.construct_from_userinfo(userinfo, idp)
    assert user.userinfo['test_value'] == test_value

    assert user.preferences.deployment_mode == UserPreferences.DEPLOYMENT_MODE_BOTH

def test_user_deletion_cascade(user):
    """ the user deletion must cascade to its preferences """

    prefs_pk = user.preferences.pk

    assert UserPreferences.objects.filter(pk=prefs_pk).exists()

    user.delete()

    assert not UserPreferences.objects.filter(pk=prefs_pk).exists()
