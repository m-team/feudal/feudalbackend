# pylint: disable=unused-argument,no-member,import-outside-toplevel

import logging

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django_mysql.models import JSONField

from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.auth.vos import VO, Group, Entitlement

LOGGER = logging.getLogger(__name__)


def user_info_default():  # pragma: no cover
    return {}


class User(AbstractUser):
    USER_ALREADY_EXISTS = Exception('The user does already exist. This usually implies that the IdP changed the sub. Only possible fix: delete the old user')

    TYPE_CHOICE_DOWNSTREAM = 'apiclient'
    TYPE_CHOICE_USER = 'oidcuser'
    TYPE_CHOICE_ADMIN = 'admin'
    TYPE_CHOICE_UPSTREAM = 'upstream'

    # for __str__
    NAME_PREFIXES = {
        TYPE_CHOICE_ADMIN: 'ADMIN ',
        TYPE_CHOICE_USER: 'USER ',
        TYPE_CHOICE_DOWNSTREAM: 'DOWNSTREAM-CLIENT ',
        TYPE_CHOICE_UPSTREAM: 'UPSTREAM-CLIENT ',
    }

    TYPE_CHOICES = (
        (TYPE_CHOICE_DOWNSTREAM, 'Downstream Client'),  # clients which connect to us via pubsub
        (TYPE_CHOICE_USER, 'Webpage User'),             # normal users which logged in using the webpage
        (TYPE_CHOICE_ADMIN, 'Admin'),                   # admins of the django admin
        (TYPE_CHOICE_UPSTREAM, 'Upstream Client'),      # E.g. an idP that provides us with fresh userinfos or access tokens
    )

    user_type = models.CharField(
        max_length=20,
        choices=TYPE_CHOICES,
        default=TYPE_CHOICE_DOWNSTREAM,
    )
    sub = models.CharField(
        max_length=150,
        blank=True,
        null=True,
        editable=False,
    )
    password = models.CharField(
        max_length=150,
        blank=True,
        null=True,
    )
    # the real state of the user
    # (self.is_active is the supposed state of the user)
    _is_active = models.BooleanField(
        default=True,
        editable=False,
    )
    # the idp which authenticated the user
    idp = models.ForeignKey(
        OIDCConfig,
        related_name='users',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        editable=True,  # editable because when user_type==upstream the idp needs to be configurable in the admin
    )
    userinfo = JSONField(
        default=user_info_default,
        null=True,
        blank=True,
        editable=False,
    )

    vos = models.ManyToManyField(
        VO,
        blank=True,
    )

# PROPERTIES
    @property
    def profile_name(self):
        return (
            self.userinfo.get('email', '')
            or self.userinfo.get('name', '')
            or self.userinfo.get('sub', '')
            or self.username

        )

    # we hide deleted keys here
    # the full list of ssh keys is self._ssh_keys
    @property
    def ssh_keys(self):
        return self._ssh_keys.filter(deleted=False)

    @property
    def credentials(self):
        return {
            'ssh_key': self.ssh_keys.all(),
        }

    # services returns all services this user has access to
    @property
    def services(self):
        s = set()

        for vo in self.vos.all():
            s = s.union(vo.services.all())

        return list(s)

    @property
    def is_active_at_clients(self):
        return self._is_active

# CLASS METHODS
    @classmethod
    def construct_from_userinfo(cls, userinfo, idp):
        if 'sub' not in userinfo:
            raise ValueError('Missing attribute in userinfo: sub')

        user, created = cls.objects.get_or_create(
            user_type=cls.TYPE_CHOICE_USER,
            sub=userinfo['sub'],
            idp=idp,
            defaults={
                'username': '{}@{}'.format(userinfo['sub'], idp.id),
            },
        )

        if created:
            LOGGER.info('construct_from_userinfo: new user: %s', user)

        user.update_userinfo(userinfo)
        user.save()

        _, createdPrefs = UserPreferences.objects.get_or_create(
            user=user,
        )
        if createdPrefs:
            LOGGER.info('construct_from_userinfo: Created preferences for user  %s', user)

        return user

    @classmethod
    def construct_client(cls, username, password, user_type=None, idp=None):
        _user_type = None
        if user_type is None:
            _user_type = cls.TYPE_CHOICE_DOWNSTREAM
        else:
            _user_type = user_type

        LOGGER.debug('Constructing %s %s', _user_type, username)
        client = cls(
            username=username,
            user_type=_user_type,
        )
        client.set_password(password)

        if idp is not None:
            client.idp = idp
            client.save()

        return client


# PROTECTED METHODS
    def _delete_deployments(self):
        for deployment in self.deployments.all():
            LOGGER.debug('Deleting users deployment %s', deployment)
            deployment.delete()

    def _deployments_remove_all(self):
        LOGGER.debug('Removing all deployments of user %s', self)

        sites = []

        # find which sites need to be notified
        for dep in self.deployments.all():
            for state_item in dep.states.all():
                if state_item.state != 'not_deployed':
                    sites.append(state_item.site)

    def _add_vo(self, vo):
        self.vos.add(vo)
        LOGGER.debug(self.msg('Added VO: {}'.format(vo)))

    def _remove_vo(self, vo):
        self.vos.remove(vo)
        LOGGER.debug(self.msg('Removed VO: {}'.format(vo)))

        # check if all the users deployments are still permitted without the removed vo

        # vo deployments
        for dep in self.deployments.filter(vodeployment__vo=vo):
            LOGGER.info(dep.msg('Deleting as user is not permitted anymore'))

            states = list(dep.states.all())
            dep.delete()
            for state in states:
                state.dep_target_changed()  # inform the deployments states about its deletion

        # service deployments
        from feudal.backend.models.deployments import ServiceDeployment
        for dep in self.deployments.instance_of(ServiceDeployment):
            still_permitted = False
            for virto in dep.service.vos.all():
                if virto in self.vos.all():
                    still_permitted = True
                    break

            if not still_permitted:
                LOGGER.info(dep.msg('Deleting as user is not permitted anymore'))

                states = list(dep.states.all())
                dep.delete()
                for state in states:
                    state.dep_target_changed()  # inform the deployments states about its deletion

    def _update_userinfo_entitlements(self, userinfo):
        ''' returns True if the entitlements changed in the userinfo '''
        changed = False

        local_entitlements = self.vos.instance_of(Entitlement)
        remote_entitlements = []
        remote_entitlements_raw = []

        # determine upstream entitlements
        if self.idp.userinfo_field_entitlements != '':
            if self.idp.userinfo_field_entitlements in userinfo:
                field = userinfo[self.idp.userinfo_field_entitlements]
                if isinstance(field, list):
                    remote_entitlements = [
                        Entitlement.extract_name(name)
                        for name in field
                    ]
                    remote_entitlements_raw = field
                elif isinstance(field, str):
                    remote_entitlements = [Entitlement.extract_name(field)]
                    remote_entitlements_raw = [field]
                else:
                    LOGGER.error('Userinfo field %s is neither str nor list', self.idp.userinfo_field_entitlements)
            else:
                LOGGER.info(
                    'Userinfo from the idp %s does not contain the configured field %s. Change the OIDC Config',
                    self.idp,
                    self.idp.userinfo_field_entitlements,
                )

        # check if local_entitlements were removed
        for loc_ent in local_entitlements:
            if loc_ent.name not in remote_entitlements:
                self._remove_vo(loc_ent)
                changed = True

        for rem_ent_name in remote_entitlements_raw:
            ent = Entitlement.get_entitlement(rem_ent_name, idp=self.idp)

            # check if the user needs to be in this entitlement
            if self not in ent.user_set.all():
                self._add_vo(ent)
                changed = True

        return changed

    def _update_userinfo_groups(self, userinfo):
        ''' returns True if the group changed in the userinfo '''
        changed = False

        local_groups = self.vos.instance_of(Group)
        remote_groups = []

        if self.idp.userinfo_field_groups != '':
            if self.idp.userinfo_field_groups in userinfo:
                field = userinfo[self.idp.userinfo_field_groups]
                if isinstance(field, list):
                    remote_groups = field
                elif isinstance(field, str):
                    remote_groups = [field]
                else:
                    LOGGER.error('Userinfo field %s is neither str nor list', self.idp.userinfo_field_groups)
            else:
                LOGGER.info(
                    'Userinfo from the idp %s does not contain the configured field %s. Change the OIDC Config',
                    self.idp,
                    self.idp.userinfo_field_groups,
                )

        # check if groups were removed
        for group in local_groups:
            if group.name not in remote_groups:
                self._remove_vo(group)
                changed = True

        # check if groups were added
        for group_name in remote_groups:
            group = Group.get_group(group_name, self.idp)

            # check if user needs to be in this group
            if self not in group.user_set.all():
                self._add_vo(group)
                changed = True

        return changed

    def _update_userinfo_ssh_key(self, userinfo):
        ''' returns True if the ssh key changed in the userinfo '''

        # TODO move these hardcoded values into the oidcconfig (with a default)
        idp_key_name = 'ssh_key'
        unity_key_name = 'unity_key'

        unity_key_value = userinfo.get(idp_key_name, '')

        try:
            key = self._ssh_keys.get(name=unity_key_name)

            # is the idp key still present?
            if idp_key_name not in userinfo:
                self.remove_key(key)
                return True

            # is the idp key changed?
            if key.key != unity_key_value:
                self.remove_key(key)

                # causes creation of a new key
                raise SSHPublicKey.DoesNotExist

        except SSHPublicKey.DoesNotExist:
            if idp_key_name not in userinfo:
                # no key exists and there was none in the userinfo
                return False

            key = SSHPublicKey(
                name=unity_key_name,
                key=unity_key_value,
                user=self,
            )
            key.save()

            self.add_key(key)
            return True
        return False


# PUBLIC METHODS
    def update_userinfo(self, new_userinfo, initial=False):
        changed = False

        # check if the new userinfo matches the user
        if not initial and (new_userinfo.get('sub', '') != self.sub or new_userinfo.get('iss', '') != self.idp.issuer_uri):
            raise ValueError('New userinfo does not match the users sub/iss')

        changed_values = []
        added_values = []
        removed_values = []

        # determine if values are changed
        for key in self.userinfo.keys():
            local = self.userinfo.get(key, None)
            remote = new_userinfo.get(key, None)
            if local is not None and remote is None:
                changed = True
                removed_values.append(key)
            elif local != remote:
                changed = True
                changed_values.append(key)

        # determine if values were added
        for key in new_userinfo.keys():
            local = self.userinfo.get(key, None)
            remote = new_userinfo.get(key, None)
            if local != remote:
                changed = True
                if key not in changed_values:
                    added_values.append(key)

        if changed:
            LOGGER.debug(self.msg('Userinfo: changed: {}  added: {}  removed: {}'.format(changed_values, added_values, removed_values)))
            #LOGGER.debug('\n%s\n%s', self.userinfo, new_userinfo)
            changed = True
            self.email = new_userinfo.get('email', self.email)
            self.last_name = new_userinfo.get('family_name', self.last_name)
            self.first_name = new_userinfo.get('given_name', self.first_name)

            self.userinfo = new_userinfo
            self.save()
        else:
            LOGGER.debug(self.msg('Userinfo unchanged'))

        # changed should already catch changes on its own
        changed = self._update_userinfo_entitlements(new_userinfo) or changed
        changed = self._update_userinfo_groups(new_userinfo) or changed
        changed = self._update_userinfo_ssh_key(new_userinfo) or changed

        # deployments need to get the new userinfo
        if changed:
            for state in self.states.all():
                state.userinfo_changed(new_userinfo)

    def __str__(self):
        name = ''
        if self.user_type in self.NAME_PREFIXES:
            name += self.NAME_PREFIXES[self.user_type]

        if self.user_type == self.TYPE_CHOICE_USER and not self.is_active:
            name = 'DEACTIVATED ' + name

        name += self.profile_name  if self.user_type == self.TYPE_CHOICE_USER else self.username
        return name

    def msg(self, msg):
        return '[{}] {}'.format(self, msg)

    def activate(self):
        if self._is_active:
            LOGGER.error(self.msg('already activated'))
            return

        self.is_active = True
        self._is_active = True
        self.save()
        LOGGER.info(self.msg('activated'))

        # oidcuser: deploy the according credentials
        if self.user_type == self.TYPE_CHOICE_USER:
            # for dep in self.deployments.all():
            #    dep.activate()
            pass

    def deactivate(self):
        if not self._is_active:
            LOGGER.error(self.msg('already deactivated'))
            return

        self.is_active = False
        self._is_active = False
        self.save()

        LOGGER.debug(self.msg('deactivating'))

        # oidcuser: remove all credentials
        if self.user_type == self.TYPE_CHOICE_USER:
            self._deployments_remove_all()

        LOGGER.info(self.msg('deactivated'))

    def delete(self, using=None, keep_parents=False):
        if self.user_type == self.TYPE_CHOICE_USER:
            LOGGER.info(self.msg('Preparing user deletion'))

            self._delete_deployments()

            # delete SSH Keys
            for key in self.ssh_keys.all():
                key.delete_key()

        LOGGER.info(self.msg('Deleting right now'))
        super().delete(using, keep_parents)

    def add_key(self, key):
        LOGGER.debug(self.msg('Adding key: {}'.format(key)))

        for dep in self.deployments.all():
            dep.user_credential_added(key)

    def remove_key(self, key):
        LOGGER.debug(self.msg('Removing key: {}'.format(key)))

        if key.delete_key():
            return

        for dep in self.deployments.all():
            dep.user_credential_removed(key)


class SSHPublicKey(models.Model):
    name = models.CharField(
        max_length=150,
    )
    key = models.TextField(
        max_length=1000
    )
    # hidden field at the user
    user = models.ForeignKey(
        User,
        related_name='_ssh_keys',
        on_delete=models.SET_NULL,
        null=True,
    )

    # has the user triggered the deletion of this key?
    deleted = models.BooleanField(
        default=False,
        editable=False,
    )

    @property
    def value(self):
        return self.key

    # returns true if the deletion is final
    def delete_key(self):
        if self.try_delete_key():
            return True

        LOGGER.debug(self.msg('Deletion started'))
        self.user = None
        self.key = ''
        self.deleted = True
        self.save()
        return False

    # if this key has no credential states anymore we _really_ delete it
    def try_delete_key(self):
        from feudal.backend.models.deployments import DEPLOYED
        if not self.credential_states.filter(state=DEPLOYED).exists():
            LOGGER.info(self.msg('Deleting key right now'))
            self.delete()
            return True

        return False

    def __str__(self):
        if self.deleted:
            return 'DELETED: {}'.format(self.name)
        return self.name

    def msg(self, msg):
        return '[SSHKey:{}] {}'.format(self, msg)


@receiver(post_save, sender=User)
def deactivate_user(sender, instance=None, created=False, **kwargs):
    if created:
        return

    if not instance.is_active and instance.is_active_at_clients:
        instance.deactivate()


@receiver(post_save, sender=User)
def activate_user(sender, instance=None, created=False, **kwargs):
    if created:
        return

    if instance.is_active and not instance.is_active_at_clients:
        instance.activate()


class UserPreferences(models.Model):

    user = models.OneToOneField(
        User,
        related_name='preferences',
        on_delete=models.CASCADE,
    )

    DEPLOYMENT_MODE_BOTH = 'both'
    DEPLOYMENT_MODE_SERVICES_ONLY = 'services-only'
    DEPLOYMENT_MODE_VOS_ONLY = 'vos-only'
    DEPLOYMENT_MODE_DEFAULT = DEPLOYMENT_MODE_BOTH

    DEPLOYMENT_MODE_CHOICES = (
        (DEPLOYMENT_MODE_BOTH, 'Pick a combination of services and VOs for deployment.'),
        (DEPLOYMENT_MODE_SERVICES_ONLY, 'Pick individual services for deployment'),
        (DEPLOYMENT_MODE_VOS_ONLY, 'Pick individual VOs for deployment'),
    )

    deployment_mode = models.CharField(
        max_length=20,
        choices=DEPLOYMENT_MODE_CHOICES,
        default=DEPLOYMENT_MODE_DEFAULT,
    )
