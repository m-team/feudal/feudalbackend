
from rest_framework.permissions import IsAuthenticated

from .models import User


class TypeOnly(IsAuthenticated):
    user_type = ''

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.user.user_type == self.user_type


class UpstreamOnly(TypeOnly):
    user_type = User.TYPE_CHOICE_UPSTREAM


class DownstreamOnly(TypeOnly):
    user_type = User.TYPE_CHOICE_DOWNSTREAM


class UserOnly(TypeOnly):
    user_type = User.TYPE_CHOICE_USER
