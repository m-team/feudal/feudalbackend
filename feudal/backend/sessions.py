
import logging

from django.db.utils import OperationalError

LOGGER = logging.getLogger(__name__)


# Exception for all session operations
class SessionError(OperationalError):
    pass


def get_session(request, key, default):
    try:
        value = request.session.get(key, None)
    except OperationalError as exp:
        raise SessionError('get_session: Error getting from session: {}'.format(exp))

    if value is not None:
        return value

    return default


def set_session(request, key, value):
    try:
        value = request.session[key] = value
    except OperationalError as exp:
        raise SessionError('set_session: Error setting in session: {}'.format(exp))


def del_session(request, keys):
    try:
        for key in keys:
            if key in request.session:
                del request.session[key]
    except OperationalError as exp:
        raise SessionError('del_session: Error deleting values from session: {}'.format(exp))
