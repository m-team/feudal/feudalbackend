# pylint: disable=line-too-long,invalid-name,unused-argument,redefined-outer-name

import logging

from django.contrib.auth import authenticate
from django.test import Client, TestCase

from feudal.backend.models import Site, Service
from feudal.backend.models.users import User, SSHPublicKey
from feudal.backend.models.auth import OIDCConfig
from feudal.backend.models.auth.vos import Group, Entitlement

LOGGER = logging.getLogger(__name__)

# base test
class BaseTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.TEST_NAME = 'Gustav Holst'
        cls.TEST_EMAIL = 'gustav@test-domain.de'
        cls.TEST_SUB = 'fb0fa558-cfa2-49f9-b847-5c651d1f6135'
        cls.TEST_KEY = 'ssh-rsa AAAAB3NzaC1yhApzBpUulukg9Q== TEST_KEY'
        cls.TEST_ISSUER = 'https://unity.test-federation.de/oauth2'

        cls.TEST_USERINFO = {
            'sub': cls.TEST_SUB,
            'iss': cls.TEST_ISSUER,
            'email': cls.TEST_EMAIL,
            'name': cls.TEST_NAME,
            'ssh_key': cls.TEST_KEY,
        }

        cls.idp = OIDCConfig(
            name='test_idp_name',
            client_id='test_idp_client_id',
            client_secret='test_idp_secret',
            issuer_uri=cls.TEST_ISSUER,
            scopes=['test_scope'],
            userinfo_field_entitlements='',
            userinfo_field_groups='',
        )
        cls.idp.save()

        cls.entitlement = Entitlement.get_entitlement(name='test_entitlement', idp=cls.idp)
        cls.entitlement.save()

        # Has no service
        cls.GROUP_NO_SERVICES = 'test_group_no'
        # Has a service
        cls.GROUP_ONE_SERVICE = 'test_group_one'
        # Has two services
        cls.GROUP_TWO_SERVICES = 'test_group_two'
        cls.group_none = Group.get_group(name=cls.GROUP_NO_SERVICES, idp=cls.idp)
        cls.group_none.save()
        cls.group_one = Group.get_group(name=cls.GROUP_ONE_SERVICE, idp=cls.idp)
        cls.group_one.save()
        cls.group_two = Group.get_group(name=cls.GROUP_TWO_SERVICES, idp=cls.idp)
        cls.group_two.save()

        cls.user = User.construct_from_userinfo(cls.TEST_USERINFO, cls.idp)

        cls.USER_NAME = cls.user.username
        cls.USER_PASSWORD = 'asdf1234foo'
        cls.user.set_password(cls.USER_PASSWORD)
        cls.user.save()
        cls.user.vos.add(cls.group_none)
        cls.user.vos.add(cls.group_one)
        cls.user.vos.add(cls.group_two)

        cls.site = Site.objects.create(name='test_site')
        cls.site2 = Site.objects.create(name='test_site_2')

        cls.service_one = Service.get_service(
            'test_service_one',
            cls.site,
            vos=[cls.group_one],
        )
        cls.service_two_a = Service.get_service(
            'test_service_two_a',
            cls.site,
            vos=[cls.group_two],
        )
        cls.service_two_b = Service.get_service(
            'test_service_two_b',
            cls.site,
            vos=[cls.group_two],
        )

        cls.key = SSHPublicKey(
            name=cls.TEST_NAME,
            key=cls.TEST_KEY,
            user=cls.user,
        ).save()

