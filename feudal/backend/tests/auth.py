# pylint: disable=unused-argument,too-many-arguments

import logging
from urllib.error import HTTPError

import jwt

# for the running the test client
from django.urls import reverse

from django.contrib.sessions.middleware import SessionMiddleware

from feudal.backend.auth import OIDCTokenAuthHTTPBackend
from feudal.backend.sessions import set_session
from feudal.backend.models.auth import OIDCConfig

LOGGER = logging.getLogger(__name__)


# issuer identified via header
def test_http_authenticate_header(monkeypatch, rf, user, userinfo, idp):
    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer foo',
        HTTP_X_ISSUER=idp.issuer_uri,
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    def mockuserinfo(*args):
        return userinfo

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is not None
    assert auth_user.id == user.id


# issuer identified via session
def test_http_authenticate_session(monkeypatch, rf, user, userinfo, idp):
    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer foo',
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    set_session(request, 'idp_id', idp.id)

    def mockuserinfo(*args):
        return userinfo

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is not None
    assert auth_user.id == user.id


# issuer identified via jwt 'iss' property
def test_http_authenticate_jwt(monkeypatch, rf, user, userinfo, idp):
    key = 'secret'
    token = jwt.encode(
        {'iss': idp.issuer_uri},  # jwt payload
        key,
        algorithm='HS256',
    )

    LOGGER.debug('Token: %s', token)

    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer ' + token.decode('utf-8'),
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    def mockuserinfo(*args):
        return userinfo

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is not None
    assert auth_user.id == user.id

# bruteforce checking of userinfos succeeds
def test_http_authenticate_bruteforce_success(monkeypatch, rf, user, userinfo, idp):
    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer foo',
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    def mockuserinfo(*args):
        return userinfo

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is not None
    assert auth_user.id == user.id


# bruteforce checking of userinfos fails
def test_http_authenticate_bruteforce_failure(monkeypatch, rf, user, userinfo, idp):
    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer foo',
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    def mockuserinfo(*args):
        raise HTTPError(url='foo', code='400', msg='mock-error', hdrs=None, fp=None)

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is None


def test_http_authenticate_deactivated(monkeypatch, rf, user, userinfo, idp):
    request = rf.get(
        reverse('user'),
        HTTP_AUTHORIZATION='Bearer foo',
        HTTP_X_ISSUER=idp.issuer_uri,
    )

    # add session to request
    middleware = SessionMiddleware()
    middleware.process_request(request)

    def mockuserinfo(*args):
        return userinfo

    monkeypatch.setattr(OIDCConfig, 'get_userinfo', mockuserinfo)

    user.deactivate()

    auth_user, _ = OIDCTokenAuthHTTPBackend().authenticate(request)

    assert auth_user is None
