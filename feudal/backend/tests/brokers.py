
from unittest.mock import MagicMock

import pytest

from pika import BlockingConnection

from django.conf import settings

from feudal.backend.brokers import RabbitMQInstance

# disable the module if NO_BROKER is set (e.g. in docker)
pytestmark = pytest.mark.skipif(getattr(settings, 'NO_BROKER', False), reason='No RabbitMQ broker')

def test_broker_publish_user(user):
    broker = RabbitMQInstance()
    broker.publish_to_user(user, 'test_message')


def test_broker_publish_dep_state(dep_state):
    broker = RabbitMQInstance()
    broker.publish_deployment_state(dep_state, 'test_message')


def test_broker_connection_error(monkeypatch):
    # make the initialization of BlockingConnection fail
    monkeypatch.setattr(BlockingConnection, '__init__', MagicMock(side_effect=Exception('test_exception')))

    # TODO
    # a breaking connection should not raise exception!
    with pytest.raises(Exception):
        RabbitMQInstance()
