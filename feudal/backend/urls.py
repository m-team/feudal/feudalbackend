
from django.urls import include, path
from django.contrib import admin

from .views.clients import URLPATTERNS as clientapi_urls
from .views.webpage import URLPATTERNS as webpage_urls
from .views.rest import URLPATTERNS as user_rest_urls
from .views.upstream import URLPATTERNS as upstream_urls
from .views.auth.urls import URLPATTERNS as auth_urls


# all these paths must be configured in the nginx config!
urlpatterns = [
    path('client/', include(clientapi_urls)),

    path('webpage/', include(webpage_urls)),

    path('user/', include(user_rest_urls)),

    path('auth/', include(auth_urls)),

    path('upstream/', include(upstream_urls)),

    path('admin/', admin.site.urls),
]
