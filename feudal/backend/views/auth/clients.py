import logging
import json

from django.http import HttpResponse
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.sessions.models import Session

from feudal.backend.models import Site, Service
from feudal.backend.models.users import User
from feudal.backend.models.auth.vos import Group, Entitlement
from feudal.backend.brokers import RabbitMQInstance

LOGGER = logging.getLogger(__name__)

ALLOW = HttpResponse('allow')
DENY = HttpResponse('deny')


### HELPERS FOR ALL ENDPOINTS ###

def _valid_vhost(request):
    if request.POST.get('vhost') == RabbitMQInstance().vhost:
        return True
    LOGGER.error('illegal vhost requested')
    return False

def _apiclient_valid(request):
    valid = User.objects.filter(
        user_type=User.TYPE_CHOICE_DOWNSTREAM,
        username=request.POST.get('username'),
    ).exists()
    if valid:
        return True
    return False

def _webpage_client_userid(request):
    userid = ''
    username = request.POST.get('username')
    if username.startswith('webpage-client:'):
        components = username.split(':', maxsplit=1)
        if len(components) == 2:
            userid = components[1]
    return userid


### USER ENDPOINT ###

def _webpage_client_valid(request):
    userid = _webpage_client_userid(request)
    try:
        session = Session.objects.get(
            session_key=request.POST.get('password'),
        )
        sd = session.get_decoded()
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, 'Session data: %s', json.dumps(sd, sort_keys=True, indent=4))


        return sd.get('_auth_user_id') == userid

    except Session.DoesNotExist:
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, 'User %s has no session', userid)
        return False

def user_endpoint(request):
    if _webpage_client_valid(request):
        # LOGGER.info('Authenticated webpage client')
        return ALLOW

    username = request.POST.get('username', '')

    user = authenticate(
        username=username,
        password=request.POST.get('password', ''),
    )
    if user is not None:
        LOGGER.info('Authenticated client as %s', user)
        return ALLOW

    LOGGER.error('Failed to authenticate user for RabbitMQ: %s', username)
    return DENY


### VHOST ENDPOINT ###

def _valid_user(request):
    return _apiclient_valid(request) or _webpage_client_userid(request)

def vhost_endpoint(request):
    # check if on the correct virtual host
    if _valid_vhost(request) and _valid_user(request):
        return ALLOW

    LOGGER.error('Authorization check for vhost failed for %s', request.POST)
    return DENY


### RESOURCE ENDPOINT ###

def _resource_authorized_webpage_client(request):
    resource = request.POST.get('resource')
    name = request.POST.get('name', '')
    permission = request.POST.get('permission', [])
    return (
        resource == 'exchange'
        and name == 'users'
        and 'write' not in permission
    ) or (
        resource == 'queue'
        and name.startswith('stomp-subscription-')
    ) or (
        resource == 'topic'
        and name == _webpage_client_userid(request)
    )

def _resource_authorized_apiclient(request):
    resource = request.POST.get('resource')
    name = request.POST.get('name', '')
    permission = request.POST.get('permission', [])
    return (
        resource == 'queue'
        and name.startswith('amq.gen-')
    ) or (
        resource == 'exchange'
        and name in RabbitMQInstance().exchanges
        and 'write' not in permission
    )

def resource_auth_decision(request, decision):
    user = request.POST.get('username')
    permission = request.POST.get('permission', [])
    resource = request.POST.get('resource', '')
    name = request.POST.get('name', '')

    LOGGER.log(settings.DEBUGX_LOG_LEVEL, "[resource] %s %s %s '%s' for %s", decision.content, permission, resource, name, user)

    return decision

def resource_endpoint(request):
    if _valid_vhost(request):
        if (
                _webpage_client_userid(request)
                and _resource_authorized_webpage_client(request)
        ):
            return resource_auth_decision(request, ALLOW)

        if (
                _apiclient_valid(request)
                and _resource_authorized_apiclient(request)
        ):
            return resource_auth_decision(request, ALLOW)

    return resource_auth_decision(request, DENY)


### TOPIC ENDPOINT ###

def _valid_permission(request):
    perm = request.POST.get('permission', [])
    if 'write' not in perm:
        return True
    LOGGER.info('Illegal permission requested %s', perm)
    return False

def topic_auth_decision(request, decision):
    user = request.POST.get('username')
    permission = request.POST.get('permission', [])
    resource = request.POST.get('resource', '')
    name = request.POST.get('name', '')
    routing_key = request.POST.get('routing_key', '')

    LOGGER.log(settings.DEBUGX_LOG_LEVEL, "[topic] %s %s %s %s '%s' for %s", decision.content, permission, resource, name, routing_key, user)

    return decision

def topic_endpoint_webpageclient(request, webpage_client_userid):
    try:
        User.objects.get(id=webpage_client_userid)
        return topic_auth_decision(request, ALLOW)

    except User.DoesNotExist:
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, 'Webpage client %s does not exist', webpage_client_userid)
        return topic_auth_decision(request, DENY)

def topic_endpoint_apiclient(request, apiclient):
    name = request.POST.get('name', '')
    routing_key = request.POST.get('routing_key', '')

    try:
        if name in ('groups', 'entitlements'):
            try:
                vo = None
                if name == 'groups':
                    vo = Group.objects.get(name=routing_key)
                elif name == 'entitlements':
                    vo = Entitlement.objects.get(
                        # we strip the group authority from the routing key if it was included
                        name=Entitlement.extract_name(routing_key),
                    )

                Site.objects.get(
                    services__vos=vo,
                    client=apiclient,
                )
                return topic_auth_decision(request, ALLOW)

            except (Group.DoesNotExist, Entitlement.DoesNotExist):
                LOGGER.error('VO does not exist: %s', routing_key)
                return topic_auth_decision(request, DENY)

        elif name == 'services':
            try:
                service = Service.objects.get(name=routing_key)

                Site.objects.get(
                    services=service,
                    client=apiclient,
                )
                return topic_auth_decision(request, ALLOW)

            except Service.DoesNotExist:
                LOGGER.error('Service does not exist: %s', routing_key)
                return topic_auth_decision(request, DENY)

    except Site.MultipleObjectsReturned as error:
        return topic_auth_decision(request, ALLOW)

    except Site.DoesNotExist as error:
        LOGGER.error(error)
        return topic_auth_decision(request, DENY)

    return topic_auth_decision(request, DENY)

def topic_endpoint(request):
    if not _valid_vhost(request) or not _valid_permission(request):
        return DENY

    webpage_client_userid = _webpage_client_userid(request)
    LOGGER.log(settings.DEBUGX_LOG_LEVEL, 'Webpage client userid: %s', webpage_client_userid)

    if webpage_client_userid != '':
        return topic_endpoint_webpageclient(request, webpage_client_userid)

    try:
        apiclient = User.objects.get(
            user_type=User.TYPE_CHOICE_DOWNSTREAM,
            username=request.POST.get('username'),
        )
        return topic_endpoint_apiclient(request, apiclient)

    except User.DoesNotExist:
        LOGGER.log(settings.DEBUGX_LOG_LEVEL, 'Apiclient %s does not exist', request.POST.get('username'))
        return topic_auth_decision(request, DENY)
