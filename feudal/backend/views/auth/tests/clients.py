# pylint: disable=unused-argument,too-many-arguments

import logging
import pytest

from django.contrib.sessions.backends.db import SessionStore


LOGGER = logging.getLogger(__name__)


## tests for the user endpoint
#
def test_user_webpage_client_invalid_username(test_client, broker, user):
    response = test_client.post(
        '/auth/client/user',
        {
            'username': 'webpage-client:{}'.format(user.id+9999),  # invalid userid
            'password': 'invalid-password',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_user_webpage_client_invalid_password(test_client, broker, user):
    response = test_client.post(
        '/auth/client/user',
        {
            'username': 'webpage-client:{}'.format(user.id),
            'password': 'invalid-password',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_user_webpage_client_success(test_client, broker, user):
    # hack that the user has a session
    session = SessionStore()
    session.create()
    session['_auth_user_id'] = str(user.id)
    session.save()

    response = test_client.post(
        '/auth/client/user',
        {
            'username': 'webpage-client:{}'.format(user.id),
            'password': session.session_key,
        }
    )
    assert response.content.decode('utf-8') == 'allow'

def test_user_downstream_client_success(test_client, downstream_client, password):
    response = test_client.post(
        '/auth/client/user',
        {
            'username': downstream_client.username,
            'password': password,
        }
    )
    assert response.content.decode('utf-8') == 'allow'


## tests for the vhost endpoint
#
def test_vhost_invalid_vhost(test_client, downstream_client, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/vhost',
        {
            'username': downstream_client.username,
            'vhost': 'invalid-vhost',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_vhost_success(test_client, broker, downstream_client, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/vhost',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
        }
    )
    assert response.content.decode('utf-8') == 'allow'


## tests for the resource endpoint
#
def test_resource_invalid_resource(test_client, broker, downstream_client, group):
    response = test_client.post(
        '/auth/client/resource',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'invalid-resource',
            'name': 'name',
            'permission': ['read', 'write'],
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_resource_invalid_username(test_client, broker, downstream_client, group):
    response = test_client.post(
        '/auth/client/resource',
        {
            'username': 'invalid-username',
            'vhost': broker.vhost,
            'resource': 'invalid-resource',
            'name': 'name',
            'permission': ['read', 'write'],
        }
    )
    assert response.content.decode('utf-8') == 'deny'

@pytest.mark.parametrize(
    'resource_name',
    [
        'entitlements',
        'groups',
        'services',
    ],
)
def test_resource_downstream_client_success(resource_name, test_client, broker, downstream_client, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/resource',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'exchange',
            'name': resource_name,
            'permission': ['read'],
        }
    )
    assert response.content.decode('utf-8') == 'allow'

@pytest.mark.parametrize(
    'resource_name',
    [
        'users',
    ],
)
def test_resource_webpage_client_success(resource_name, test_client, broker, downstream_client, group, user):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/resource',
        {
            'username': 'webpage-client:{}'.format(user.id),
            'vhost': broker.vhost,
            'resource': 'exchange',
            'name': resource_name,
            'permission': ['read'],
        }
    )
    assert response.content.decode('utf-8') == 'allow'


## tests for the topic endpoint
#
def test_topic_invalid_vhost(test_client, downstream_client, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': 'invalid-vhost',
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': group.name,
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_topic_invalid_username(test_client, broker, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': 'invalid-username',
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': group.name,
        }
    )
    assert response.content.decode('utf-8') == 'deny'


def test_topic_group_success(test_client, downstream_client, broker, group, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': group.name,
        },
    )
    assert response.content.decode('utf-8') == 'allow'

def test_topic_group_non_existent_group(test_client, downstream_client, broker):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': 'non-existent-group',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_topic_group_non_service_missing(test_client, downstream_client, broker, group, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': group.name,
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_topic_group_non_site_missing(test_client, downstream_client, broker, group):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'groups',  # exchange name
            'routing_key': group.name,
        }
    )
    assert response.content.decode('utf-8') == 'deny'


def test_topic_entitlement_success(test_client, broker, downstream_client, entitlement, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'entitlements',  # exchange name
            'routing_key': repr(entitlement),
        }
    )
    assert response.content.decode('utf-8') == 'allow'

def test_topic_entitlement_failure(test_client, downstream_client, broker):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'entitlements',  # exchange name
            'routing_key': 'non-existent-entitlement',
        }
    )
    assert response.content.decode('utf-8') == 'deny'


def test_topic_service_success(test_client, broker, downstream_client, entitlement, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'services',  # exchange name
            'routing_key': service.name,
        }
    )
    assert response.content.decode('utf-8') == 'allow'

def test_topic_service_failure(test_client, downstream_client, broker):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': downstream_client.username,
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'services',  # exchange name
            'routing_key': 'non-existent-service-name',
        }
    )
    assert response.content.decode('utf-8') == 'deny'


def test_topic_webpage_client_invalid_username(test_client, broker, user, entitlement, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': 'webpage-client:{}'.format(user.id+9999),  # invalid userid
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'services',  # exchange name
            'routing_key': service.name,
            'permission': 'read',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_topic_webpage_client_write(test_client, broker, user, entitlement, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': 'webpage-client:{}'.format(user.id),
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'services',  # exchange name
            'routing_key': service.name,
            'permission': 'write',
        }
    )
    assert response.content.decode('utf-8') == 'deny'

def test_topic_webpage_client_success(test_client, broker, user, entitlement, service, site):
    # https://github.com/rabbitmq/rabbitmq-auth-backend-http#topic_path
    response = test_client.post(
        '/auth/client/topic',
        {
            'username': 'webpage-client:{}'.format(user.id),
            'vhost': broker.vhost,
            'resource': 'topic',
            'name': 'services',  # exchange name
            'routing_key': service.name,
            'permission': 'read',
        }
    )
    assert response.content.decode('utf-8') == 'allow'
