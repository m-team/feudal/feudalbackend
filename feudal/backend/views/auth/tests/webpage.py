# pylint: disable=too-many-arguments,unused-argument

import logging
import json
from unittest.mock import MagicMock

import pytest

from django.conf import settings
from django.urls import reverse
from django.core.exceptions import ImproperlyConfigured

# for mocking
from oic.oic import Client as oicClient
from oic.oic.message import MissingRequiredAttribute, MissingRequiredValue
from oic.oauth2.exception import HttpError, ResponseError

from feudal.backend.views.auth.webpage import AuthCallback
from feudal.backend.models.auth import OIDCConfig


LOGGER = logging.getLogger(__name__)



# 'login' view
#
def test_webpage_auth_failure_missing_arg(monkeypatch, test_client, user, userinfo, idp):
    response = test_client.get(reverse('login'))

    # redirect back to the webpage
    assert response.status_code == 302
    assert response.url == '/'
    assert test_client.session.get('msg').startswith('Missing query parameter')

def test_webpage_auth_failure_invalid_arg(test_client, idp):
    response = test_client.get(reverse('login'), {settings.HTTP_PARAM_IDPHINT: 'invalid.url'})

    # redirect back to the webpage
    assert response.status_code == 302
    assert response.url == '/'
    assert test_client.session.get('msg') == 'Selected IdP not found!'

def test_webpage_auth_failure_non_unique_idp(test_client, idp):
    # two idps share the same issuer_uri
    OIDCConfig.objects.create(
        name='OIDCConfig-test-fixture',
        client_id='test_idp_client_id',
        client_secret='test_idp_secret',
        issuer_uri=idp.issuer_uri,
        scopes=['test_scope'],
        userinfo_field_entitlements='eduperson_entitlements',
        userinfo_field_groups='groups',
        enabled=True,
    )

    response = test_client.get(reverse('login'), {settings.HTTP_PARAM_IDPHINT: idp.issuer_uri})

    assert response.status_code == 302
    assert response.url == '/'
    assert test_client.session.get('msg') == 'Server misconfigured: IdP Issuer URL is not unique!'

# successful call to 'login'
def test_webpage_auth_success(monkeypatch, test_client, idp, idp_well_known_config):

    # mock the oic.Clients request for the idps openid-configuration
    def mock_request(self, *args, **kwargs):
        LOGGER.debug('args: %s %s', args, kwargs)
        m = MagicMock()
        m.status_code = 200
        m.text = idp_well_known_config
        return m

    # http_request is luckily only called once during the handling of the request below
    # so we can patch in unconditionally
    monkeypatch.setattr(oicClient, 'http_request', mock_request)

    response = test_client.get(reverse('login'), {settings.HTTP_PARAM_IDPHINT: idp.issuer_uri, 'foo': 'bar'})

    assert test_client.session.get('state') is not None
    assert test_client.session.get('idp_id') == idp.id

    assert response.status_code == 302
    assert response.url.startswith(idp.issuer_uri)
    assert 'foo=bar' in response.url  # query params need to be preserved


# 'callback' view
#
# request to 'callback' missing session values
def test_webpage_auth_callback_missing_session_values(monkeypatch, test_client, idp):
    response = test_client.get(reverse('callback'))

    # missing values should cause a retry of the flow
    assert test_client.session.get('auth_error') == "No 'idp_id' or 'state' in session"
    assert test_client.session.get('retry')

    assert response.status_code == 302
    assert response.url == reverse('login')

def test_webpage_auth_callback_repeated_missing_values(monkeypatch, test_client, idp):
    response = test_client.get(reverse('callback'))

    # missing values should cause a retry of the flow
    assert test_client.session.get('auth_error') == "No 'idp_id' or 'state' in session"
    assert test_client.session.get('retry')

    assert response.status_code == 302
    assert response.url == reverse('login')

    # request again
    response = test_client.get(reverse('callback'))

    # missing values should cause a retry of the flow
    assert test_client.session.get('auth_error') == "No 'idp_id' or 'state' in session"
    assert not test_client.session.get('retry')

    assert response.status_code == 302
    assert response.url == '/'

def test_webpage_auth_callback_invalid_session_values(monkeypatch, test_client, idp):
    # fill the session with invalid values
    state = 'foobarbaz'

    # Be careful : To modify the session and then save it, it must be stored in a variable first
    # (because a new SessionStore is created every time this property is accessed)
    session = test_client.session
    session.update({
        'idp_id': idp.id+1,  # setting an invalid value here
        'state': state,
    })
    session.save()

    response = test_client.get(reverse('callback'))

    session = test_client.session
    assert session.get('auth_error') == 'Unable to determine IdP'

    assert response.status_code == 302
    assert response.url == '/'

# returns a mock for OIDCConfig.oidc_client
# this mock causes the AuthCallback view to succeed
def oidc_client_mock_success(state, code, at):
    mm = MagicMock()
    mm.parse_response.return_value = {
        'state': state,
        'code': code,
    }
    mm.do_access_token_request.return_value = {
        'access_token': at,
    }
    return mm

# mock failures in oic.Client and compare with expected error messages
@pytest.mark.parametrize(
    ('mock_attributes', 'auth_error'),
    [
        # failure in oic.Client.parse_response
        ({'parse_response.side_effect': ResponseError('error')}, 'Unable to parse authorization response'), # raise error
        ({'parse_response.return_value': {'error': 'bla'}}, 'Authorization response error: bla'),  # return contains error
        ({'parse_response.return_value': {'state': 'not_matching_state'}}, 'States do not match'), # return contains a non-matching state
        ({'parse_response.return_value': {'state': 'foobarbaz'}}, 'Did not receive a code'),  # return contains no 'code'
        # failure in oic.Client.do_access_token_request
        ({'do_access_token_request.side_effect': MissingRequiredAttribute('error')}, 'Missing required attribute/value'),  # raise error
        ({'do_access_token_request.side_effect': MissingRequiredValue('error')}, 'Missing required attribute/value'),  # raise error
        ({'do_access_token_request.side_effect': HttpError('error')}, 'HTTP error on do_access_token_request'),  # raise error
        ({'do_access_token_request.return_value': {}}, 'No access token in access token response'),  # response contains no access token
    ]
)
def test_webpage_auth_callback_failures(monkeypatch, test_client, idp, mock_attributes, auth_error):
    state = 'foobarbaz'
    session = test_client.session
    session.update({
        'idp_id': idp.id,
        'state': state,
    })
    session.save()

    # mocking a failure in parse_response
    mm = oidc_client_mock_success(state, 'code', 'at')
    mm.configure_mock(**mock_attributes)
    monkeypatch.setattr(OIDCConfig, 'oidc_client', mm)

    response = test_client.get(reverse('callback'))

    ae = test_client.session.get('auth_error')
    assert ae == auth_error
    assert response.status_code == 302
    assert response.url == '/'

def test_webpage_auth_callback_no_login(monkeypatch, test_client, idp):

    state = 'foobarbaz'
    code = 'aoeust.hp,t.pntheuo'
    at = 'temp_test_at'

    session = test_client.session
    session.update({
        'idp_id': idp.id,
        'state': state,
    })
    session.save()

    # successful mock
    mm = oidc_client_mock_success(state, code, at)
    monkeypatch.setattr(OIDCConfig, 'oidc_client', mm)

    response = test_client.get(reverse('callback'))

    assert test_client.session.get('auth_error') is not None

    assert response.status_code == 302
    assert response.url == '/'

def test_webpage_auth_callback_success(monkeypatch, test_client, idp, userinfo):
    state = 'foobarbaz'
    code = 'aoeust.hp,t.pntheuo'
    at = 'temp_test_at'

    session = test_client.session
    session.update({
        'idp_id': idp.id,
        'state': state,
    })
    session.save()

    # patch the relevant functions
    monkeypatch.setattr(OIDCConfig, 'oidc_client', oidc_client_mock_success(state, code, at))
    monkeypatch.setattr(OIDCConfig, 'get_userinfo', lambda self, at: userinfo)  # need a valid userinfo for a login

    response = test_client.get(reverse('callback'))

    assert test_client.session.get('auth_error') is None

    assert response.status_code == 302
    assert response.url == '/'


# 'auth_info' view
#
# with one enabled idp
def test_auth_info(test_client, idp):
    response = test_client.get(reverse('auth_info'))
    LOGGER.debug('Response: %s', response)

    assert response.status_code == 200

    data = json.loads(response.content)
    LOGGER.debug('Data: %s', data)

    assert len(data['idps']) == 1 and data['idps'][0]['id'] == idp.id
    assert data['default'] == idp.id

# with one disabled idp
def test_auth_info_deactivated_id(test_client, idp):
    idp.enabled = False
    idp.save()

    with pytest.raises(ImproperlyConfigured):
        test_client.get(reverse('auth_info'))

# with to idp
def test_auth_info_no_id(test_client):
    with pytest.raises(ImproperlyConfigured):
        test_client.get(reverse('auth_info'))


# 'logout' view
#
def test_logout_after_login(user_test_client):
    # user_test_client has a logged in user
    response = user_test_client.post(reverse('logout'))

    assert response.status_code == 302
    assert response.url == '/'

def test_logout_without_login(test_client):
    # user_test_client has no logged in user
    response = test_client.post(reverse('logout'))

    assert response.status_code == 302
    assert response.url == '/'
