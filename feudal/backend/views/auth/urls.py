
from django.urls import path, include
from django.views.decorators.csrf import csrf_exempt

from feudal.backend.views.auth import webpage, clients

clientpatterns = [
    path('user', csrf_exempt(clients.user_endpoint)),
    path('vhost', csrf_exempt(clients.vhost_endpoint)),
    path('resource', csrf_exempt(clients.resource_endpoint)),
    path('topic', csrf_exempt(clients.topic_endpoint)),
]

URLPATTERNS = [
    path('info', webpage.AuthInfo.as_view(), name='auth_info'),
    path('request', webpage.Auth.as_view(), name='login'),
    path('callback', webpage.AuthCallback.as_view(), name='callback'),
    path('logout', webpage.LogoutView.as_view(), name='logout'),

    path('client/', include(clientpatterns)),
]
