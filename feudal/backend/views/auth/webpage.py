# pylint:
#disable=attribute-defined-outside-init

import json
import logging
import urllib

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.views import View
from django.conf import settings

from oic import rndstr
from oic.oic.message import AuthorizationResponse, MissingRequiredAttribute, MissingRequiredValue
from oic.oauth2.exception import HttpError, ResponseError

from rest_framework import generics, views
from rest_framework.permissions import AllowAny

from feudal.backend.sessions import get_session, set_session, del_session
from feudal.backend.models.auth import OIDCConfig, default_idp
from feudal.backend.models.auth.serializers import AuthInfoSerializer

LOGGER = logging.getLogger(__name__)

HTTP_PARAM_IDP_ID = 'idp_id'


class AuthFlowException(Exception):
    pass


class Auth(View):

    # sets the error message in the session and redirects back to '/'
    def _report_error(self, request, msg):
        LOGGER.info('Auth error: %s', msg)
        set_session(request, 'msg', msg)
        return redirect('/')

    def get(self, request):
        state = rndstr()
        set_session(request, 'state', state)
        nonce = rndstr()

        # kwargs for the finding the config
        oidc_config_kwargs = {
            'enabled': True,
        }

        # select an idp according to query parameter
        if settings.HTTP_PARAM_IDPHINT not in request.GET and HTTP_PARAM_IDP_ID not in request.GET:
            return self._report_error(request, 'Missing query parameter \'{}\' or \'{}\''.format(
                settings.HTTP_PARAM_IDPHINT,
                HTTP_PARAM_IDP_ID,
            ))

        issuer_uri_urlenc = request.GET.get(settings.HTTP_PARAM_IDPHINT, None)
        idp_id = request.GET.get(HTTP_PARAM_IDP_ID, None)

        # id has precendence over uri, as its more stable to uri changes
        if idp_id is not None:
            oidc_config_kwargs['id'] = idp_id
        elif issuer_uri_urlenc is not None:
            oidc_config_kwargs['issuer_uri'] = urllib.parse.unquote(issuer_uri_urlenc)

        try:
            oidc_config = OIDCConfig.objects.get(**oidc_config_kwargs)

            # store the idp used for the authentication in the users session
            set_session(request, 'idp_id', oidc_config.id)

            # construct the auth redirect
            auth_redirect = oidc_config.get_auth_request(
                state,
                nonce,
            )

            # include potential query parameters in the redirect to the idp
            if len(list(request.GET.items())) > 1:
                urlparams = request.GET.copy()
                if 'idp' in urlparams:
                    del urlparams['idp']
                auth_redirect += '&'+urlparams.urlencode()

            return redirect(auth_redirect)

        except OIDCConfig.DoesNotExist:
            LOGGER.debug('oidc_config_kwargs: %s', oidc_config_kwargs)
            return self._report_error(request, 'Selected IdP not found!')

        except OIDCConfig.MultipleObjectsReturned:
            LOGGER.debug('oidc_config_kwargs: %s', oidc_config_kwargs)
            LOGGER.error('Server misconfigured: Multiple IdP with identical issuer uri')
            return self._report_error(request, 'Server misconfigured: IdP Issuer URL is not unique!')


class AuthCallback(View):
    request = None
    state = None
    idp = None

    # call if the authentication is successful to remove earlier error messages
    def _reset_session(self):
        del_session(self.request, ['auth_error', 'retry'])

    def _retry_flow(self):
        if get_session(self.request, 'retry', False):
            # remove retry flag from session
            del_session(self.request, ['retry'])
            LOGGER.error('Auth flow failed repeatedly. Not retrying.')
            return redirect('/')

        LOGGER.debug('Retrying auth flow')
        set_session(self.request, 'retry', True)
        return redirect('login')

    def _flow_failed(self):
        auth_error = get_session(self.request, 'auth_error', None) or 'No auth_error provided'
        msg = get_session(self.request, 'msg', None) or 'Authentication failed'

        LOGGER.error('Auth flow failed: auth_error="%s" msg="%s"', auth_error, msg)
        return redirect('/')

    def _get_aresp(self):
        try:
            aresp = self.idp.oidc_client.parse_response(
                AuthorizationResponse,
                info=json.dumps(self.request.GET),
            )
            if 'error' in aresp:
                raise AuthFlowException('Authorization response error: {}'.format(aresp['error']))

            if self.state != aresp.get('state', ''):
                raise AuthFlowException('States do not match')

            if 'code' not in aresp:
                raise AuthFlowException('Did not receive a code')

            return aresp

        except ResponseError as e:
            raise AuthFlowException('Unable to parse authorization response') from e

    def _get_access_token(self):
        try:
            aresp = self._get_aresp()
            access_token_response = (
                self.idp.oidc_client.do_access_token_request(
                    state=self.state,
                    request_args=aresp,
                )
            )

            if 'access_token' not in access_token_response:
                raise AuthFlowException('No access token in access token response')

            return access_token_response.get('access_token')

        except (MissingRequiredAttribute, MissingRequiredValue) as e:
            raise AuthFlowException('Missing required attribute/value') from e

        except HttpError as e:
            raise AuthFlowException('HTTP error on do_access_token_request') from e

    def get(self, request):
        self.request = request

        self.state = get_session(self.request, 'state', None)
        idp_id = get_session(self.request, 'idp_id', None)

        if idp_id is None or self.state is None:
            set_session(self.request, 'msg', 'Authentication failed. Retrying authentication')
            set_session(self.request, 'auth_error', "No 'idp_id' or 'state' in session")
            return self._retry_flow()

        try:
            self.idp = OIDCConfig.objects.get(id=idp_id)

            access_token = self._get_access_token()
            request.META['HTTP_AUTHORIZATION'] = 'Bearer '+access_token

            # authenticate sets 'auth_error' in the session, if something is wrong
            user = authenticate(request)

            if user is not None:
                # log user in for session authentication
                login(request, user)

                # removes error messages from session
                self._reset_session()

            return redirect('/')

        except OIDCConfig.DoesNotExist:
            set_session(self.request, 'auth_error', 'Unable to determine IdP')
            return self._flow_failed()

        except AuthFlowException as e:
            set_session(self.request, 'auth_error', str(e))
            return self._flow_failed()


class LogoutView(views.APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        LOGGER.info('Logging out %s', request.user)
        logout(request)
        return redirect('/')


class AuthInfo(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = AuthInfoSerializer

    def get_object(self):
        idps = OIDCConfig.objects.filter(enabled=True)
        return {
            'idps': idps,
            'default': default_idp().id,
        }
