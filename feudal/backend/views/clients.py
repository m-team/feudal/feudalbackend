
import logging

from django.urls import path
from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import get_object_or_404
from rest_framework import generics, views, exceptions
from rest_framework.authentication import BasicAuthentication
from rest_framework.response import Response

from feudal.backend.models import Site, Service
from feudal.backend.models.auth.vos import VO, Group, Entitlement
from feudal.backend.models.serializers import clients
from feudal.backend.brokers import RabbitMQInstance
from feudal.backend.permissions import DownstreamOnly

LOGGER = logging.getLogger(__name__)


# authentication class for the client api
AUTHENTICATION_CLASSES = (BasicAuthentication,)
PERMISSION_CLASSES = (DownstreamOnly,)


class DeploymentStateView(generics.UpdateAPIView):
    authentication_classes = AUTHENTICATION_CLASSES
    permission_classes = PERMISSION_CLASSES
    serializer_class = clients.DeploymentStateSerializer

    def get_object(self):
        if 'id' not in self.request.data:
            raise exceptions.ValidationError('Need "id"')

        return get_object_or_404(
            self.request.user.site.states.all(),
            id=self.request.data.get('id'),
        )

    def perform_update(self, serializer):
        state = serializer.save()

        # update the credential states of this deployment state
        state.client_credential_states(self.request.data.get('credential_states', {}))

        state.state_changed()


class DeploymentStateListView(generics.ListAPIView):
    authentication_classes = AUTHENTICATION_CLASSES
    permission_classes = PERMISSION_CLASSES
    serializer_class = clients.DeploymentStateSerializer

    def get_queryset(self):
        if self.request.query_params.get('initial', '') == 'true':
            return [
                state
                for state in self.request.user.site.states.all()
                if state.is_pending_for_restarting_client
            ]

        return [
            state
            for state in self.request.user.site.states.all()
            if state.is_pending
        ]


# the client has to fetch the configuration
class ConfigurationView(views.APIView):
    authentication_classes = AUTHENTICATION_CLASSES
    permission_classes = PERMISSION_CLASSES

    sid_to_service = {}

    @property
    def service_names(self):
        return [
            service.name
            for service in self.sid_to_service.values()
        ]

    @staticmethod
    def add_vo_to_service(service, vo):
        try:
            service.vos.get(name=vo.name)
        except VO.DoesNotExist:
            service.vos.add(vo)

    # returns the service ID to service mapping contained in the request
    def parse_sid_to_service(self, request):
        self.sid_to_service = {}
        services = request.data.get('services', None)
        if services is None:
            # maybe fail?
            return

        for service_id in services:
            service_descriptor = services.get(service_id, {})
            if 'name' in service_descriptor:
                # construct the service object
                self.sid_to_service[service_id] = Service.get_service(
                    service_descriptor.get('name', ''),
                    request.user.site,
                    description=service_descriptor.get('description', ''),
                    contact_email=service_descriptor.get('contact_email', ''),
                    contact_description=service_descriptor.get('contact_description', ''),
                )

    # adds / removes vos from services as needed to match the clients config
    def apply_vos_to_services(self, request):
        # apply groups / entitlements to the services
        group_to_service_ids = request.data.get('group_to_service_ids', None)
        if group_to_service_ids is not None:
            for group_name in group_to_service_ids:
                group = Group.get_group(name=group_name)

                service_ids = group_to_service_ids.get(group_name, None)
                if service_ids is not None:
                    for service_id in service_ids:
                        service = self.sid_to_service.get(service_id, None)
                        if service is not None:
                            self.add_vo_to_service(service, group)

        entitlement_to_service_ids = request.data.get('entitlement_to_service_ids', None)
        if entitlement_to_service_ids is not None:
            for entitlement_name in entitlement_to_service_ids:
                entitlement = Entitlement.get_entitlement(name=entitlement_name)

                service_ids = entitlement_to_service_ids.get(entitlement_name, None)
                if service_ids is not None:
                    for service_id in service_ids:
                        service = self.sid_to_service.get(service_id, None)
                        if service is not None:
                            self.add_vo_to_service(service, entitlement)

    # the client puts its (stripped) config
    def put(self, request):

        # the site where client is located
        try:
            request.user.site
        except Site.DoesNotExist:
            LOGGER.error('Improperly Configured! Client %s has no site. Configure one in the admin console!', request.user)
            return Response(
                'This client has no configured \'site\' at the portal. A feudal admin needs to fix the configuration',
                status=500,
            )

        LOGGER.debug('Client PUT config: %s', request.data)

        # load the services
        self.parse_sid_to_service(request)

        # check if the client stopped providing services
        for old_service in request.user.site.services.all():
            if old_service.name not in self.service_names:
                old_service.remove_service()

        self.apply_vos_to_services(request)

        # initialize the broker, just in case
        broker = RabbitMQInstance()
        broker.initialize()

        return Response({
            'rabbitmq_config': clients.RabbitMQInstanceSerializer(
                broker,
            ).data,
            'site': request.user.site.name,
        })


class DeregisterView(views.APIView):
    authentication_classes = AUTHENTICATION_CLASSES
    permission_classes = PERMISSION_CLASSES

    def put(self, request):

        # the site where client is located
        client_site = None
        try:
            client_site = request.user.site
        except Site.DoesNotExist:
            raise ImproperlyConfigured('client has no site')

        # deregister the client / its services / its site
        # we expect all deployments at the clients to be removed (and therefore simply delete them all)
        LOGGER.info('[DEREG] Client %s from site %s is deregistering', request.user, client_site)

        for service in client_site.services.all():
            LOGGER.info('[DEREG] Triggering service removal %s', service)
            service.remove_service()

        response = 'Success deregistering'
        return Response(response)


URLPATTERNS = [
    path('dep-state', DeploymentStateView.as_view(), name='client-dep-state'),
    path('dep-states', DeploymentStateListView.as_view(), name='client-dep-states'),

    path('config', ConfigurationView.as_view(), name='client-config'),
    path('deregister', DeregisterView.as_view(), name='client-deregister'),
]
