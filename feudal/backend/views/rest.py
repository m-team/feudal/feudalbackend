
import logging

from django.urls import path, re_path
from django.contrib.auth import logout
from django.shortcuts import get_object_or_404
from rest_framework import generics, exceptions, views
from rest_framework.response import Response
from rest_framework.permissions import AllowAny

from feudal.backend.views.renderers import PlainTextRenderer
from feudal.backend.models import Service
from feudal.backend.models.users import UserPreferences
from feudal.backend.models.serializers import (
    UserStateSerializer, ServiceSerializer, SSHPublicKeySerializer,
    DeploymentSerializer, DeploymentStateSerializer,
    UserPreferencesSerializer
)
from feudal.backend.models.deployments import VODeployment, ServiceDeployment
from feudal.backend.models.auth.vos import VO
from feudal.backend.models.auth.serializers import VOSerializer
from feudal.backend.permissions import UserOnly

LOGGER = logging.getLogger(__name__)
HELP_TEXT = """FEUDAL USER REST INTERFACE


PATH            METHOD  DESCRIPTION

user            GET     Show your user
                DELETE  Delete your user

services        GET     Show services available to you
service/<id>    GET     Show service with id <id>

vos             GET     Show virtual organisations available to you
vo/<id>         GET     Show virtual organisation with id <id>

ssh-keys        GET     Show your ssh keys
                POST    Upload a new ssh key

ssh-key/<id>    GET     Show your ssh key with id <id>
                DELETE  Delete your ssh key with id <id>

deployments
deployments/service
deployments/vo
                GET     Show all / service / vo deployments

deployment/vo/<vo id>
                GET     Show your deployment for the VO with <vo id>
                PATCH   Request deployment or removal by patching state_target to deployed
                        or not_deployed
deployment/service/<service id>
                GET     Show your deployment for the service with <service id>
                PATCH   Request deployment or removal by patching state_target to deployed
                        or not_deployed

states          GET     Show your deployment states
state/<id>      GET     Show your deployment state with id <id>
                PATCH   Patch answers to answer questions by the client. This is needed if your
                        deployment state has state=questionnaire
"""

PERMISSION_CLASSES = (UserOnly,)

class HelpView(views.APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (PlainTextRenderer,)

    def get(self, request):
        return Response(HELP_TEXT)


class UserView(generics.RetrieveDestroyAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = UserStateSerializer

    def get_object(self):
        return self.request.user

    def perform_destroy(self, instance):
        LOGGER.info('Deleting user on user request')
        logout(self.request)
        instance.delete()


class UserPreferencesView(generics.RetrieveUpdateAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = UserPreferencesSerializer

    def get_object(self):
        return self.request.user.preferences


    def perform_update(self, serializer):

        preferences = serializer.save()

        LOGGER.debug(self.request.user.msg('Updated preferences'))

        if preferences.deployment_mode == UserPreferences.DEPLOYMENT_MODE_SERVICES_ONLY:
            qs = self.request.user.deployments.instance_of(VODeployment)
            if qs.exists():
                LOGGER.debug(self.request.user.msg('Removing my VODeployments because of a new preference'))
                qs.delete()

        elif preferences.deployment_mode == UserPreferences.DEPLOYMENT_MODE_VOS_ONLY:
            qs = self.request.user.deployments.instance_of(ServiceDeployment)
            if qs.exists():
                LOGGER.debug(self.request.user.msg('Removing my ServiceDeployments because of a new preference'))
                qs.delete()


class ServiceListView(generics.ListAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = ServiceSerializer

    def get_queryset(self):
        return Service.objects.filter(vos__user=self.request.user)


class ServiceView(generics.RetrieveAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = ServiceSerializer

    def get_object(self):
        available_services = Service.objects.filter(vos__user=self.request.user).distinct()
        LOGGER.debug('Available services: %s', available_services)
        return get_object_or_404(
            available_services,
            id=self.kwargs['id'],
        )


class VOListView(generics.ListAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = VOSerializer

    def get_queryset(self):
        return self.request.user.vos.all()


class VOView(generics.RetrieveAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = VOSerializer

    def get_object(self):
        return get_object_or_404(
            self.request.user.vos.all(),
            id=self.kwargs['id'],
        )


class SSHPublicKeyListView(generics.ListCreateAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = SSHPublicKeySerializer

    def get_queryset(self):
        return self.request.user.ssh_keys.all()

    def perform_create(self, serializer):
        key = serializer.save(user=self.request.user)
        self.request.user.add_key(key)


class SSHPublicKeyView(generics.RetrieveDestroyAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = SSHPublicKeySerializer

    def get_object(self):
        return get_object_or_404(
            self.request.user.ssh_keys.all(),
            id=self.kwargs['id'],
        )

    def perform_destroy(self, instance):
        self.request.user.remove_key(instance)


class DeploymentListView(generics.ListAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = DeploymentSerializer

    def get_queryset(self):
        if self.kwargs.get('type', '') == 'service':
            return self.request.user.deployments.instance_of(ServiceDeployment)
        if self.kwargs.get('type', '') == 'vo':
            return self.request.user.deployments.instance_of(VODeployment)

        return self.request.user.deployments.all()


class DeploymentView(generics.RetrieveUpdateAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = DeploymentSerializer

    def get_serializer_context(self):
        if self.kwargs['type'] == 'vo':
            self.request.data['resourcetype'] = 'VODeployment'
        elif self.kwargs['type'] == 'service':
            self.request.data['resourcetype'] = 'ServiceDeployment'

        return {
            'request': self.request,
        }

    def get_object(self):
        dep_id = self.kwargs['id']

        if self.kwargs['type'] == 'vo':
            try:
                vo = self.request.user.vos.get(id=dep_id)
                return VODeployment.get_or_create(self.request.user, vo)

            except VO.DoesNotExist:
                raise exceptions.ValidationError('You have no VO with id "{}"'.format(dep_id))

        if self.kwargs['type'] == 'service':
            # find service with id dep_id
            service = None
            for s in self.request.user.services:
                if s.id == int(dep_id):
                    service = s
                    break

            if service is None:
                raise exceptions.ValidationError('You are not permitted to access a service with id "{}"'.format(dep_id))

            return ServiceDeployment.get_or_create(self.request.user, service)

        raise exceptions.ValidationError('Type must be either "service" or "vo"')

    def perform_update(self, serializer):
        old_state_target = self.get_object().state_target

        dep = serializer.save()

        if dep.state_target != old_state_target:
            dep.target_changed()
        else:
            LOGGER.debug(dep.msg('Patched but the state_target was not changed'))


class DeploymentStateListView(generics.ListCreateAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = DeploymentStateSerializer

    def get_queryset(self):
        return self.request.user.states.all()


class DeploymentStateView(generics.RetrieveUpdateAPIView):
    permission_classes = PERMISSION_CLASSES
    serializer_class = DeploymentStateSerializer

    def get_object(self):
        state_id = self.kwargs['id']

        return get_object_or_404(
            self.request.user.states.all(),
            id=state_id,
        )

    def perform_update(self, serializer):

        old_answers = None
        if 'answers' in serializer.validated_data:
            old_answers = self.get_object().answers

        state = serializer.save()

        if old_answers is not None:
            for key in state.answers:
                if state.answers[key] != old_answers.get(key, ''):
                    state.answers_changed()
                    return


# the HelpView is included one level above
URLPATTERNS = [
    path('user', UserView.as_view(), name='user'),

    path('user-prefs', UserPreferencesView.as_view(), name='user-prefs'),

    path('services', ServiceListView.as_view(), name='user-service-list'),
    re_path(r'^service/(?P<id>[0-9]+)$', ServiceView.as_view(), name='user-service'),

    path('vos', VOListView.as_view(), name='user-vo-list'),
    re_path(r'^vo/(?P<id>[0-9]+)$', VOView.as_view(), name='user-vo'),

    path('ssh-keys', SSHPublicKeyListView.as_view(), name='user-ssh-key-list'),
    re_path(r'^ssh-key/(?P<id>[0-9]+)$', SSHPublicKeyView.as_view(), name='user-ssh-key'),

    # type is either 'vo' or 'service'
    re_path(r'^deployments/?(?P<type>.+)?$', DeploymentListView.as_view(), name='user-dep-list'),
    re_path(r'^deployment/(?P<type>.+)/(?P<id>[0-9]+)$', DeploymentView.as_view(), name='user-dep'),

    path('states', DeploymentStateListView.as_view(), name='user-dep-state-list'),
    re_path(r'^state/(?P<id>[0-9]+)$', DeploymentStateView.as_view(), name='user-dep-state'),

    # this catch all must be last in the list!
    path('', HelpView.as_view(), name='user-help'),
    re_path(r'.*$', HelpView.as_view()),
]
