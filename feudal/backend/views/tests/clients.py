
import logging
import base64

from django.test import TestCase, Client

from feudal.backend.models import Site
from feudal.backend.models.users import User

LOGGER = logging.getLogger(__name__)


class ClientViewTest(TestCase):

    client = None

    @classmethod
    def setUpTestData(cls):
        cls.CLIENT_NAME = 'TEST_DOWNSTREAM_CLIENT'
        cls.CLIENT_PASSWORD = 'test1234'

        cls.api_client = User.construct_client(
            cls.CLIENT_NAME,
            cls.CLIENT_PASSWORD,
        )
        cls.api_client.save()

        cls.SITE_NAME = 'TEST_SITE'

        cls.site = Site(
            client=cls.api_client,
            name=cls.SITE_NAME,
        )
        cls.site.save()

    def setUp(self):
        self.client = Client()

    def auth_headers(self):
        auth_str = '{}:{}'.format(
            self.CLIENT_NAME,
            self.CLIENT_PASSWORD,
        )
        return {
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(auth_str.encode()).decode(),
        }

    def test_configuration_entitlement(self):
        response = self.client.put(
            '/client/config',
            {
                "services": {
                    "CVMFS": {
                        "name": "CERN VM FS",
                        "command": "xxxxxxxxxxxx",
                        "description": "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
                    }
                },
                "entitlement_to_service_ids": {
                    "urn:geant:h-df.de:group:myExampleColab#unity.helmholtz-data-federation.de": [
                        "CVMFS"
                    ]
                }
            },
            content_type='application/json',
            **self.auth_headers(),
        )
        LOGGER.debug('response: %s', response)
        LOGGER.debug('response json: %s', response.json())
        self.assertEqual(response.status_code, 200)

    def test_configuration_group(self):
        response = self.client.put(
            '/client/config',
            {
                "services": {
                    "CVMFS": {
                        "name": "CERN VM FS",
                        "command": "xxxxxxxxxxxx",
                        "description": "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
                    }
                },
                "group_to_service_ids": {
                    "/foobar": [
                        "CVMFS"
                    ]
                }
            },
            content_type='application/json',
            **self.auth_headers(),
        )
        LOGGER.debug('response: %s', response)
        LOGGER.debug('response json: %s', response.json())
        self.assertEqual(response.status_code, 200)

    def test_configuration_error(self):
        services = {
            "CVMFS": {
                "name": "CERN VM FS",
                "command": "xxxxxxxxxxxx",
                "description": "xxxxxxxxxxxxxxxxxxxxxxxxxxxx"
            }
        }
        invalid_configs = [
            {
                "services": services,
                "group_to_service_ids": None,
            },
            {
                "services": services,
                "group_to_service_ids": "",
            },
            {
                "services": services,
                "entitlement_to_service_ids": None,
            },
            {
                "services": services,
                "entitlement_to_service_ids": "",
            },
        ]

        for invalid_config in invalid_configs:
            response = self.client.put(
                '/client/config',
                invalid_config,
                content_type='application/json',
                **self.auth_headers(),
            )
            LOGGER.debug('response: %s', response)
            LOGGER.debug('response json: %s', response.json())
            self.assertEqual(response.status_code, 200)

    def test_deregister(self):
        response = self.client.put(
            '/client/deregister',
            {
            },
            content_type='application/json',
            **self.auth_headers(),
        )
        LOGGER.debug('response: %s', response)
        LOGGER.debug('response json: %s', response.json())
        self.assertEqual(response.status_code, 200)

    def test_get_states(self):
        response = self.client.get(
            '/client/dep-states',
            **self.auth_headers(),
        )
        LOGGER.debug('response: %s', response)
        LOGGER.debug('response json: %s', response.json())
        self.assertEqual(response.status_code, 200)
