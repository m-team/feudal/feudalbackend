# pylint: disable=no-member

import logging

from django.urls import reverse

from feudal.backend.models import Service
from feudal.backend.models.users import SSHPublicKey, User, UserPreferences
from feudal.backend.models.deployments import (
    Deployment, VODeployment, ServiceDeployment, DEPLOYED, NOT_DEPLOYED, QUESTIONNAIRE
)

LOGGER = logging.getLogger(__name__)


def test_user_help(user_test_client):
    response = user_test_client.get(
        reverse('user-help'),
    )
    assert response.status_code == 200


# USER
def test_user_deletion_success(user, user_test_client):
    user_id = user.id
    assert User.objects.filter(id=user_id).exists()

    response = user_test_client.delete(
        reverse('user'),
    )

    assert response.status_code == 204
    assert not User.objects.filter(id=user_id).exists()


## USER PREFS
def test_user_prefs_fetch(user_test_client, user):
    response = user_test_client.get(
        reverse('user-prefs'),
    )
    assert response.status_code == 200
    assert response.json()['deployment_mode'] == user.preferences.deployment_mode

def test_user_pref_ddeployment_mode_services_only(user_test_client, deployed_vo_deployment):
    # default mode is 'both'
    dep, _ = deployed_vo_deployment()
    dep_pk = dep.pk

    response = user_test_client.patch(
        reverse('user-prefs'),
        {
            'deployment_mode': UserPreferences.DEPLOYMENT_MODE_SERVICES_ONLY,
        },
        format='json',
    )
    assert response.status_code == 200

    # the deployment should've been deleted when we switched into a mode which prohibits its existence
    assert not Deployment.objects.filter(pk=dep_pk).exists()

def test_user_pref_deployment_mode_vos_only(user_test_client, deployed_service_deployment):
    # default mode is 'both'
    dep, _ = deployed_service_deployment()
    dep_pk = dep.pk

    response = user_test_client.patch(
        reverse('user-prefs'),
        {
            'deployment_mode': UserPreferences.DEPLOYMENT_MODE_VOS_ONLY,
        },
        format='json',
    )
    assert response.status_code == 200

    # the deployment should've been deleted when we switched into a mode which prohibits its existence
    assert not Deployment.objects.filter(pk=dep_pk).exists()


## SERVICES
def test_service_list(user_test_client, user):
    response = user_test_client.get(
        reverse('user-service-list'),
    )
    assert response.status_code == 200
    assert len(response.json()) == Service.objects.filter(vos__user=user).count()

def test_service(user_test_client, user, service):
    _ = service  # a service needs to exist
    response = user_test_client.get(
        reverse('user-service', kwargs={'id': Service.objects.filter(vos__user=user).first().id}),
    )
    assert response.status_code == 200


## SSH-KEYS
def test_ssh_key_list(user_test_client, user):
    response = user_test_client.get(
        reverse('user-ssh-key-list'),
    )
    LOGGER.debug('response: %s', response.json())
    assert response.status_code == 200

    key_list = response.json()
    assert user.ssh_keys.count() == len(key_list)

def test_ssh_key_add_success(user_test_client, user):
    key_name = 'key_name'
    key_value = 'key_value'

    response = user_test_client.post(
        reverse('user-ssh-key-list'),
        {
            'name': key_name,
            'key': key_value,
        },
        format='json',
    )
    LOGGER.debug('response: %s', response)
    assert response.status_code == 201

    key = user.ssh_keys.get(name=key_name)
    assert key.key == key_value

def test_ssh_key_add_error(user_test_client):
    # request missing key
    response = user_test_client.post(
        reverse('user-ssh-key-list'),
        {
            'name': 'key_name',
        },
        format='json',
    )
    assert response.status_code == 400

    # request missing name
    response = user_test_client.post(
        reverse('user-ssh-key-list'),
        {
            'key': 'key_value',
        },
        format='json',
    )
    assert response.status_code == 400

def test_ssh_key_remove_success(user_test_client, user):
    sshkey = SSHPublicKey(name='key_name', key='key_value', user=user)
    sshkey.save()

    response = user_test_client.delete(
        reverse('user-ssh-key', kwargs={'id': sshkey.pk}),
    )
    assert response.status_code == 204
    assert not user.ssh_keys.filter(name='key_name').exists()

def test_ssh_key_remove_error(user_test_client):
    # no key with id 0
    response = user_test_client.delete(
        reverse('user-ssh-key', kwargs={'id': 0}),
    )
    assert response.status_code == 404


## VOS
def test_vo_list(user_test_client, user):
    response = user_test_client.get(
        reverse('user-vo-list'),
    )
    assert len(response.json()) == user.vos.count()
    assert response.status_code == 200

def test_vo(user_test_client, user):
    response = user_test_client.get(
        reverse('user-vo', kwargs={'id': user.vos.first().id}),
    )
    assert response.status_code == 200


## DEPLOYMENTS
def test_dep_list(user_test_client, user):
    response = user_test_client.get(
        reverse('user-dep-list'),
    )
    assert response.status_code == 200
    vo_deps = response.json()
    assert user.deployments.all().count() == len(vo_deps)

def test_dep_list_vo(user_test_client, user):
    response = user_test_client.get(
        reverse('user-dep-list', kwargs={'type': 'vo'}),
    )
    assert response.status_code == 200
    vo_deps = response.json()
    assert user.deployments.instance_of(VODeployment).count() == len(vo_deps)

def test_dep_list_service(user_test_client, user):
    response = user_test_client.get(
        reverse('user-dep-list', kwargs={'type': 'service'}),
    )
    assert response.status_code == 200
    service_deps = response.json()
    assert user.deployments.instance_of(ServiceDeployment).count() == len(service_deps)

def test_service_deployment(user_test_client, user, service):

    for target in [DEPLOYED, NOT_DEPLOYED]:
        response = user_test_client.patch(
            reverse('user-dep', kwargs={'type': 'service', 'id': service.id}),
            {
                'state_target': target,
            },
            format='json',
        )
        LOGGER.debug('%s', response.json())
        assert response.status_code == 200

        dep = user.deployments.get(servicedeployment__service=service)
        assert dep.state_target == target

def test_service_deployment_failure(user_test_client):
    ''' no service with this id '''

    target = DEPLOYED
    response = user_test_client.patch(
        reverse('user-dep', kwargs={'type': 'service', 'id': 0}),
        {
            'state_target': target,
        },
        format='json',
    )
    LOGGER.debug('%s', response.json())
    assert response.status_code == 400

def test_vo_deployment(user_test_client, user, entitlement):
    vo = entitlement

    for target in [DEPLOYED, NOT_DEPLOYED]:
        LOGGER.debug('State target: %s', target)
        response = user_test_client.patch(
            reverse('user-dep', kwargs={'type': 'vo', 'id': vo.id}),
            {'state_target': target},
            format='json',
        )
        LOGGER.debug('%s', response.json())
        assert response.status_code == 200

        # user needs to get the deployment
        dep = user.deployments.get(vodeployment__vo=vo)
        assert dep.state_target == target

def test_vo_deployment_failure(user_test_client):
    ''' no vo with this id '''
    response = user_test_client.patch(
        reverse('user-dep', kwargs={'type': 'vo', 'id': 0}),
        {'state_target': DEPLOYED},
        format='json',
    )
    assert response.status_code == 400

def test_dep_failure(user_test_client):
    ''' no invalid type '''
    response = user_test_client.patch(
        reverse('user-dep', kwargs={'type': 'invalid', 'id': 0}),
        {'state_target': DEPLOYED},
        format='json',
    )
    assert response.status_code == 400


# DEP STATES
def test_dep_state_list(user_test_client, user):
    response = user_test_client.get(
        reverse('user-dep-state-list'),
    )
    assert response.status_code == 200
    states = response.json()
    assert user.states.all().count() == len(states)

def test_questionnaire_answers(user_test_client, user, service):
    response = user_test_client.patch(
        reverse('user-dep', kwargs={'type': 'service', 'id': service.id}),
        {
            'state_target': DEPLOYED,
        },
        format='json',
    )
    assert response.status_code == 200
    assert 'states' in response.json()
    assert len(response.json().get('states')) == 1

    state_id = response.json()['states'][0]['id']
    state = user.states.get(id=state_id)

    # mock client questionnaire response
    state.state = QUESTIONNAIRE
    state.questionnaire = {
        'question_foo': 'description_foo',
        'question_bar': 'description_bar',
    }
    state.state_changed()

    # answer the questions as the user
    response = user_test_client.patch(
        reverse('user-dep-state', kwargs={'id': state.id}),
        {
            'answers': {
                'question_foo': 'answer_foo',
                'question_bar': 'answer_bar',
            },
        },
        format='json',
    )
    LOGGER.debug('Response: %s', response.json())
    assert response.status_code == 200
