# pylint: disable=redefined-outer-name, unused-argument
import logging
import json
import base64
from urllib.error import HTTPError
from urllib.parse import quote

import pytest

from django.urls import reverse, resolve
from rest_framework.test import APIClient

from feudal.backend.models.users import User
from feudal.backend.models.auth.vos import Group, Entitlement

LOGGER = logging.getLogger(__name__)

password = 'foobarbaz'

raw_entitlement = 'urn:geant:h-df.de:group:aai-admin:role=test_users#unity.helmholtz-data-federation.de'

# good_at has a user (see upstreamClient)
good_at = 'good_at'
# good_at has no user (see upstreamClient)
bad_at = 'bad_at'

@pytest.fixture
def invalid_userinfo(idp):
    return {'sub': 'non-existent-sub', 'iss': idp.issuer_uri}

@pytest.fixture
def patched_get_userinfo(userinfo, invalid_userinfo):
    return lambda at: userinfo if at == good_at else invalid_userinfo

@pytest.fixture
def upstreamClient(idp, patched_get_userinfo, monkeypatch):
    # patch get_userinfo so it produces a valid userinfo for `good_at`
    monkeypatch.setattr(idp, 'get_userinfo', patched_get_userinfo)

    uc = User.construct_client(
        'upstream-client-with-idp',
        password,
        user_type=User.TYPE_CHOICE_UPSTREAM,
        idp=idp,
    )
    assert uc is not None
    uc.save()
    return uc

@pytest.fixture
def upstreamClientWithoutIdP():
    uc = User.construct_client(
        'upstream-client-without-idp',
        password,
        user_type=User.TYPE_CHOICE_UPSTREAM,
        idp=None,
    )
    assert uc is not None
    uc.save()
    return uc

@pytest.fixture
def apiClient(upstreamClient):
    client = APIClient()
    client.force_authenticate(user=upstreamClient)
    return client

@pytest.fixture
def apiClientWithoutIdP(upstreamClientWithoutIdP):
    client = APIClient()
    client.force_authenticate(user=upstreamClientWithoutIdP)
    return client

# TEST upstream-help
def test_help(apiClient):
    response = apiClient.get(reverse('upstream-help'))
    assert response.status_code == 200

# TEST upstream-at
@pytest.mark.parametrize('data', [{}, {'at': ''}])
def test_at_no_data(apiClient, data):
    response = apiClient.put(reverse('upstream-at'), data)
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 400

def test_at_unassociated(apiClientWithoutIdP):
    response = apiClientWithoutIdP.put(reverse('upstream-at'), {})
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 403

def test_at_no_user(apiClient):
    response = apiClient.put(
        reverse('upstream-at'),
        {'at': bad_at},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 404

def raise_http_error(_):
    # required positional arguments: 'url', 'code', 'msg', 'hdrs', and 'fp'
    raise HTTPError('foo', 500, 'test-error', {}, None)

def test_at_error(apiClient, idp, monkeypatch):
    # patch so get_userinfo raises an http error
    monkeypatch.setattr(idp, 'get_userinfo', raise_http_error)

    apiClient.force_authenticate(
        user=User.construct_client(
            'test_at_error-idp',
            password,
            user_type=User.TYPE_CHOICE_UPSTREAM,
            idp=idp,
        )
    )
    response = apiClient.put(
        reverse('upstream-at'),
        {'at': good_at},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 503

def test_at_success(apiClient, user):
    response = apiClient.put(
        reverse('upstream-at'),
        {'at': good_at},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 204

# TEST upstream-userinfo
@pytest.mark.parametrize('data', [{}, {'userinfo': {}}])
def test_userinfo_no_data(apiClient, data):
    response = apiClient.put(
        reverse('upstream-userinfo'),
        format='json'
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code >= 400

def test_userinfo_unassociated(apiClientWithoutIdP):
    response = apiClientWithoutIdP.put(
        reverse('upstream-userinfo'),
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 403

def test_userinfo_no_user(apiClient, invalid_userinfo):
    response = apiClient.put(
        reverse('upstream-userinfo'),
        {'userinfo': invalid_userinfo},
        format='json',
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 404

def test_userinfo_success(apiClient, user, userinfo):
    response = apiClient.put(
        reverse('upstream-userinfo'),
        {'userinfo': userinfo},
        format='json',
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 204

# TEST upstream-users
@pytest.mark.parametrize('path', [
    '/upstream/users/',
])
def test_users_url(path):
    view_name = 'upstream-users'
    assert resolve(path).view_name == view_name

def test_users_unassociated(apiClientWithoutIdP):
    response = apiClientWithoutIdP.get(reverse('upstream-users', args={}))
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 403

def test_users_no_vo(apiClient, user, userinfo):
    response = apiClient.get(
        reverse('upstream-users'),
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    json_response = json.loads(response.content)
    assert userinfo['sub'] in json_response

def test_users_group(apiClient, user, userinfo):
    vo = Group.get_group('test_user_vo-group', user.idp)
    user.vos.add(vo)

    response = apiClient.get(
        reverse('upstream-users'),
        {'vo': quote(vo.name)},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    assert userinfo['sub'] in json.loads(response.content)

def test_users_group_url(apiClient, user, userinfo):
    vo = Group.get_group('test_user_vo-group', user.idp)
    user.vos.add(vo)

    response = apiClient.get(
        '{}?vo={}'.format(
            reverse('upstream-users'),
            quote(vo.name),
        ),
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    assert userinfo['sub'] in json.loads(response.content)

def test_users_entitlement(apiClient, user, userinfo):
    vo = Entitlement.get_entitlement(raw_entitlement, user.idp)
    user.vos.add(vo)

    response = apiClient.get(
        reverse('upstream-users'),
        {'vo': quote(vo.name)},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    assert userinfo['sub'] in json.loads(response.content)

def test_users_entitlement_url(apiClient, user, userinfo):
    vo = Entitlement.get_entitlement(raw_entitlement, user.idp)
    user.vos.add(vo)

    response = apiClient.get(
        '{}?vo={}'.format(
            reverse('upstream-users'),
            quote(vo.name),
        ),
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    assert userinfo['sub'] in json.loads(response.content)

def test_users_empty_vo(apiClient, idp):
    vo = Group.get_group('test_users_empty_vo-group', idp)

    response = apiClient.get(
        reverse('upstream-users'),
        {'vo': quote(vo.name)},
    )
    LOGGER.debug(response.status_code, response.content)
    assert response.status_code == 200
    assert len(json.loads(response.content)) == 0

def test_users_non_existent_vo(apiClient):
    response = apiClient.get(
        reverse('upstream-users'),
        {'vo': quote('non-existent-vo')},
    )
    LOGGER.debug('content: %s', response.content)
    assert response.status_code == 404

# TEST upstream-user
def test_user_url():
    view_name = 'upstream-user'
    path = reverse(view_name, kwargs={'sub': 'subject'})
    assert resolve(path).view_name == view_name

def test_user_existent(apiClient, user):
    response = apiClient.get(
        reverse('upstream-user', kwargs={'sub': quote(user.sub)}),
    )
    LOGGER.debug('content: %s', response.content)
    assert response.status_code == 200

def test_user_non_existent(apiClient):
    non_existent_sub = 'fb0fa558-cfa2-49f9-b847-5c651d1f6135-non-existent'
    response = apiClient.get(
        reverse('upstream-user', kwargs={'sub': quote(non_existent_sub)}),
    )
    LOGGER.debug('content: %s', response.content)
    assert response.status_code == 404

def test_user_delete_existent(apiClient, user):
    response = apiClient.delete(
        reverse('upstream-user', kwargs={'sub': quote(user.sub)}),
    )
    LOGGER.debug('content: %s', response.content)
    assert response.status_code == 204

def test_user_delete_non_existent(apiClient):
    non_existent_sub = 'fb0fa558-cfa2-49f9-b847-5c651d1f6135-non-existent'
    response = apiClient.delete(
        reverse('upstream-user', kwargs={'sub': quote(non_existent_sub)}),
    )
    LOGGER.debug('content: %s', response.content)
    assert response.status_code == 404

# TEST basic authentication. Does not use apiClient
def test_basic_auth(testClient):
    auth_str = '{}:{}'.format(
        'upstream-client-with-idp',
        password,
    )
    testClient.put(
        reverse('upstream-at'),
        {'at': 'foo'},
        content_type='application/json',
        **{
            'HTTP_AUTHORIZATION': 'Basic ' + base64.b64encode(auth_str.encode()).decode(),
        },
    )
