# pylint: disable=unused-argument

import logging


LOGGER = logging.getLogger(__name__)


def test_api_state(user_test_client, user):
    response = user_test_client.get('/webpage/state')

    assert 'user' in response.json()
    LOGGER.debug('response: %s', response.json())

    user_data = response.json()['user']
    assert user_data is not None

    for key in ['services', 'ssh_keys', 'userinfo', 'vos', 'id', 'deployments', 'profile_name']:
        assert key in user_data

    assert user_data['userinfo']['email'] == user.email


# messages are only shown once to the user
def test_api_state_msg_deletion(user_test_client, user):
    session = user_test_client.session
    msg = 'temp_test_message'
    session.update({'msg': msg})
    session.save()

    response_data = user_test_client.get('/webpage/state').json()

    LOGGER.debug('Response: %s', response_data)

    assert 'msg' in response_data
    assert response_data.get('msg') == msg


    response_data = user_test_client.get('/webpage/state').json()

    LOGGER.debug('Response: %s', response_data)

    # message was shown to the user and is now deleted
    assert 'msg' not in response_data
