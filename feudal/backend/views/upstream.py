
import logging

from urllib.error import HTTPError
from urllib.parse import unquote

from django.urls import path, re_path

from rest_framework.exceptions import ValidationError, NotFound, PermissionDenied
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.authentication import BasicAuthentication
from rest_framework.permissions import AllowAny

from feudal.backend.views.renderers import PlainTextRenderer
from feudal.backend.models import User
from feudal.backend.models.auth.vos import VO
from feudal.backend.permissions import UpstreamOnly

LOGGER = logging.getLogger(__name__)

HELP_TEXT = """FEUDAL UPSTREAM REST INTERFACE


PATH            METHOD  DESCRIPTION

at/             PUT     Update a user using an access token. The access token is used to retrieve an up-to-date userinfo.
                        This is the preferred method of updating userinfos.

userinfo/       PUT     Update a user using a plain userinfo.

users/
users/?vo=<vo>
                GET     Retrieve the subjects of the registered users. Can be filtered by vo.

user/<sub>/     GET     Check if the user with sub <sub> is registered.
                DELETE  Delete the user with sub <sub> from feudal.
                        This may only be done, when the user was deleted by its identity provider.
"""

class HelpView(APIView):
    permission_classes = (AllowAny,)
    renderer_classes = (PlainTextRenderer,)

    def get(self, request):
        return Response(HELP_TEXT)


# authentication class for the client api
AUTHENTICATION_CLASSES = (BasicAuthentication,)
PERMISSION_CLASSES = (UpstreamOnly,)


# checks userinfo for validity and then updates the user accordingly
def _update_userinfo(userinfo, idp):
    validation_errors = {}
    if 'iss' not in userinfo or userinfo['iss'] != idp.issuer_uri:
        validation_errors['iss'] = 'Must be a valid issuer url matching your associated IdPS issuer url'
    else:
        if userinfo['iss'] != idp.issuer_uri:
            validation_errors['iss'] = 'This field does not match the associated IdPs issuer URL'

    if 'sub' not in userinfo:
        validation_errors['sub'] = 'Must be a valid subject'

    if validation_errors:
        raise ValidationError({'userinfo': validation_errors})

    try:
        User.objects.get(
            sub=userinfo['sub'],
            idp=idp,
        ).update_userinfo(userinfo)
        return Response('User updated', status=204)
    except User.DoesNotExist:
        raise NotFound('User with subject does not exist')

class UpstreamAPIView(APIView):
    authentication_classes = AUTHENTICATION_CLASSES
    permission_classes = PERMISSION_CLASSES

    def _get_idp(self, request):
        if request.user.idp is None:
            raise PermissionDenied('No IdP associated')

        return request.user.idp

class AccessTokenView(UpstreamAPIView):

    def put(self, request):

        idp = self._get_idp(request)

        at = request.data.get('at', '')
        if at == '':
            raise ValidationError({'at': 'Must be an access token of the associated IdP'})

        try:
            userinfo = idp.get_userinfo(at)
            return _update_userinfo(userinfo, idp)

        except HTTPError as e:
            LOGGER.debug('Userinfo request using upstream access token failed: %s', e)
            return Response(
                'Request for userinfo using the access token failed: {}'.format(e.reason),
                status=503,
            )


# UserinfoView allows an upstream client to submit a plain userinfo for updating users
# the userinfos have to be associated with the upstream-clients idp
class UserinfoView(UpstreamAPIView):

    def put(self, request):
        idp = self._get_idp(request)

        userinfo = request.data.get('userinfo', {})
        if 'sub' not in userinfo or 'iss' not in userinfo:
            raise ValidationError({'userinfo': 'Must contain a valid userinfo of the associated IdP'})

        return _update_userinfo(userinfo, idp)


class UsersView(UpstreamAPIView):

    renderer_classes = [JSONRenderer]

    # returns a list of subs of the users related to the associated idps
    # can be filter using 'vo' kwarg
    def get(self, request):
        idp = self._get_idp(request)

        users = []

        if 'vo' in request.query_params:
            vo_name = request.query_params.get('vo', '')
            if vo_name == '':
                raise ValidationError({'vo': 'Must be an existing VO. In addition this VO must be related to your associated IdP'})

            vo_name = unquote(vo_name)
            LOGGER.debug('Got vo_name: %s', vo_name)
            try:
                vo = VO.objects.get(name=vo_name, idp=idp)
                users = vo.user_set.all()
            except VO.DoesNotExist:
                raise NotFound('No matching VO found')

        else:
            users = idp.users.filter(user_type=User.TYPE_CHOICE_USER)

        return Response([user.sub for user in users])


class UserView(UpstreamAPIView):
    renderer_classes = [JSONRenderer]

    def _get_user(self, request, **kwargs):
        sub = unquote(kwargs.get('sub', ''))
        if sub == '':
            raise ValidationError({'sub': 'Must be the subject of a user at your associated IdP'})

        try:
            return self._get_idp(request).users.get(sub=sub)
        except User.DoesNotExist:
            raise NotFound('User with subject does not exist')

    def get(self, request, **kwargs):
        if self._get_user(request, **kwargs) is not None:
            return Response('User with subject exists', status=200)

        raise NotFound('User with subject does not exist')

    def delete(self, request, **kwargs):
        user = self._get_user(request, **kwargs)
        user.delete()
        return Response('User deleted', status=204)



URLPATTERNS = [
    path('at/', AccessTokenView().as_view(), name='upstream-at'),
    path('userinfo/', UserinfoView().as_view(), name='upstream-userinfo'),
    path('users/', UsersView().as_view(), name='upstream-users'),
    path('user/<sub>/', UserView().as_view(), name='upstream-user'),

    # this catch all must be last in the list!
    re_path(r'.*$', HelpView.as_view(), name='upstream-help'),
]
