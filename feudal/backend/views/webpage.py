
import logging

from django.urls import path
from rest_framework import generics
from rest_framework.permissions import AllowAny

from feudal.backend.models import serializers

LOGGER = logging.getLogger(__name__)


class StateView(generics.RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = serializers.StateSerializer


    def _state_view_data(self, request):
        data = {
            'user': request.user,
            'session': request.session,
        }

        if 'msg' in request.session:
            data['msg'] = request.session.pop('msg')

        return data

    def get_object(self):
        return self._state_view_data(self.request)


URLPATTERNS = [
    path('state', StateView.as_view()),
]
