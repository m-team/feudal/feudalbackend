
# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'vAZew+5URvdWL/1dSt/AesYioa1mMJsoiJ6Tzcin6H1Z6Rfw0rUwnO0YBUxFBZ+uxDmP0XPy+ZwHySkvjlqLtw=='

NO_BROKER = True

ALLOWED_HOSTS = [
    'feudal-dev.scc.kit.edu',
]

# http parameter for the idp hint redirect
HTTP_PARAM_IDPHINT = 'idp'

CORS_ORIGIN_ALLOW_ALL = True

LOGGING_ROOT = '/logs'

OIDC_REDIRECT_URI = 'https://feudal-dev.scc.kit.edu/auth/callback'
STATIC_URL = '/django-static/'
STATIC_ROOT = '/var/www/feudal/django-static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'feudal',
        'USER': 'root',
        'PASSWORD': 'feudal',
        'HOST': 'mysql',
    }
}

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'Europe/Berlin'
USE_TZ = True
USE_I18N = True
USE_L10N = True
