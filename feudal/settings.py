# pylint: disable=wildcard-import,unused-wildcard-import

import sys
import logging
import os

from django.core.exceptions import ImproperlyConfigured

# we expect the local settings to set these values:
LOGGING_ROOT = ''

# IMPORT THE LOCAL CONFIG
try:
    from feudal.local_settings import *
except ImportError:
    print('No local settings! Trying docker settings.')

    try:
        from feudal.docker_local_settings import *
    except ImportError:
        raise ImproperlyConfigured('No local settings provided. Create feudal.local_settings.')

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

AUTH_USER_MODEL = 'backend.User'

# cookie settings
SESSION_COOKIE_AGE = 3600
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_HTTPONLY = False

WSGI_APPLICATION = 'feudal.wsgi.application'

# Application definition
ROOT_URLCONF = 'feudal.backend.urls'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'polymorphic',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'feudal.backend',
    'corsheaders',
    'django_mysql',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

# AUTHENTICATION AND AUTHORIZATION
AUTHENTICATION_BACKENDS = [
    'feudal.backend.auth.OIDCTokenAuthBackend',
    'django.contrib.auth.backends.ModelBackend',
]

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': [
        'rest_framework.authentication.SessionAuthentication',
        'feudal.backend.auth.OIDCTokenAuthHTTPBackend',
    ],
    'DEFAULT_PERMISSION_CLASSES': [
        'rest_framework.permissions.IsAuthenticated',
    ],
}

# Password validation
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# LOGGING
if not LOGGING_ROOT:
    raise ImproperlyConfigured('Local settings didnt specify LOGGING_ROOT')

FILE_HANDLER = 'logging.handlers.RotatingFileHandler'
HANDLER_MAX_BYTES = 1024*1024*20  # 20 MB
HANDLER_BACKUP_COUNT = 3

# audit log
DEBUGX_LOG_LEVEL = 9  # less then debug
logging.addLevelName(DEBUGX_LOG_LEVEL, 'DEBUX')

AUDIT_LOG_LEVEL = 5  # lowest log level
logging.addLevelName(AUDIT_LOG_LEVEL, 'AUDIT')

# custom filter callback
def audit_only_callback(record):
    return record.levelname == 'AUDIT'

# are we currently running tests? -> log to a subdirectory (or not at all see loggers below)
TESTING = 'test' in sys.argv or 'pytest' in sys.modules
if TESTING:
    LOGGING_ROOT += '/tests'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        },
        'compact': {
            'format': '%(levelname)s - %(message)s',
        },
        'audit': {
            'format': '%(asctime)s - %(message)s',
        },
    },
    'filters': {
        'audit_only': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': audit_only_callback,
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
        'root': {
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/root.log',
            'formatter': 'standard',
        },
        'django': {
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/django.log',
            'formatter': 'standard',
        },
        'audit': {
            'level': 'AUDIT',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/audit.log',
            'formatter': 'audit',
            'filters': ['audit_only'], # display no other messages
        },
        'debugx': {
            'level': 'DEBUX',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/debug-extra.log',
            'formatter': 'standard',
        },
        'compact-debugx': {
            'level': 'DEBUX',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/compact-debug-extra.log',
            'formatter': 'compact',
        },
        'debug': {
            'level': 'DEBUG',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/debug.log',
            'formatter': 'standard',
        },
        'compact-debug': {
            'level': 'DEBUG',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/compact-debug.log',
            'formatter': 'compact',
        },
        'info': {
            'level': 'INFO',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/info.log',
            'formatter': 'standard',
        },
        'error': {
            'level': 'ERROR',
            'class': FILE_HANDLER,
            'maxBytes': HANDLER_MAX_BYTES,
            'backupCount': HANDLER_BACKUP_COUNT,
            'filename': LOGGING_ROOT + '/error.log',
            'formatter': 'standard',
        },
    },
    'root': {
        'handlers': ['root'],
        'level': 'ERROR',
    },
    'loggers': {
        'feudal': {
            'handlers': ['compact-debugx', 'debugx', 'compact-debug', 'debug', 'info', 'error', 'audit'],
            'level': 'AUDIT',
            'propagate': False,
        },
        'django': {
            'handlers': ['django'],
            'level': 'INFO',
            'propagate': False,
        },
    } if not TESTING else {
        'feudal': {
            'level': 'DEBUG',
            'propagate': True,
        },
        'django': {
            'level': 'INFO',
            'propagate': True,
        },
    },
}
