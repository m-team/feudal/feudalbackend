#!/bin/bash

# we expect python3 to be 3.6
python3 ./manage.py collectstatic --no-input
python3 ./manage.py migrate --no-input

# create a superuser
cat << EOF  | ./manage.py shell
from feudal.backend.models.users import User;

username = '${1}'
password = '${2}'

qs = User.objects.filter(username=username)
if not qs.exists():
	admin = User.objects.create_superuser(username, '', password)
else:
	admin = qs.first()
	admin.set_password(password)

admin.user_type = 'admin'
admin.save()

EOF
