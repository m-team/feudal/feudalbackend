#!/bin/bash

# exit whole script
trap "exit" INT

# {{{ MOCK CONFIG FILES
# the webpage is now a submodule
cd $(dirname "$0")/..
export backend=$(pwd)
export webpage=$backend/webpage
export dist=$webpage/dist
export static=$backend/static
export DJANGO_SECRET=$backend/.secret.key
export venv=$backend/env



# {{{ WEBPAGE
# this file get changed during building
[[ -d $webpage ]] && cd $webpage && git checkout package-lock.json

cd $backend
git submodule update --init

echo "FEUDAL> Changing directory to $webpage"
cd $webpage

echo "FEUDAL> Installing dependencies (using npm install)"
export npm_config_loglevel=error
npm install

ng=$webpage/node_modules/.bin/ng
ng_flags="--prod"
[[ -n $DEV ]] && ng_flags="--aot"
echo "FEUDAL> Building webpage (using $ng build $ng_flags)"
$ng build $ng_flags
# }}}

# vim: set foldmethod=marker :
