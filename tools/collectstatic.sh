#!/bin/bash

# exit whole script
trap "exit" INT

# {{{ MOCK CONFIG FILES
# the webpage is now a submodule
cd $(dirname "$0")/..
export backend=$(pwd)
export webpage=$backend/webpage
export dist=$webpage/dist
export static=$backend/static
export DJANGO_SECRET=$backend/.secret.key
export venv=$backend/env


# django settings

# {{{ PYTHON
echo "FEUDAL> Creating new python virtual environment: $venv"
python3.6 -m venv $venv

echo "FEUDAL> Activating virtual environment"
source $venv/bin/activate

pip -q install -U pip
pip install -e .
# }}}

# {{{ DJANGO

# django secret key
if [[ ! -f $DJANGO_SECRET ]]
then
	echo "FEUDAL> Generating secret key: $DJANGO_SECRET"
	dd if=/dev/urandom bs=64 count=1 | base64 > $DJANGO_SECRET
fi

echo "FEUDAL> Preparing static files in $static"
$backend/manage.py collectstatic --no-input

# }}}

# vim: set foldmethod=marker :
